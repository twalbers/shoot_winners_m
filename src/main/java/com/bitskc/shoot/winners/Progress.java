/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 *
 * @author talbers
 */
public class Progress {

    static JDialog dialog; 
    public static JProgressBar pb;
    public static void showProgress(String title, String message, int pbMax) {
        dialog = new JDialog((JFrame)null, title);
        pb = new JProgressBar(JProgressBar.HORIZONTAL);
        pb.setStringPainted(true);
        if (pbMax > 0){
            pb.setMaximum(pbMax); 
        } else {
            pb.setIndeterminate(true);
        }           
        JLabel lbl = new JLabel(message);
        JPanel cntrPnl = new JPanel();
        cntrPnl.add(lbl);
        cntrPnl.add(pb);
        dialog.getContentPane().add(cntrPnl, BorderLayout.CENTER);
        dialog.pack();
        dialog.setVisible(true);
        dialog.setLocationRelativeTo(null);
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);   //Don't allow them to close the window
}

    public static void incProgress(int n){
        pb.setValue(n);
        dialog.repaint();
    }
    public static void closeProgress() {   
        if (dialog != null) {
            dialog.dispose();             
        }       
    }
    
    public static void endProgram(){
        System.exit(0);
    }
}
