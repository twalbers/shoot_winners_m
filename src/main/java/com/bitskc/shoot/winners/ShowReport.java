/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Shoot_Winners.frame;
import java.awt.Desktop;
import java.io.File;
import java.util.HashMap;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.slf4j.LoggerFactory;
/**
 *
 * @author talbers
 */
public class ShowReport {

        static org.slf4j.Logger logger = LoggerFactory.getLogger("Show Report"); 
        
    public static void CreateReport(String strReport){
        JasperReport jasperReport;
        JasperPrint jasperPrint;  
        String strExport;
        String strSaveFile;
        File f;
        int i = 1;
        try
        {
            if (strReport.contains("Excel")){
                strExport = ".xlsx";
            }else{
                strExport = ".pdf";
            }
            strSaveFile = Constants.fileName + strExport;
            f = new File(strSaveFile);
            while (f.exists()){ //if the file already exists do not overwrite but rather add another
                strSaveFile = Constants.fileName + "-" + Integer.toString(i) + strExport;
                logger.debug("Report save path " + strSaveFile);
                f = new File(strSaveFile);
                i++;
            } 
            logger.debug("Starting to compile report");
            jasperReport = JasperCompileManager.compileReport( "Reports/" + strReport);
            logger.debug("Report Compiled. File path for report: Reports/" + strReport);
            jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<String, Object>(), Constants.conn);  //use existing connection for this report
            logger.debug("Creating pdf or Excel file");
            if (strExport.contains("xlsx")){
                JRXlsxExporter exporter = new JRXlsxExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(f));
                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration(); 
                configuration.setDetectCellType(false);//Set configuration as you like it!!
                configuration.setCollapseRowSpan(false);
                exporter.setConfiguration(configuration);
                exporter.exportReport();                
            }else{
                JasperExportManager.exportReportToPdfFile(jasperPrint, strSaveFile);   
            }              
            JOptionPane.showMessageDialog(frame, "Report complete it should display momentarily!", "Report Done!", JOptionPane.INFORMATION_MESSAGE);              
            logger.debug("Report Created, opening viewer!"); 
            ViewReport(strSaveFile);
        } catch (JRException e){
            logger.error("Error " + e.getMessage());
            e.printStackTrace();
        }          
    }
    
    public static void ViewReport(String strFile){
        try {
            File pdfFile = new File(strFile);
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                } else {
                    logger.error("Awt Desktop is not supported!");
                }
            } else {
                logger.error("File does not exists!");
            }
            logger.debug("Report Created");
        } catch (Exception ex) {
            logger.error("Error " + ex.getMessage());            
            ex.printStackTrace();
        }        
    }
}
