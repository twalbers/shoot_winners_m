/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Shoot_Winners.frame;
import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
/**
 *
 * @author talbers
 */
public class Activation {
        
    static String machineID;
    static String SQL;
    static Logger logger = LoggerFactory.getLogger("Activation");
    static ResultSet rsMachineID;  
    static Statement acstmt;
    //static String serialnum = "Demo";
    public static void GetMachineInfo(){         
        try {
            acstmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            machineID = getMachineID();
            if (machineID.equals("Error")){
                logger.error("getMachineID returned Error");
                System.exit(1);
            };
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date());     
            //Now query the machineID to see if the program has been activated
            //System.out.println("Getting rowcount of machineID");
            logger.debug("Getting rowcount of machineID");
            SQL = "Select count(*) as nRows from machineID";
            rsMachineID = acstmt.executeQuery(SQL);
            rsMachineID.next();
            if ((rsMachineID.getInt("nRows")) == 0){  //This is the first run so populate the machineID info
                logger.debug("First run so populating the machineID table");
                SQL = "Insert into machineID (serialnum, whenadded, timesrun, isActivated, uniqueID) values('Demo', '" + timeStamp + "',3,false,'" + machineID + "')";
                logger.debug("Inserting initial row in machineID");
                acstmt.executeUpdate(SQL);              
                Constants.newDB = "Yes";
            }
            //We should have a row in machineID
            logger.debug("Getting data from machineID");
            SQL = "Select * from machineID";
            rsMachineID = acstmt.executeQuery(SQL);
            rsMachineID.next();
            if (!rsMachineID.getString("uniqueID").equals(machineID)){
                //The UUID's do not match so update local database
                logger.debug("UUID's do not match so updating");
                SQL = "Update machineID set serialnum = 'Demo', whenadded='" + timeStamp + "', timesrun = 3, isActivated = false, SMS = false, uniqueID = '" + machineID + "'";
                updateMachineID(SQL); //acstmt.executeUpdate(SQL); 
                Constants.newDB = "Yes";
                //now reload rs after data insert
                SQL = "Select * from machineID";
                rsMachineID = acstmt.executeQuery(SQL);
                rsMachineID.next();
            }
            //Progress.closeProgress();            
            //Check on Activation
            Activation(rsMachineID.getString("serialnum"), rsMachineID.getTimestamp("whenadded").toLocalDateTime().toLocalDate(),rsMachineID.getInt("timesrun"),
                    rsMachineID.getBoolean("isactivated"),rsMachineID.getString("uniqueID"));   
//            rsMachineID.close();
        } catch (Exception e){
            errorPrint(e);
        }
    }
    public static void updateMachineID(String SQL){
        //quick routine to update the machineID table
        try {
            acstmt.executeUpdate(SQL);
        }  catch (Throwable e) {
                errorPrint(e);      
        }        
    }     

    public static void Activation(String serialnum, LocalDate whenAdded, int timesRun, boolean isActivated, String uniqueID){
            
        try {           
            //System.out.println("Checking Activation");
            logger.debug("Checking Activation");
            //Get how many days since last checkin with Web activation 
            long daysActivated = ChronoUnit.DAYS.between(whenAdded, LocalDate.now());
            if (daysActivated > 35) {
                String title = frame.getTitle();           
                frame.setTitle(title + " - " + (45 - daysActivated) + " days till activation check");
            }
            if (!isActivated) {                  
                Shoot_Winners.deactivate.setEnabled(false);
                //System.out.println("Checking for web connectivity");
                logger.debug("Checking for web connectivity");
                if (!InternetAvailabilityChecker.isInternetAvailable()) {
                    JOptionPane.showMessageDialog(null, "Cannot connect to the internet.\n\nActivation Failed! Please try again",
                            "\n\nNo internet Connection or Activation server not reachable.",JOptionPane.ERROR_MESSAGE);
                    System.exit(0); //Internet is required for Demo or reactivation after database delete
                }               
                //Display userinput asking for serialNum, if blank then demo
                String[] options = {"OK"};
                JPanel panel = new JPanel();
                JLabel lbl = new JLabel("Serial Number : ");
                JTextField txt = new JTextField(24);
                //Fill out the serial number if available
  //              if (!serialnum.equals("Demo")) txt.setText(serialnum);    //Removed this to discourage activating and deactivating
                panel.add(lbl);
                panel.add(txt);
                new dlgActivate();  //display form asking for serial num                  
                if (!Constants.strSerial.equals("")) serialnum = Constants.strSerial;
                if (Constants.newDB.equals("Yes") || !serialnum.equals("Demo")) {  //yes was selected 
                    if (InternetAvailabilityChecker.isMySQLAvailable(Constants.host)) {
                        WebActivation(Constants.newDB, machineID, serialnum); 
                    } else {
                        JOptionPane.showMessageDialog(null, "Cannot contact online activation server!\nPlease contact " 
                                + Constants.email, "Activation error!",JOptionPane.ERROR_MESSAGE);
                    }
                } else {  //no was selected treat this as a demo
                    DemoAvailable(timesRun, "Yes", serialnum); 
                }
            } else if (daysActivated > 45 || Constants.forceActivation.equals("Yes")){  //Program has been activated but we must 'phone home' to confirm activation
                //Check for Web connection, if none available decrement times run by 1, if lower than 3 warn them
                //System.out.println("Attempting to confirm activation");
                logger.debug("Attempting to confirm activation");
                if (InternetAvailabilityChecker.isInternetAvailable()) {
                    if (InternetAvailabilityChecker.isMySQLAvailable(Constants.host)) {
                        WebActivation(Constants.newDB, machineID, serialnum); 
                    } else {
                        JOptionPane.showMessageDialog(null, "Cannot contact online activation server!\nPlease contact " 
                                + Constants.email, "Activation error!",JOptionPane.ERROR_MESSAGE);
                        DemoAvailable(timesRun, "No", serialnum);
                    }
                } else DemoAvailable(timesRun, "No", serialnum);  //Decrease the timesrun by 1
            }
            logger.debug("Exiting activation");
        } catch (Exception e){
            errorPrint(e);
        } finally {
            Constants.forceActivation = "No";
        }
    }
    
    public static void DemoAvailable(int timesRun, String inDemo, String serialNum){
        
        try {
            logger.debug("Entering DemoAvailable");
            if (timesRun == 0){
                JOptionPane.showMessageDialog(null, "The program has been disabled pending activation\nPlease activate or contact tech support: " 
                        + Constants.email, "Activation issue",JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }else if (timesRun < 3 && inDemo.equals("No")){
                JOptionPane.showMessageDialog(null,"Internet connection is required to re-authenticate program.\n\nYou have " + timesRun 
                        + " more times before the program becomes disabled.","Activation issue",JOptionPane.WARNING_MESSAGE);
            }else if (inDemo.equals("Yes")){                
                //Do a web check to see if the UUID has been put in, if yes, activated is false, 
                if (InternetAvailabilityChecker.isMySQLAvailable(Constants.host)) {
                    WebActivation(Constants.newDB, machineID, serialNum);  
                } else {
                    JOptionPane.showMessageDialog(null, "Cannot contact online activation server!\nPlease contact " 
                            + Constants.email, "Activation error!",JOptionPane.ERROR_MESSAGE);
                }
            }
            SQL = "Update machineID set timesrun = " + (timesRun - 1);
            acstmt.executeUpdate(SQL);           
        }catch (Exception e){
                errorPrint(e);
            }
    }
            
    
    public static String APICall(String url){        
        try {
            URL urlcon = new URL("https://api.shootwinners.com/api/ShootWinners/" + url);
            HttpsURLConnection conn = (HttpsURLConnection) urlcon.openConnection();
            conn.setRequestMethod("GET");            
            String apiKey = "eYdBnE7pKzYUeVKfEX799gDhS0ji9NtHx5Xv";            
            conn.setRequestProperty("ApiKey", apiKey);
            int status = conn.getResponseCode();
            
            switch(status){
                case 200:
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null){
                        sb.append(line+"\n");
                    }
                    br.close();
                    return sb.toString();
            }
            
        } catch (Exception e){
            logger.debug(e.getMessage());
            return "";
        }        
        return "";
    }
    
    public static boolean APIPost(JSONObject json, String url) throws IOException{
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost("https://api.shootwinners.com/api/ShootWinners/" + url);
            StringEntity params = new StringEntity(json.toString());
            request.addHeader("content-type", "application/json");
            request.addHeader("ApiKey", "eYdBnE7pKzYUeVKfEX799gDhS0ji9NtHx5Xv");
            request.setEntity(params);
            httpClient.execute(request);
        } catch (Exception ex) {
            logger.debug("Error doing API Post");
            return false;
        } finally {
            httpClient.close();
        }
        return true;
    }
    
    public static void WebActivation(String newDB, String UUID, String serialNum){
    
        ResultSet rsClient;
        ResultSet rsActivations;
        int curActivations = 0;
        int availActivations = 0;
        boolean SMSAllowed = false;
        String machName = "Unknown";
        JSONObject obj = new JSONObject();
        Progress.showProgress("Activation","Verifying Activation  ", 0);
        try {
            logger.debug("Query Web server for activation");
            if (!serialNum.equals("Demo")){  //no need to check available activations if they are using a demo
                machName = getMachineName();
                logger.debug("Checking for Serial Number activations allowed");
                String result = APICall("useractivation?serial=" + serialNum);
                if (result != "") {
                    obj = new JSONObject(result);                
                    availActivations = obj.getInt("allowedAct");
                    SMSAllowed = obj.getBoolean("sms");
                    obj = new JSONObject(APICall("usercurrentactivation?serial=" + serialNum + "&machineId=" + UUID));                    
                    curActivations = obj.getInt("total");
                    if (curActivations < availActivations){
                        if (SMSAllowed){
                            obj = new JSONObject(APICall("getsmsactivations?serial=" + serialNum));
                            SQL = "Update machineID set SMS = true, Account_SID = '" + obj.getString("account_SID")
                                    + "', auth_token = '" + obj.getString("authToken") + "', SMSDate = '" 
                                        + LocalDate.now() + "', PhoneNum = '" + obj.getString("phoneNum") + "' where uniqueID = '" + UUID + "'";
                                logger.debug("Setting SMS activation locally");
                                Shoot_Winners.mnSMS.setEnabled(true);
                                acstmt.executeUpdate(SQL);                            
                        } else {
                            SQL = "Update machineID set SMS = false, Account_SID = '0', auth_token = '0' "
                                    + "where uniqueID = '" + UUID + "'";
                            logger.debug("Disabling SMS locally");
                            Shoot_Winners.mnSMS.setEnabled(false);
                            acstmt.executeUpdate(SQL);
                        }              
                        logger.debug("Activating program");
                        obj = new JSONObject(APICall("machineexist?machineId=" + UUID));
                        if (obj.getInt("exist") != 0) {  //records exist so just update
                            logger.debug("Activated so update");
                            System.out.println("Updating current Client record");                           
                            //Get the current date in timestamp format
                            Calendar calendar = Calendar.getInstance();
                            java.sql.Date curDate = new java.sql.Date(calendar.getTime().getTime());
                            obj = new JSONObject();
                            obj.put("serial", serialNum);
                            obj.put("curDate", curDate);
                            obj.put("activated", true);
                            obj.put("machName", machName);
                            obj.put("machId", UUID);
                            if (APIPost(obj, "upsertClient") != true){
                                logger.debug("There was a problem activating client");
                            }
                            frame.setTitle(Constants.programTitle);
                        } else {  //no records exist so insert
                            //Activations available so put this machine in the Clients table                            
                            logger.debug("Adding to Client table");
                            //Get the current date in timestamp format
                            Calendar calendar = Calendar.getInstance();
                            java.sql.Date curDate = new java.sql.Date(calendar.getTime().getTime());
                            obj = new JSONObject();
                            obj.put("serial", serialNum);
                            obj.put("curDate", curDate);
                            obj.put("activated", true);
                            obj.put("machName", machName);
                            obj.put("machId", UUID);
                            if (APIPost(obj, "InsertClient") != true){
                                logger.debug("Error inserting Client");
                            }                               
                        }
                        Timestamp t = new Timestamp(System.currentTimeMillis());
                        Shoot_Winners.deactivate.setEnabled(true);
                        //update local database to activated
                        SQL = "Update machineID set serialNum = '" + serialNum + "', whenAdded = '" + t 
                                + "', timesrun = 5, isactivated = true where uniqueID = '" + UUID + "'";
                        acstmt.executeUpdate(SQL);
                        } else {
                            //No activations available, there are multiple causes for this but we need to deactivate the local database and the Clients table 
                            logger.debug("Deactivating Clients");
                            Calendar calendar = Calendar.getInstance();
                            LocalDate curDate = LocalDate.now();      
                            obj = new JSONObject();
                            obj.put("serial", serialNum);
                            obj.put("curDate", curDate);
                            obj.put("activated", true);
                            obj.put("machName", machName);
                            obj.put("machId", UUID);
                            if (APIPost(obj, "upsertClient") != true){
                                logger.debug("Error inserting Client");
                            }                              
                            logger.debug("Deactivating Local");
                            SQL = "Update machineID set isactivated = false, serialnum = 'Demo', timesrun = 0";
                            updateMachineID(SQL);
                            JOptionPane.showMessageDialog(null, "No Activations available\nIf you feel this is in error contact " + Constants.email
                                    + "\nID: " + UUID,
                                    "Cannot Activate!", JOptionPane.ERROR_MESSAGE);
                            System.exit(0);
                        }  
                } else {
                    logger.debug("Serial Number was not found so cannot activate!");
                    quitProgram();
                }                        
            } else if(newDB.equals("Yes")) {  //this is a demo with a new database, check for UUID if available then deactivate program
                obj = new JSONObject(APICall("machineexist?machineId=" + UUID));
                if (obj.getInt("exist") == 0) {    //no records so proceed
                    logger.debug("New demo adding to Database");
                    //Get the current date in timestamp format
                    Calendar calendar = Calendar.getInstance();
                    java.sql.Date curDate = new java.sql.Date(calendar.getTime().getTime());
                    obj = new JSONObject();
                    obj.put("serial", "Demo");
                    obj.put("curDate", curDate);
                    obj.put("activated", true);
                    obj.put("machName", "Demo");
                    obj.put("machId", UUID);
                    if (APIPost(obj, "InsertClient") != true){
                        logger.debug("Error inserting Client");
                    }                     
                } else {  //This computer has already had a demo so quit
                    logger.error("Computer already had demo or local DB was deleted!");                  
                    quitProgram();
                }
            } else {  //this is a demo with an untampered database, if UUID exists then continue else add UUID
                obj = new JSONObject(APICall("machineexist?machineId=" + UUID));
                logger.debug("Continuing Demo");
                if (obj.getInt("exist") == 0) {    //no records so proceed
                    //Get the current date in timestamp format
                    Calendar calendar = Calendar.getInstance();
                    java.sql.Date curDate = new java.sql.Date(calendar.getTime().getTime());
                    obj = new JSONObject();
                    obj.put("serial", "Demo");
                    obj.put("curDate", curDate);
                    obj.put("activated", true);
                    obj.put("machName", "Demo");
                    obj.put("machId", UUID);
                    if (APIPost(obj, "InsertClient") != true){
                        logger.debug("Error inserting Client");
                    }                   
                }               
            }
        } catch (Exception e){            
            errorPrint(e);
            quitProgram();
        }       
        Progress.closeProgress();           
    }
    
    public static void DeActivate(){
        String machName = "Unknown";
        //Remove activation from local and remote database
        try {
            SQL = "Select * from machineid";
            rsMachineID = acstmt.executeQuery(SQL);
            if (rsMachineID.next()){
                logger.debug("Deactivating Clients");
                machName = getMachineName();
                Calendar calendar = Calendar.getInstance();
                java.sql.Date curDate = new java.sql.Date(calendar.getTime().getTime());   
                JSONObject obj = new JSONObject();
                obj.put("serial", rsMachineID.getString("serialnum"));
                obj.put("curDate", curDate);
                obj.put("activated", false);
                obj.put("machName", machName);
                obj.put("machId", rsMachineID.getString("uniqueid"));
                if (APIPost(obj, "upsertClient") != true){
                    logger.debug("Error inserting Client");
                }   
                logger.debug("Deactivating Local");
                SQL = "Update machineID set isactivated = false, serialnum = 'Demo', timesrun = 0";
                //update local
                updateMachineID(SQL);
                System.exit(0);                         
            }
//            rsMachineID.close();
        } catch (Exception e){
            errorPrint(e);
        }        
    }
    
    static void quitProgram() {         
        try {
            SQL = "Update machineID set timesrun = 0";
            acstmt.executeUpdate(SQL);  
        }catch (Exception e){
            errorPrint(e);
        }
        JOptionPane.showMessageDialog(null, "The program has been disabled pending activation\nPlease activate or contact tech support: " 
                + Constants.email, "Activation issue",JOptionPane.ERROR_MESSAGE);
        System.exit(1);           
    }
    
    public static String getMachineName() {        
        try {
            java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
            logger.debug("Hostname of local machine: " + localMachine.getHostName());
            //System.out.println("Hostname of local machine: " + localMachine.getHostName());    
            return localMachine.getHostName();            
        } catch (UnknownHostException err) {
            logger.error("Error getting hostname.");
            //System.out.println("Error getting hostname.");
            err.printStackTrace();
            return "Unknown Hostname";
        }
    }
        
    public static String getMachineID() throws Exception{        
        try {
            String GUID = Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Cryptography","MachineGuid");
            return GUID;
        } catch (Exception ex){
            try {
                //Get the processor serial
                SystemInfo systemInfo = new SystemInfo();
                HardwareAbstractionLayer hardwareAbstractionLayer = systemInfo.getHardware();
                //String processorSerialNumber = hardwareAbstractionLayer.getProcessor().getProcessorID();
                String processorSerialNumber = hardwareAbstractionLayer.getComputerSystem().getSerialNumber();
                if(processorSerialNumber.equalsIgnoreCase("default string") 
                        || processorSerialNumber.equalsIgnoreCase("unknown") 
                        || processorSerialNumber.equalsIgnoreCase("")
                        || processorSerialNumber.contains(" ")){
                    processorSerialNumber = hardwareAbstractionLayer.getProcessor().getProcessorID() + getMachineName();                
                }
                return processorSerialNumber;    
            } catch (Exception e){
                errorPrint(e); 
            }
        }
        return "Error";              
    }  
}
