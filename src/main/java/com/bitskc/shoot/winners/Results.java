/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import java.util.ArrayList;
import static com.bitskc.shoot.winners.Database.stmt;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import org.slf4j.LoggerFactory;
/**
 *
 * @author talbers
 */
public class Results extends Shoot_Winners{
    public static int numElOA;
    static org.slf4j.Logger logger = LoggerFactory.getLogger("Results"); 
    static ArrayList<Integer> yardGpsWin = new ArrayList<Integer>();
    static ArrayList<String> yardGps = new ArrayList<String>();
    public static void getResults(String strResult){          
        //Current ATA rules state that category shooters are only eligible for Champion
        //If that changes then you need to increase numELOA to the correct number of OA places
        //a category shooter is eligible for 
        numElOA = 1;    //POSSIBLE CHANGE if the ATA ever allows more than just Champion    
        int Win = 0;
        //Get the OA Winners
        if (Integer.parseInt(cmbOA.getSelectedItem().toString()) != 0){
            OAWinners.getOAWinners(strResult, numElOA, "Champion");  //Send the number of winners that are eligible for the OA, this could change
            Win++;
        }
        //Get Category Winners
        if (Integer.parseInt(cmbCat.getSelectedItem().toString()) != 0){
            CatWinners.getCatWinners(strResult);  
            Win++;
        }
        //Get Runner up+ assuming it is needed 
        if (Integer.parseInt(cmbOA.getSelectedItem().toString()) > numElOA){    //only do this when we have more OA winners 
            OAWinners.getOAOther(strResult, Integer.parseInt(cmbOA.getSelectedItem().toString()) - numElOA);
            Win++;
        }     
        //Get Class Winners
        if (pnlClassD.getComponentCount() > 0){   //This is singles or doubles so do class 
            if (Integer.parseInt(cmbClass.getSelectedItem().toString()) != 0){    //only do if we have class winners
                ClassYardWin.getClassWinners(strResult);
                Win++;
            }
        } else if (Integer.parseInt(cmbYard.getSelectedItem().toString()) != 0){    //only do if we have yardage winners
            //get the yardage groups
            yardGps.clear();
            yardGps.add(cmbYard1.getSelectedItem().toString());
            yardGps.add(cmbYard2.getSelectedItem().toString());
            yardGps.add(cmbYard3.getSelectedItem().toString());
            yardGps.add(cmbYard4.getSelectedItem().toString());
            yardGps.add(cmbYard5.getSelectedItem().toString());
            //get yardage group winners
            yardGpsWin.clear();
            yardGpsWin.add(Integer.parseInt(cmbWinYard1.getSelectedItem().toString()));
            yardGpsWin.add(Integer.parseInt(cmbWinYard2.getSelectedItem().toString()));
            yardGpsWin.add(Integer.parseInt(cmbWinYard3.getSelectedItem().toString()));
            yardGpsWin.add(Integer.parseInt(cmbWinYard4.getSelectedItem().toString()));
            yardGpsWin.add(Integer.parseInt(cmbWinYard5.getSelectedItem().toString()));
            ClassYardWin.getYardWinners(strResult, yardGps, yardGpsWin);
            Win++;
        }
        if (Win != 0 && strResult.equals("") || Win != 0 && strResult.contains("Out")){  //we should have some winners so display the report selection
            if (!Constants.ACCOUNT_SID.equals("")){SMS.addSMStoDB();}   //Add text alerts if available
            //Check to see if automated export is selected
            while (Constants.threeSExport && threeSLink.threeS()) {
                //<editor-fold defaultstate="collapsed">                  
                //Export Results to results.csv for 3S Link
                try {                    
                    String SQL;
                    //Clear any existing report for this event, if this fails then exit
                    String eventNum = Constants.eventNum;
                    switch (eventNum){
                        case "HAA":
                            eventNum = "26";
                            break;
                        case "HOA":
                            eventNum = "25";
                            break;
                    }
                    if (!threeSLink.ExecLink("CLEARREPORT", eventNum)){break;}                    
                    //Delete the results if they exist
                    File results = new File("results.csv");
                    if (results.exists()){results.delete();}   
                    //Different call if this is a zone or state shoot to include the state info
                    String escape = " - ";
                    if (cmbState.getSelectedItem() != "" || cmbZone.getSelectedItem() != ""){       //State or Zone Shoot
                        SQL = "CALL SYSCS_UTIL.SYSCS_EXPORT_QUERY('Select Membernum, Name, Score, Place, ResultType || '' - '' || Extra, " 
                                + "ClassorYardage, State, Category from Results order by ResultType, PrintOrder','results.csv', ',', null, null)"; 
                    } else {
                        SQL = "CALL SYSCS_UTIL.SYSCS_EXPORT_QUERY('Select Membernum, Name, Score, Place, Extra, ClassorYardage, " 
                                + "State, Category from Results order by PrintOrder','results.csv', ',', null, null)";              
                    }                       
                    stmt.executeUpdate(SQL);
                    //Load the file into a String
                    String f = new String(Files.readAllBytes(Paths.get("results.csv")),"UTF-8");
                    //Remove all double quotes
                    f = f.replace("\"","");
                    //Delete the current file
                    File rs = new File("results.csv");
                    rs.delete();
                    //Write the new file
                    PrintWriter c = new PrintWriter("results.csv", "UTF-8");      
                    c.print(f);
                    c.close();      
                    //Run the 3S Link program
                    //Add event to 3S
                    threeSLink.ExecLink("POSTREPORT", eventNum + ",results.csv");
                    JOptionPane.showMessageDialog(null, "Report posted to 3S.");
                    break;
                } catch (SQLException ex) {
                    logger.error("3S link, unable to export results.");
                    logger.error(ex.getMessage());
                    //Logger.getLogger(Results.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    logger.error("3S link, file error.");
                    logger.error(ex.getMessage());
                    //Logger.getLogger(Results.class.getName()).log(Level.SEVERE, null, ex);
                }
                //</editor-fold>                
            } 
            //Automated Export not selected so display report
            new PickReport();                  
            
        }
    }
}
