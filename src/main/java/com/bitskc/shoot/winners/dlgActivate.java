/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.PlainDocument;

/**
 *
 * @author talbers
 */
public class dlgActivate {
   
    static JTextField txt;
    static JDialog dlg;
    
    public dlgActivate(){
        Constants.strSerial = "";
        dlg = new JDialog(Shoot_Winners.frame, "Activate", true);
        dlg.setTitle("Serial Number");
        dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        JPanel pnl = new JPanel();
        pnl.setLayout(new GridBagLayout());
        GridBagConstraints gbd = new GridBagConstraints();
        gbd.gridx = 0;
        gbd.gridy = 0;
        gbd.fill = GridBagConstraints.HORIZONTAL;
        //add label
        JLabel lbl = new JLabel("Enter Serial Number (leave blank for Demo): ");
        pnl.add(lbl, gbd);
        //add textbox
        txt = new JTextField(25);
        JPopupMenu rightclk = new JPopupMenu();
        JMenuItem item = new JMenuItem(new DefaultEditorKit.PasteAction());
        item.setText("Paste");
        rightclk.add(item);
        txt.setComponentPopupMenu(rightclk);        
        gbd.gridx = 1;
        pnl.add(txt);
        //This limits the number of characters to 36 
        txt.setDocument(new JTextFieldLimit(36));
        //Ok button
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener(){      
            public void actionPerformed(ActionEvent e){
                Constants.strSerial = txt.getText();
                dlg.dispose();
            }});
        pnl.add(btn);
        dlg.add(pnl);
        dlg.pack();
        //dlg.setSize(200, 60);
        dlg.setLocationRelativeTo(Shoot_Winners.frame); 
        dlg.setResizable(false);
        dlg.setVisible(true);         
    } 
    
    //This is what is used to limit the character count to 36
    class JTextFieldLimit extends PlainDocument {
        private int limit;
        JTextFieldLimit(int limit) {
          super();
          this.limit = limit;
        }

        JTextFieldLimit(int limit, boolean upper) {
          super();
          this.limit = limit;
        }

        public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
          if (str == null)
            return;

          if ((getLength() + str.length()) <= limit) {
            super.insertString(offset, str, attr);
          }
        }
      }

}
