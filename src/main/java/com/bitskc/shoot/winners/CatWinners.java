/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Database.stmt;
import static com.bitskc.shoot.winners.Shoot_Winners.pnlCatD;
import java.awt.Component;
import org.slf4j.LoggerFactory;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JSpinner;
/**
 *
 * @author talbers
 */
public class CatWinners {
    
    static org.slf4j.Logger logger = LoggerFactory.getLogger("CatWinners"); 

    public static void getCatWinners(String strResult){      
        Statement stWin;
        ResultSet rsWin;
        ResultSet rsCount;    
        String Extra, SQL;
        int PrintOrder;
        int lastScore, numWin, curWin, numCount, placesAdded, standbyScore, curScore;        
        try {
            stWin = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            //Loop through the Category panel
            for (Component c: pnlCatD.getComponents()){
                if (c instanceof JSpinner){
                    numWin = (Integer)((JSpinner)c).getValue();
                    PrintOrder = 39995;
                    curScore = 0;
                    if (numWin > 0) {    //Only do for categories that have winners
                        //Figure out if the results table has any category shooters
                        //if so determine if they are eligible.  
                        //If they are still in RAW2 then they are eligible, if they are in Results then they are in a shootoff so we need
                        //to add one to numWin and possibly have standby for this category
                        SQL = "Select count(*) as totalScores from RAW2 join results on raw2.membernum = results.membernum where raw2.category = '" + (c.getName()) + "'";
                        rsWin = stWin.executeQuery(SQL);
                        rsWin.next();
                        placesAdded = rsWin.getInt("totalScores");
                        SQL = "Select count(score) as totalScores, score from RAW2 where Category = '" + (c.getName()) 
                                + "' group by Score order by Score desc";    
                        rsWin = stWin.executeQuery(SQL);    //filter for current category
                        //Get the last score needed
                        curWin = 0;
                        standbyScore = 0;
                        lastScore = 100000;
                        while (rsWin.next() && curWin < numWin) {   //This lets us know how many people are up for the places available
                            curWin = curWin + rsWin.getInt("totalScores");
                            lastScore = rsWin.getInt("Score");
                            if (standbyScore == 1){
                                standbyScore = rsWin.getInt("Score");                                  
                            }
                            if (curWin >= numWin && placesAdded !=0 && standbyScore == 0){
                                curWin = curWin - placesAdded;  //Get us the extra places needed for people in shootoffs
                                standbyScore = 1;   //If we go through the loop again then we are getting standby scores, if not there are no standby scores                           
                            }
                        }
                        if (curWin < numWin){   //This should only happen if we have more places than people, if that happens remove all scores as all won
                            lastScore = 0;
                        }
                        //the current record is the last one eligible for this, any scores less would be gauranteed a trophy
                        //and should be removed from RAW2
//                        lastScore = rsWin.getInt("Score");
                        PrintOrder = PrintOrder + (Shoot_Winners.strCatPrint.indexOf(c.getName()) * 150);
//                        SQL = "Select * from RAW2 where Score >= " + lastScore + " and Category = '" + (c.getName()) + "' order by Score desc, squad";
                        SQL = "Select id, state, squad, membernum, name, category, classoryardage, shotat, one, two, three, four, five, six, seven, eight, score, "
                            + "(Select count(score) from raw2 as r where Score >= " + lastScore + " and Category = '" + (c.getName()) + "' and r.score = raw2.score) as totalscores "
                            + "from RAW2 where Score >= " + lastScore + " and Category = '" + (c.getName()) + "' order by Score desc, squad";   
                        rsWin = stWin.executeQuery(SQL);     
                        while (rsWin.next()){
                            if (rsWin.getInt("Score") != curScore){
                                curScore = rsWin.getInt("Score");
                                PrintOrder = PrintOrder + 5;
                            } 
/*                            
                            rsCount = stmt.executeQuery("Select Count(Score) as totalScores from RAW2 where Score = " 
                                    + rsWin.getInt("Score") + " and Category = '" + (c.getName()) + "'");
                            rsCount.next();
                            numCount = rsCount.getInt("totalScores");
*/
                            numCount = rsWin.getInt("totalScores");
                            //put these people in the results table, if their name is already in results then mark Extra as "Pending above Shootoff"
                            rsCount = stmt.executeQuery("Select count(*) as InShootoff from Results where membernum = '" + rsWin.getString("membernum").toString() + "'");
                            rsCount.next(); 
                            if (rsCount.getInt("InShootoff") > 0){
                                Extra = "Pending OA " + Shoot_Winners.cmbShootOff.getSelectedItem().toString();
                            } else if (rsWin.getInt("Score") <= standbyScore){
                                Extra = "Category Standby";
                            } else if (numCount > 1) {
                                Extra = Shoot_Winners.cmbShootOff.getSelectedItem().toString();
                            } else {
                                Extra = "";
                            }
                            stmt.executeUpdate("Insert into Results (EventName, Place, ResultType, State, Squad, MemberNum, Name, Category, ClassorYardage, ShotAt, one, two, three, four, "
                                    + "five, six, seven, eight, score, Extra, PrintOrder) values ('" + Shoot_Winners.strEvent + "', '" + c.getName() + "', '" + strResult + "', '" 
                                    + rsWin.getString("State") + "', '" + rsWin.getString("Squad") 
                                    + "', '" + rsWin.getString("MemberNum") + "', '" + rsWin.getString("Name").replaceAll("'","\\\'\\\'") + "', '" + rsWin.getString("Category") + "', '" 
                                    + rsWin.getString("ClassorYardage") + "', " + rsWin.getInt("Shotat") + ", " + rsWin.getInt("One") + ", " + rsWin.getInt("two") 
                                    + ", " + rsWin.getInt("Three") + ", " + rsWin.getInt("four") + ", " + rsWin.getInt("five") + ", " + rsWin.getInt("six") 
                                    + ", " + rsWin.getInt("seven") + ", " + rsWin.getInt("eight") + ", " + rsWin.getInt("score") + ", '" + Extra + "', " + PrintOrder + ")");  
                        }
                        if (standbyScore > 1){ //This means we have standby shooters so remove all scores above this
                            SQL = "Delete from RAW2 where score > " + standbyScore + " and Category = '" + (c.getName()) + "'";
                        } else {    //We don't have any standby scores, the last score has either won something or in a shootoff so they are no longer eligible
                            SQL = "Delete from RAW2 where score >= " + lastScore + " and Category = '" + (c.getName()) + "'";
                        }
                        stmt.executeUpdate(SQL);    //Remove ineligible people
                        logger.debug(c.getName() + " done!");
                    } 
                }
            }
        } catch (Throwable e) {
            logger.error("Error getting Category Winners " + e.getMessage());
            errorPrint(e);
        }
    }
}