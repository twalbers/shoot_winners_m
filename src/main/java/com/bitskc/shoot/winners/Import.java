/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Database.stmt;
import static com.bitskc.shoot.winners.Shoot_Winners.frame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.slf4j.LoggerFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 *
 * @author talbers
 */
public class Import {

    static String defaultDirectory = System.getProperty("user.home");  
    //static String fileName;
    static File[] fileNames;
    static List<File> fileNames2 = new ArrayList<>();
    static org.slf4j.Logger logger = LoggerFactory.getLogger("Import");
    static JDialog dialog;
    
    public static void closeDialog() {        
        dialog.dispose();        
    }
    public static boolean ImportData(){
        //Clean out RAW database
        try {
            if (!Constants.secondEvnt){
                stmt.executeUpdate("Delete from RAW");
                stmt.executeUpdate("Delete from RAW2");                
            } else {
                //Move Raw to RAW2
                stmt.executeUpdate("Delete from RAW2");  
                stmt.executeUpdate("insert into RAW2 (STATE,SQUAD,MEMBERNUM,NAME,CATEGORY,CLASSORYARDAGE,SHOTAT,ONE,TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,SCORE) Select STATE,SQUAD,MEMBERNUM,NAME,CATEGORY,CLASSORYARDAGE,SHOTAT,ONE,TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,SCORE from RAW");
                stmt.executeUpdate("Delete from RAW");                 
            }
        }
        catch (Throwable e) {
            errorPrint(e);
            return false;
        }   
        //If we are using a link do it here
        while (threeSLink.threeS() && Constants.threeSImport) {
            //Get the number of events
            int numEvents = threeSLink.Events();
            //If the following is true there appears to be an issue so exit automated link and ask them to pick a file.
            if (numEvents == 0){break;}
            //Get Tournament Name
            if (threeSLink.ExecLink("GETTOURNAMENTNAME", "")){
                Constants.shootName = threeSLink.SngleResult().trim();
            } 
            //Display form to pick event and create results file
            new threeSLink(numEvents);
            //Write the Event name at the top and put in a couple of blank lines
            try {
                String f = new String(Files.readAllBytes(Paths.get("results.csv")),"UTF-8");
                File rs = new File("results.csv");
                rs.delete();
                //Write new file
                PrintWriter c = new PrintWriter("results.csv", "UTF-8");
                c.print(Constants.shootName + "    " + Constants.eventName + "\n\n\n");
                c.print(f);
                c.close();
                //Add the file to the list
                File[] rx = new File[1];
                rx[0] = rs;
                fileNames = rx;
            } catch (FileNotFoundException ex) {
                errorPrint(ex);
                return false;
            } catch (IOException ex) {
                errorPrint(ex);
                return false;
            } 
            //Import the results.csv file to Raw
            if (!Open_Import_File(fileNames, true)){
                logger.error("Could not import file");
                JOptionPane.showMessageDialog(null, "Could not import file, is it in the correct format?",
                            "Error!",JOptionPane.ERROR_MESSAGE);                
                return false;
            } else {   
                //Get the count of how many were in the event
                try {
                    ResultSet rsCount ;
                    rsCount = stmt.executeQuery("Select Count(*) as count from Raw");
                    rsCount.next();  
                    Shoot_Winners.strEvent = Shoot_Winners.strEvent + " - " + rsCount.getInt("Count") + " Entries"; 
                } catch (Throwable e) {
                    errorPrint(e); 
                }
                    return true;
            }                        
        } 
        //Select the file you want to import        
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setCurrentDirectory(new File(defaultDirectory));
        fileChooser.setDialogTitle("Select File");
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Files: (.txt & .csv)", "txt", "csv"));
        fileChooser.setAcceptAllFileFilterUsed(false);    
        int result = fileChooser.showOpenDialog(frame);
        if (result == JFileChooser.APPROVE_OPTION){
            //defaultDirectory = fileChooser.getSe
            File selectedFile = fileChooser.getSelectedFile();
            //Set the default path for FileChooser to current location
            defaultDirectory = selectedFile.getParent();
            fileNames = fileChooser.getSelectedFiles();           
            //Import the selected file to Raw
            if (!Open_Import_File(fileNames, false)){
                logger.error("Could not import file");
                JOptionPane.showMessageDialog(null, "Could not import file, is it in the correct format?",
                            "Error!",JOptionPane.ERROR_MESSAGE);                
                return false;
            } else {   
                //Get the count of how many were in the event
                try {
                    ResultSet rsCount ;
                    rsCount = stmt.executeQuery("Select Count(*) as count from Raw");
                    rsCount.next();  
                    Shoot_Winners.strEvent = Shoot_Winners.strEvent + " - " + rsCount.getInt("Count") + " Entries"; 
                } catch (Throwable e) {
                    errorPrint(e); 
                }
                    return true;
            }
        }             

        //something is wrong
        return false;
    }    

    public static boolean Open_Import_File(File[] fileNames, boolean link){
        boolean csvError = false;        
        for (int j = 0; j < fileNames.length; j++){
            Constants.fileName = fileNames[j].toString();           
            //This will get one line at a time
            String line = null; 
            String eventName = "";
            String strHAAorHOA = "";
            try {
                //FileReader reads text files in the default encoding
                FileReader fileReader = new FileReader(Constants.fileName);            
                //Always wrap FileReader in Bufferedreader
                BufferedReader bufferedReader = new BufferedReader(fileReader);                       
                int curLine = 1; 
                while ((line = bufferedReader.readLine()) != null){
                    if (curLine == 1 && j == 0){  //Set the event title but only look at the first file
                        if (fileNames.length > 1){
                            String[] options = {"OK"};
                            JPanel panel = new JPanel();
                            JLabel lbl = new JLabel("File Name : ");
                            JTextField txt = new JTextField(20);
                            panel.add(lbl);
                            panel.add(txt);
                            int selectedOption = JOptionPane.showOptionDialog(frame,panel,"You selected multiple files please enter Event Name: ",JOptionPane.NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE, null, options, null);
                            if (selectedOption == 0 && !txt.getText().isEmpty()) {
                                eventName = txt.getText();
                                eventName = eventName.replace("\'", "");
                                eventName = eventName.replace("\"", "");
                                Shoot_Winners.strEvent = eventName;
                            } else {
                                Shoot_Winners.strEvent = "No event name entered!";
                            }

                        } else {
                            eventName = line;
                            eventName = eventName.replace("\'", "");
                            eventName = eventName.replace("\"", "");
                            Shoot_Winners.strEvent = eventName;                               
                        }
                        if (line.contains("High All Around") || line.contains("High Over All")){
                            strHAAorHOA = "Yes";
                        } else {
                            strHAAorHOA = "No";
                        }
                    }
                    if (curLine > 3){
                        //Put data into table
                        //Split current line                    
                        String[] lineData = line.split(",");
                        //Check to make sure we have the correct amount of Data 16 for normal and 6 for HOA & HAA
                        if (!checkString(lineData.length, strHAAorHOA)){
                            //There was an error with the file, display the line with the error and exit
                            logger.error("Error in import file - " + line);
                            return false;
                        }
                        //If the CSV is formatted correctly it will go state, squad, etc
                        //If it is incorrect the score will be first. The following checks for number, if it is swap 1st and last                        
                        if (Character.isDigit(lineData[0].charAt(0)) && csvError == false){  
                            int answer = JOptionPane.showConfirmDialog(null, "There appears to be an issue with the file, is the first number the total score? \n" 
                                    + line,"File Issue!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                            if (answer == JOptionPane.YES_OPTION){
                                csvError = true;
                            } else csvError = false;
                        };
                        if (csvError){
                            String finalScore = lineData[0];
                            for (int cp = 0; cp < lineData.length-1; cp++){
                                lineData[cp] = lineData[cp+1];
                            }
                            lineData[lineData.length - 1] = finalScore;
                        }
                        //Changed from i = 2 to take care of the ATA number on HAA and HOA
                        for (int i = 1; i < lineData.length; i++){                        
                            if (i == 3 || (i == 2 && strHAAorHOA.equals("Yes"))){
                                lineData[i] = lineData[i].replaceAll("\\s+$","");   //replace all spaces after the name    
                                lineData[i] = lineData[i].replaceAll("'","\\\'\\\'");
                            } else {
                                lineData[i] = lineData[i].replaceAll("\\s+","");
                            }
                        }
                        //insert rows to recordset/database 
                        if (strHAAorHOA.equals("Yes")){
                            stmt.executeUpdate("Insert into RAW (State, Membernum, Name, ClassorYardage, Category, Score) values ('" + lineData[0] + "','" + lineData[1] + "','" + lineData[2]
                                + "','" + lineData[3] + "','" + lineData[4] + "'," + Integer.parseInt(lineData[5]) + ")");
                        } else {
                            stmt.executeUpdate("Insert into RAW (State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, One, "
                                + "Two, Three, Four, Five, Six, Seven, Eight, Score) values ('" + lineData[0] + "','" + lineData[1] + "','" + lineData[2]
                                + "','" + lineData[3] + "','" + lineData[4] + "','" + lineData[5] + "'," + Integer.parseInt(lineData[6]) + "," 
                                + Integer.parseInt(lineData[7]) + "," + Integer.parseInt(lineData[8]) + "," + Integer.parseInt(lineData[9]) + "," 
                                + Integer.parseInt(lineData[10]) + "," + Integer.parseInt(lineData[11]) + "," + Integer.parseInt(lineData[12]) + "," 
                                + Integer.parseInt(lineData[13]) + "," + Integer.parseInt(lineData[14]) + "," + Integer.parseInt(lineData[15]) + ")");
                        }
                    }
                    curLine ++;
                }            
                //Make sure to close the file
                bufferedReader.close();
//                return true;
            }
            catch(FileNotFoundException ex) {
                logger.error("Unable to open file '" + Constants.fileName + "'");
                return false;
            }
            catch(IOException ex){
                logger.error("Error reading file '" + Constants.fileName + "'");
                return false;
            }
            catch(Throwable e){
                errorPrint(e);
                logger.error(e.getMessage());
                return false;
            }
            //Reset csvError in case we are selecting multiple files, not all may have the issue
            if (csvError){
                logger.info("CSV Format error in '" + Constants.fileName + "', automatically moved final score to the end of the line");
                //Reset
                csvError = false;
            }            
        }

        if (fileNames.length > 1){
            int pos = Constants.fileName.lastIndexOf('\\');
            Constants.fileName = Constants.fileName.substring(0, pos) + "\\" + Shoot_Winners.strEvent;            
        } else if(!link) {
            int pos = Constants.fileName.lastIndexOf(".");
            Constants.fileName = Constants.fileName.substring(0,pos);
        } else {
            Constants.fileName = Constants.eventName;
        }
        logger.info("File path for pdf " + Constants.fileName);
        return true;
    }  
    
    public static boolean checkString(int fileLine, String fileType){
        //Check to see if we are working with a HOA or HAA file
        if (fileLine == 6 || fileLine ==16){
            return true;         
        } else if (fileType.equals("Yes")) {
            JOptionPane.showMessageDialog(null, "Expecting a High Overall File with 6 fields\n The current file has " + fileLine + " fields!",
                "Error!",JOptionPane.ERROR_MESSAGE); 
        } else {
            JOptionPane.showMessageDialog(null, "Expecting an Event File with 16 fields\n The current file has " + fileLine + " fields!",
                "Error!",JOptionPane.ERROR_MESSAGE);            
        }
        return false;
    }
}
