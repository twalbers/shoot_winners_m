/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author talbers
 */
public class CheckUpdate {

    static Logger logger = LoggerFactory.getLogger("CheckUpdate");     
    public CheckUpdate() {       
        String ver = "";
        String[] httpVer;
        int ans = JOptionPane.NO_OPTION;
        logger.debug("Checking for update.");
        try { 
            URL updateURL = new URL("https://www.shootwinners.com/ver.txt"); // new URL("http://ata.albersmo.com/prog/shootwinners/ver.txt");

            BufferedReader in = new BufferedReader(
            new InputStreamReader(updateURL.openStream()));

            String inputLine;
            inputLine = in.readLine();
            if (inputLine != null){
                ver = inputLine;
            }
            in.close();
            httpVer = ver.split("\\.");
            //Parse the version text
            String[] strcurVer;
            strcurVer = Constants.programTitle.substring(Constants.programTitle.indexOf("v")+1).split("\\.");
            for (int i = 0; i < httpVer.length; i++){
                //If we are running a newer version than is on the web don't do anything
                if (Integer.parseInt(httpVer[i]) < Integer.parseInt(strcurVer[i])){
                    break;
                } else if (Integer.parseInt(httpVer[i]) > Integer.parseInt(strcurVer[i])){ //We have a new version
                    ans = JOptionPane.showConfirmDialog(Shoot_Winners.frame, "There is an update! Do you want to download it?", 
                            "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                }
                if (ans == JOptionPane.YES_OPTION){                
                    String fileURL = "";
                    String saveDir = "";
                    if (Constants.vers.indexOf("windows") != -1 && !Constants.vers.equals("")){
                        fileURL = "https://f002.backblazeb2.com/file/BITSFileShare/Shoot+Winners.exe";  // "http://ata.albersmo.com/prog/shootwinners/Shoot%20Winners.exe";
                        String home = System.getProperty("user.home");
                        saveDir = home + "/Downloads/";
                    } else if (Constants.vers.indexOf("Mac") != 1 && !Constants.vers.equals("")){
                        fileURL = "http://ata.albersmo.com/prog/shootwinners/Shoot%20Winners.pkg";
                        String home = System.getProperty("user.home");
                        saveDir = home + "/Downloads/"; 
                    } else {
                        break;  //unhandled Linux so exit
                    }
                    logger.debug("Update found and wanted so starting download!");
                    try {
                        Shoot_Winners.frame.setTitle(Constants.programTitle + " - Downloading update!");
                        HttpDownloadUtility.downloadFile(fileURL, saveDir);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }                
                    break;
                }
            }
            logger.debug("No update found");
        }catch (Throwable e){
            logger.error("Error in CheckUpdate");
            logger.error(e.getMessage());
        }
        
    }
    

}
