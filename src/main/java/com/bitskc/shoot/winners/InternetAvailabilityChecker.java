/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author talbers
 */
public class InternetAvailabilityChecker {
    
    static Logger logger = LoggerFactory.getLogger("InternetAvailabilityChecker");
    
    public static boolean isInternetAvailable() throws IOException
    {
        return isHostAvailable("google.com") || isHostAvailable("amazon.com");
    }

    private static boolean isHostAvailable(String hostName) throws IOException
    {
        try(Socket socket = new Socket())
        {
            int port = 80;
            InetSocketAddress socketAddress = new InetSocketAddress(hostName, port);
            socket.connect(socketAddress, 3000);
            return true;
        }
        catch(UnknownHostException unknownHost)
        {
            return false;
        }
    }

    public static boolean isMySQLAvailable(String host) {
        //System.out.println("--------------Testing MySQL connectivity");
        logger.debug("--------------Testing SQL connectivity");
        Socket s = null;
        try {
            s = new Socket(host, 15684);

            // If the code makes it this far without an exception it means
            // something is using the port and has responded.
            //System.out.println("--------------Port is not available");
            logger.debug("--------------Port is not available");
            return false;
        } catch (IOException e) {
            //System.out.println("--------------Port is available");
            logger.debug("--------------Port is available");
            return true;
        } finally {
            if( s != null){
                try {
                    s.close();
                } catch (IOException e) {
                    throw new RuntimeException("You should handle this error." , e);
                }
            }
        }
    }
}
