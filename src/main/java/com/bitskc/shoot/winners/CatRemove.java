/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Database.stmt;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.slf4j.LoggerFactory;
import static com.bitskc.shoot.winners.Shoot_Winners.frame;
/**
 *
 * @author talbers
 */
public class CatRemove {
    static org.slf4j.Logger logger = LoggerFactory.getLogger("CatRemove");
    JDialog dialog;    
    public CatRemove(){
        ResultSet rsCat;  //Unique categories
        String SQL;
        int numDSCat;   //how many distinct categories do we have
        GridBagConstraints gbcI = new GridBagConstraints();
        SQL = "Select count(distinct(category)) as totalRows from raw where category is not null and category <> ''";
        //Get the number of unique categories in this shoot
        try {
            rsCat = stmt.executeQuery(SQL);
            rsCat.next();
            numDSCat = rsCat.getInt("totalRows");   //Figure out how many categories we have
            SQL = "Select distinct(category) from raw where category is not null and category <> '' order by category";
            rsCat = stmt.executeQuery(SQL); //fill out recordset with distinct categories
            rsCat.next();
        } catch (Throwable e){
            errorPrint(e);
            logger.error(e.getMessage()); 
            return;
        }
        //Now we have the categories involved display them on a form
        dialog = new JDialog(Shoot_Winners.frame, "Categories to Remove", true);
        dialog.setLayout(new GridBagLayout());
        gbcI.gridx = 0;
        gbcI.gridy = 0;
        gbcI.weightx = 1;
        gbcI.weighty = 1;
        gbcI.fill = GridBagConstraints.HORIZONTAL;
        //Setup top Panel
        JLabel lbl = new JLabel("<html>Select any categories that are not<br><center>eligible for trophies!</center></html>");
        lbl.setFont(new Font("Serif", Font.BOLD, 16));
        JPanel topPnl = new JPanel();
        topPnl.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        //topPnl.setLayout(new GridBagLayout());
        //topPnl.setPreferredSize(new java.awt.Dimension(120,30));
        topPnl.add(lbl);
        dialog.getContentPane().add(topPnl, gbcI);  
        //Now setup center panel with checkboxes
        JPanel cntrPnl = new JPanel();
        cntrPnl.setLayout(new GridBagLayout());
        int x = 0;
        gbcI.gridx = 0;
        gbcI.gridy = 0;
        gbcI.insets = new Insets(0,0,0,0);   //top, left, bottom, right  
        gbcI.ipadx = 0;
        gbcI.ipady = 0;
        gbcI.gridwidth = 1;
        gbcI.anchor = GridBagConstraints.LINE_START;
        JCheckBox ckCat;
        int numCat;
        try {
            for (numCat = 1; numCat <= numDSCat; numCat++){
                ckCat = new JCheckBox();
                gbcI.gridx = x;
                gbcI.gridy = ((numCat-1) / 4);
                ckCat.setName(rsCat.getString("Category"));
                ckCat.setText(rsCat.getString("Category"));     
                cntrPnl.add(ckCat,gbcI);
                rsCat.next();
                x++;
                if (x == 4)x = 0;                
            }
        } catch (Throwable e){
            errorPrint(e);
            logger.error(e.getMessage()); 
            return;
        }
        if ((numDSCat % 4) != 0){
            numCat = (numDSCat / 4 + 1) * 25;  //Set the height of the form
        } else {
            numCat = (numDSCat / 4) * 25;  //Set the height of the form
        }
        cntrPnl.setPreferredSize(new java.awt.Dimension(300,numCat));
        gbcI.gridx = 0;
        gbcI.gridy = 1;
        gbcI.insets = new Insets(0,5,0,0);   //top, left, bottom, right  
        dialog.getContentPane().add(cntrPnl, gbcI); 
        JPanel btmPanel = new JPanel();
        JButton btnOK = new JButton();
        btnOK.setText("Continue");
        btnOK.setPreferredSize(new Dimension(150,40));
        btnOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) 
            {       
                //Build a string of categories to remove
                String SQL = "Delete from RAW where Category in ("; 
                String ckCheck = "";
                Component[] components = cntrPnl.getComponents();  //Get a list of components in panel
                for (Component c : components) {
                    if (((JCheckBox)c).isSelected()) {
                        ckCheck = ckCheck + "'" + ((JCheckBox)c).getName().toString() + "',";                       
                    }                    
                }    
                if (ckCheck != ""){
                    SQL = SQL + ckCheck.substring(0,ckCheck.length() - 1) + ")";
                    try {
                        stmt.executeUpdate(SQL);  //remove selected categories
                    } catch (Throwable e){
                        errorPrint(e);
                        logger.error(e.getMessage()); 
                    } finally {
                        dialog.setVisible(false);
                    }                    
                } else dialog.setVisible(false);                
            }
        }) ;                 
        btmPanel.add(btnOK);
        gbcI.gridy = 2;
        dialog.getContentPane().add(btmPanel, gbcI);
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dialog.setSize(300, numCat);
        dialog.setLocationRelativeTo(frame); 
        dialog.pack();
        dialog.setResizable(false);
        dialog.setVisible(true);
    }    
    public void closeDialog() {        
        dialog.dispose();        
    }
}
