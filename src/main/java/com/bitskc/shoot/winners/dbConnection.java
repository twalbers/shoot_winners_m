/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import entitymanager.Entity;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author talbers
 */
public class dbConnection {  
    static Logger logger = LoggerFactory.getLogger("dbConnection");
    public static String connectionURL;
    public static Statement stmt;
    public static Entity entity = new Entity();
    
    public dbConnection(){
        logger.debug("Loading connection properties");
        try{
        //    Progress.showProgress("Preparing","Getting program ready!");
            Constants.pgmInfo1 = entity.getEntity("pgmInfo1");
            Constants.pgmInfo2 = entity.getEntity("pgminfo2");
            connectionURL = "jdbc:derby:" + Constants.dbName + ";create=true;user=" + Constants.pgmInfo1 + 
                    ";password=" + Constants.pgmInfo2;
        } catch (Throwable e){
            logger.error("Cannot load connection properties");
            System.exit(0);
        }         
        Constants.conn = null;    
        // Load the driver
        try {
            Class.forName(Constants.driver);
            logger.debug(Constants.driver + " loaded.");
        } catch (java.lang.ClassNotFoundException e) {
            logger.debug("ClassNotFoundException: ");
            logger.debug(e.getMessage());
            logger.debug("\n Make sure your CLASSPATH variable " +
                "contains %DERBY_HOME%\\lib\\derby.jar (${DERBY_HOME}/lib/derby.jar). \n");
            logger.debug("ClassNotFoundException: ");
        }    
        try {
            logger.debug("Trying to connect to " + Constants.dbName);
            //System.out.println("Trying to connect to " + dbName);
            Constants.conn = DriverManager.getConnection(connectionURL);
   //         stmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);    
        } catch (Throwable e){
            
        }
}
}
