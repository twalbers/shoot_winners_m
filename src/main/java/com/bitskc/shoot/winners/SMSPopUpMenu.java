/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author talbers
 */
public class SMSPopUpMenu extends JPopupMenu {
    
    JMenuItem mnuDelete;
    JMenuItem mnuSend;
    JMenuItem mnuSpace;
    Statement stmt;
    Logger logger = LoggerFactory.getLogger("SMSPopUpMenu"); 
    
    public SMSPopUpMenu(){
        mnuSend = new JMenuItem("Send Texts");
        mnuSend.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //Confirm that they want to send the text alerts
                int dlgResult = JOptionPane.showConfirmDialog(Shoot_Winners.frame, "Confirm that you want to send text alerts for event - "
                        + SMS.lstEvents.getSelectedValue(), "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (dlgResult == 0){
                    int count = SMS.sendSMS(SMS.lstEvents.getSelectedValue());  //send texts
                    JOptionPane.showMessageDialog(Shoot_Winners.frame, count + " texts have been sent", "Complete!", JOptionPane.INFORMATION_MESSAGE);   
                }
            }
        });
        mnuDelete = new JMenuItem("Delete");
        mnuDelete.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //Delete the selected record
                try {
                    int dlgResult = JOptionPane.showConfirmDialog(Shoot_Winners.frame, "Delete - "
                            + SMS.lstEvents.getSelectedValue(), "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (dlgResult == 0){
                        stmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                        String SQL = "Delete from SMS where eventname = '" + SMS.lstEvents.getSelectedValue() + "'";
                        stmt.executeUpdate(SQL);   
                        DefaultListModel model = (DefaultListModel)SMS.lstEvents.getModel();
                        int selectedIndex = SMS.lstEvents.getSelectedIndex();
                        if (selectedIndex != -1){
                            model.remove(selectedIndex);
                        }
                    }                 
                } catch (Throwable er){
                    errorPrint(er);
                } 
            }
        });  
        mnuSpace = new JMenuItem(" ");               
        add(mnuSend);
        add(mnuSpace);
        add(new JSeparator());
        add(mnuDelete);
    }
    
    public void errorPrint(Throwable e) {
        JOptionPane.showMessageDialog(null, "Error in the Texting Module, check log file!", "Error!", JOptionPane.ERROR_MESSAGE);
        if (e instanceof SQLException)
            SQLExceptionPrint((SQLException)e);
        else {
            logger.error("Error in the SMS module");
            logger.error((e).getMessage());
            e.printStackTrace();
        }
    }    
    //  Iterates through a stack of SQLExceptions
    public void SQLExceptionPrint(SQLException sqle) {
        while (sqle != null) {
            logger.debug("\n---SQLException Caught---\n");
            logger.debug("SQLState:   " + (sqle).getSQLState());
            logger.debug("Severity: " + (sqle).getErrorCode());
            logger.debug("Message:  " + (sqle).getMessage());
            sqle.printStackTrace();
            sqle = sqle.getNextException();
        }
    } 
}
