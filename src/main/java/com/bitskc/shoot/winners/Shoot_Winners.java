/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Database.stmt;
import static com.bitskc.shoot.winners.ImportState.defaultDirectory;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;
import static javax.swing.TransferHandler.MOVE;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.MaskFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author talbers
 */
public class Shoot_Winners extends JPanel{
    
    public JTabbedPane pnlTable;
    public JPanel pnlSelect;
//    public JPanel pnlYard = new JPanel();
    static public JPanel pnlYard;
    static public JLabel lblEvent = new JLabel();    
    final static String[] lst = {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"};
    final static public int wide = 900;
    final static public int tall = 700;
    final static public int selWide = 323;    
    
    private GridBagConstraints gbc = new GridBagConstraints();
    static Logger logger = LoggerFactory.getLogger("Shoot_Winners");
    static public JScrollPane pnlCat, pnlClass, pnlPrintCatOrder;
    static public JPanel pnlCatD;
    static public JPanel pnlClassD;
    static public JComboBox cmbOA;
    static public JComboBox cmbCat;
    static public JComboBox cmbClass;
    static public JComboBox cmbYard;
    static public JComboBox<String> cmbShootOff;
    static public JComboBox<String> cmbState, cmbZone, cmbYard1, cmbYard2, cmbYard3, cmbYard4, cmbYard5, cmbWinYard1, cmbWinYard2, cmbWinYard3, cmbWinYard4, cmbWinYard5;
    //JComboBox cmbk; 
    public JPanel pnlRaw;
    static public JTable tblRaw;
    static JFrame frame;
    static public String strEvent;
    static public DefaultListModel<String> categories = new DefaultListModel<String>();
    static public JList<String> dndList;
    static public ArrayList<String>strCatPrint;
    static public String strReport;
    static public JMenu deactivate;
    private static JButton btnWinners;
    static public JMenu mnSMS;
    static public JMenu mnLink;
    static public JMenu mnEdit;
    static public JMenuItem mnAddEvent;
    private static Font fnt = new Font("Arial", Font.PLAIN, 15);
    static public Timer smsTimer;
    public Shoot_Winners(){

        strCatPrint = new ArrayList<String>();
        setLayout(new GridBagLayout());
        //Setup Table panel
        add(TablePnl(0, 0, 1, 1), gbc);
        logger.debug("Adding main tabbed panel to form, Raw Data");        
        //Setup Options panel
        add(SelectPnl(1, 0, 0, 1),gbc);
        logger.debug("Adding Selection panel to form");
        //Setup import Panel
        pnlSelect.add(ImportPnl(0, 0, 0, 0), gbc);
        logger.debug("Finished import panel");
        //Setup Event panel
        pnlSelect.add(EventPnl(0, 1, 0, 0), gbc);  
        logger.debug("Finished Event panel");
        //Add Shoot Options
        pnlSelect.add(OptionPnl(0, 2, 0, 0), gbc);
        logger.debug("Finished Options panel");        
        //Add Winners Master
        pnlSelect.add(WinnersPnl(0, 3, 0, 0), gbc);    
        logger.debug("Finished Winners panel"); 
        pnlSelect.add(ClassPnl(0, 4, 0, 0), gbc);          
        logger.debug("Finished Class panel");         
        pnlSelect.add(CategoryPnl(0, 5, 0, 0), gbc);
        logger.debug("Finished Category panel"); 
        pnlSelect.add(YardPnl(0, 6, 0, 0), gbc);       
        logger.debug("Finished Yardage Group panel");
        pnlSelect.add(FinalPnl(0, 7, 1, 1), gbc);
        logger.debug("Finished Final panel");                 
}        
    private JTabbedPane TablePnl(int x, int y, int w, int h){
    // <editor-fold defaultstate="collapsed">
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.gridwidth = w;
        gbc.gridheight = h;
        gbc.weighty = 1.0;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        pnlTable = new JTabbedPane();
        pnlTable.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(0, 0, 0)));
        pnlTable.setPreferredSize(new java.awt.Dimension(wide,tall));
        tblRaw = new JTable();
        tblRaw.setEnabled(false);   //disable user input
        JScrollPane pnlRaw = new JScrollPane(tblRaw);           
        pnlTable.addTab("Raw Data", pnlRaw);
        return pnlTable;
    }    
    // </editor-fold>               
    private JPanel SelectPnl(int x, int y, int w, int h){
    //<editor-fold defaultstate="collapsed">   
        gbc.gridx = x;  //place one column over
        gbc.gridy = y;
        gbc.weightx = w;    //do not any to x on resize
        gbc.weighty = h;
        gbc.fill = GridBagConstraints.VERTICAL; //only resize vertically
        pnlSelect = new JPanel();
        pnlSelect.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(0, 0, 0)));
        pnlSelect.setPreferredSize(new Dimension(selWide, tall));
        pnlSelect.setMinimumSize(new Dimension(selWide, tall));
        pnlSelect.setLayout(new GridBagLayout());
     //   pnlSelect.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT); 
        return pnlSelect;
    }//</editor-fold>    
    private JPanel ImportPnl(int x, int y, int w, int h){
    //<editor-fold defaultstate="collapsed">
        JPanel pnlImport = new JPanel();
        pnlImport.setPreferredSize(new Dimension((selWide-13),40));
        pnlImport.setLayout(new GridBagLayout());        
        //Add import button to pnlSelect       
        JButton btnImport = new JButton();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;  //Changed to 1.0 from 0 when the save button was removed
        gbc.weighty = 1.0;  //Changed to 1.0 from 0 when the save button was removed      
        gbc.fill = GridBagConstraints.BOTH;
        //Change from 0, 0, 0, 60 when the save button was removed
        gbc.insets = new Insets(0,0,0,0);   //top, left, bottom, right      
        btnImport.setText("Import Shoot");
        btnImport.addActionListener(new ActionListener(){
            @Override
            @SuppressWarnings("ResultOfObjectAllocationIgnored")
            public void actionPerformed(ActionEvent arg0) 
            {       
                DataImport();
//                //Import Data
//                if (Import.ImportData()){
//                    //Only do if we are importing the second event
//                    if (Constants.secondEvnt){                       
//                        try {
//                            //stmt.executeUpdate("merge into RAW r using RAW2 r2 on r.membernum = r2.membernum when matched then update set r.five = r2.one, r.six = r2.two, r.seven = r2.three, r.eight = r2.four");
//                            stmt.executeUpdate("merge into RAW r using RAW2 r2 on r.membernum = r2.membernum when matched then update set r.five = r.one, r.six = r.two, r.seven = r.three, r.eight = r.four, r.one = r2.one, r.two = r2.two, r.three = r2.three, r.four = r2.four when not matched r.five = 0 then update set r.five = r.one, r.six = r.two, r.seven = r.three, r.eight = r.four, r.one = 0, r.two = 0, r.three = 0, r.four = 0 when not matched then insert values (r2.State, r2.Squad, r2.MEMBERNUM, r2.name, r2.category, r2.classoryardage, r2.shotat, r.five = r.one, r.six = r.two, r.seven = r.three, r.eight = r.four, r.one = 0, r.two = 0, r.three = 0, r.four = 0, r.score = 0)");
//                            stmt.executeUpdate("update RAW set Score = (one + two + three + four + five + six + seven + eight)");
//                        } catch (Throwable e) {
//                            errorPrint(e); 
//                        }
//                    }
//                    new CheckCatYard();
//                    //Set Event name
//                    lblEvent.setText("<html>" + strEvent + "</html>");
//                    //Fill out the states
//                    setState();
//                    //Show the form to combine any categories
//                    new CatChange();
//                    //Show the form to remove categories if enabled
//                    if (Constants.imTO){new CatRemove();}
//                    //Show the Class Change form if enabled and possible
//                    if (checkIfClass() && Constants.inClass){new ClassChange();}
//                    //Clear categories panel
//                    pnlCatD.removeAll();  
//                    pnlCatD.revalidate();
//                    pnlCatD.repaint();                    
//                    //Load the categories 
//                    CatDisplay.LoadCat(Integer.parseInt(cmbCat.getSelectedItem().toString()));
//                    //Clear classes panel
//                    pnlClassD.removeAll();  
//                    pnlClassD.revalidate();
//                    pnlClassD.repaint();                    
//                    //Load the classes if necessary
//                    String SQL = "Select classoryardage from raw";
//                    ResultSet rsClass;                 
//                    String s;
//                    try {
//                        rsClass = stmt.executeQuery(SQL);  
//                        rsClass.next();
//                        SQL = rsClass.getString("ClassorYardage");
//                        CH_LOOP:                        
//                        for (int i = 0; i < SQL.length(); i++){
//                            s = SQL.substring(i, i + 1);
//                            if (!s.equals(" ")){  //we don't want a blank character                                
//                                if (!s.matches("[-+]?\\d*\\.?\\d+")) {  //This means that the first character is not numeric so assume class 
//                                    //Setup the Class's used
//                                    ClassDisplay.LoadClass(Integer.parseInt(cmbClass.getSelectedItem().toString()));
//                                }
//                                break CH_LOOP;  //stop the for loop
//                            }
//                        }
//                    }catch (Exception ex) {
//                        errorPrint(ex);
//                    }
//                    //Show raw data
//                    LoadRaw();   
//                    //Enable 2nd Event Import
//                    mnAddEvent.setEnabled(true);
//                }
            }
        }) ; 
        pnlImport.add(btnImport, gbc);
        //Add Save Button 
        JButton btnSave = new JButton();
        gbc.gridx = 1;   
        gbc.insets = new Insets(0,0,0,0);   //top, left, bottom, right               
        btnSave.setText("Save");
        try {
            Image img = ImageIO.read(getClass().getResource("/images/save.png"));
            btnSave.setIcon(new ImageIcon(img));            
        } catch (Exception ex) {
            errorPrint(ex);
        }
        btnSave.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) 
            {       
                // Do save here
            }
        }) ;         
//        pnlImport.add(btnSave);
        btnSave.setEnabled(false); 
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = w;
        gbc.weighty = h;    
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0,5,0,0);   //top, left, bottom, right        
        return pnlImport;
    }
    //</editor-fold>
    private JPanel EventPnl(int x, int y, int w, int h){
    //<editor-fold defaultstate="collapsed">        
        JPanel pnlEvent = new JPanel();
        pnlEvent.setPreferredSize(new Dimension((selWide-13),60));
        pnlEvent.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                "Event", TitledBorder.LEFT, TitledBorder.ABOVE_TOP,null, Color.BLACK));
        pnlEvent.setLayout(new GridBagLayout()); 
        lblEvent.setText("");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;        
        pnlEvent.add(lblEvent, gbc);
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = w;
        gbc.weighty = h;    
        gbc.insets = new Insets(0,5,0,0);   //top, left, bottom, right 
        gbc.fill = GridBagConstraints.NONE;        
        return pnlEvent;        
    }
    //</editor-fold>
    private JPanel OptionPnl(int x, int y, int w, int h){
    //<editor-fold defaultstate="collapsed">        
        JPanel pnlOptions = new JPanel();
        pnlOptions.setPreferredSize(new Dimension((selWide-13), 50));
        pnlOptions.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                "Shoot Options", TitledBorder.LEFT, TitledBorder.ABOVE_TOP,null, Color.BLACK));
        pnlOptions.setLayout(new GridBagLayout());
        //Add pnlOptions controls
        JLabel lblState = new JLabel();
        lblState.setText("State");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0;
        gbc.weighty = 0;
        gbc.insets = new Insets(0,0,0,5);   //top, left, bottom, right
        gbc.fill = GridBagConstraints.BOTH;        
        pnlOptions.add(lblState, gbc);
        cmbState = new JComboBox<>();
        cmbState.addItem("");
        gbc.gridx = 1;
        gbc.insets = new Insets(0,0,0,40);   //top, left, bottom, right
        cmbState.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) 
            {       
                if (!cmbState.getSelectedItem().toString().equals("")) {
                    cmbZone.setSelectedItem("");
                    //btnWinners.setText("Get In-State List");
                    btnWinners.setText("Get Trophy List");
                } else {
                    btnWinners.setText("Get Trophy List");
                }
            }
        }) ;          
        pnlOptions.add(cmbState, gbc);
        JLabel lblZone = new JLabel();
        lblZone.setText("Zone");      
        gbc.gridx = 2;
        gbc.insets = new Insets(0,0,0,5);    //top, left, bottom, right
        pnlOptions.add(lblZone, gbc);
        cmbZone = new JComboBox<>();
        cmbZone.addItem("");
        cmbZone.addItem("Central");
        cmbZone.addItem("Eastern");
        cmbZone.addItem("Southern");
        cmbZone.addItem("SouthWestern");
        cmbZone.addItem("Western");  
        gbc.gridx = 3;
        gbc.insets = new Insets(0,0,0,0);    //top, left, bottom, right
        cmbZone.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) 
            {       
                if (!cmbZone.getSelectedItem().toString().equals("")) {
                    cmbState.setSelectedItem("");
                }
            }
        }) ;         
        pnlOptions.add(cmbZone);
        cmbZone.setEnabled(Constants.enableZone);
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = w;
        gbc.weighty = h;    
        gbc.insets = new Insets(0,5,0,0);   //top, left, bottom, right 
        gbc.fill = GridBagConstraints.NONE;         
        return pnlOptions;
    } 
    //</editor-fold>
    private JPanel WinnersPnl(int x, int y, int w, int h){    
    //<editor-fold defaultstate="collapsed">        
        JPanel pnlWinners = new JPanel();
        pnlWinners.setPreferredSize(new Dimension((selWide-13), 70));
        pnlWinners.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                "Set # of Winners", TitledBorder.LEFT, TitledBorder.ABOVE_TOP,null, Color.BLACK));
        pnlWinners.setLayout(new GridBagLayout());    
        JLabel lblOA = new JLabel("OA");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0;
        gbc.weighty = 0;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0,0,0,25);   //top, left, bottom, right  
        pnlWinners.add(lblOA, gbc);  
        cmbOA = new JComboBox<>(lst);
        gbc.gridy = 1;
        pnlWinners.add(cmbOA,gbc);
        JLabel lblCat = new JLabel("Categ");
        gbc.gridx = 1;
        gbc.gridy = 0;
        pnlWinners.add(lblCat, gbc);
        cmbCat = new JComboBox<>(lst);
        cmbCat.addActionListener(new cmbCatActionListener());
        gbc.gridy = 1;
        pnlWinners.add(cmbCat, gbc);
        JLabel lblYard = new JLabel("Yd Gp");
        gbc.gridx = 2;
        gbc.gridy = 0;
        pnlWinners.add(lblYard, gbc);
        cmbYard = new JComboBox<>(lst);
        cmbYard.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) 
            {   
                setYardWin();
            }
        }) ;         
        gbc.gridy = 1;
        pnlWinners.add(cmbYard, gbc);
        JLabel lblClass = new JLabel("Class");
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.insets = new Insets(0,0,0,0);    //top, left, bottom, right
        pnlWinners.add(lblClass, gbc);
        cmbClass = new JComboBox<>(lst);
        cmbClass.addActionListener(new cmbClassActionListener());
        gbc.gridy = 1;
        pnlWinners.add(cmbClass, gbc);
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = w;
        gbc.weighty = h;    
        gbc.insets = new Insets(0,5,0,0);   //top, left, bottom, right 
        gbc.fill = GridBagConstraints.NONE;         
        return pnlWinners;
    }
    //</editor-fold>
    private JScrollPane ClassPnl(int x, int y, int w, int h){ 
    //<editor-fold defaultstate="collapsed">
        pnlClassD = new JPanel();
        pnlClass = new JScrollPane(pnlClassD);
        pnlClass.setPreferredSize(new Dimension((selWide-13), 85));
        pnlClass.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                "Class", TitledBorder.LEFT, TitledBorder.ABOVE_TOP,null, Color.BLACK));
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = w;
        gbc.weighty = h;    
        gbc.insets = new Insets(0,5,0,0);   //top, left, bottom, right 
        gbc.fill = GridBagConstraints.NONE;         
        return pnlClass;
    }            
    //</editor-fold>
    private JScrollPane CategoryPnl(int x, int y, int w, int h){  
        //<editor-fold defaultstate="collapsed">        
        pnlCatD = new JPanel();
        pnlCat = new JScrollPane(pnlCatD); 
        pnlCat.setPreferredSize(new Dimension((selWide-13), 175));
        pnlCat.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                "Categories", TitledBorder.LEFT, TitledBorder.ABOVE_TOP,null, Color.BLACK));       
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = w;
        gbc.weighty = h;    
        gbc.insets = new Insets(0,5,0,0);   //top, left, bottom, right 
        gbc.fill = GridBagConstraints.NONE;         
        return pnlCat;
    }
    //</editor-fold>
    private JPanel YardPnl(int x, int y, int w, int h){    
    //<editor-fold defaultstate="collapsed">       
        pnlYard = new JPanel();
        pnlYard.setPreferredSize(new Dimension((selWide-13), 80));
        pnlYard.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                "Handicap Yardage Group Split and Winners", TitledBorder.LEFT, TitledBorder.ABOVE_TOP,null, Color.BLACK));
        pnlYard.setLayout(new GridBagLayout());  
        String[] strYard = {"","19","20","21","22","23","24","25","26","27"};
        cmbYard1 = new JComboBox<>(strYard);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0;
        gbc.weighty = 0;  
        gbc.insets = new Insets(0,0,0,0);    //top, left, bottom, right    
        gbc.fill = GridBagConstraints.NONE;        
        cmbYard1.setName("cmbYard1");
        cmbYard1.setActionCommand("cmbYard1");
        cmbYard1.setSelectedItem("19"); 
        cmbYard1.addActionListener(new cmbHdcpActionListener());
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(0,0,0,8);    //top, left, bottom, right
        pnlYard.add(cmbYard1, gbc);
        cmbYard2 = new JComboBox<>(strYard);
        cmbYard2.setName("cmbYard2");
        cmbYard2.setSelectedItem("21");
        cmbYard2.setActionCommand("cmbYard2");
        cmbYard2.addActionListener(new cmbHdcpActionListener());
        gbc.gridx = 1;
        pnlYard.add(cmbYard2, gbc);
        cmbYard3 = new JComboBox<>(strYard);
        cmbYard3.setName("cmbYard3");
        cmbYard3.setSelectedItem("24");
        cmbYard3.setActionCommand("cmbYard3");
        cmbYard3.addActionListener(new cmbHdcpActionListener());
        gbc.gridx = 2;
        pnlYard.add(cmbYard3, gbc);
        cmbYard4 = new JComboBox<>(strYard);
        cmbYard4.setName("cmbYard4");
        cmbYard4.setSelectedItem("26");
        cmbYard4.setActionCommand("cmbYard4");
        cmbYard4.addActionListener(new cmbHdcpActionListener());        
        gbc.gridx = 3;
        pnlYard.add(cmbYard4, gbc);
        cmbYard5 = new JComboBox<>(strYard);
        cmbYard5.setName("cmbYard5");
        cmbYard5.setSelectedItem("");
        cmbYard5.setActionCommand("cmbYard5");
        cmbYard5.addActionListener(new cmbHdcpActionListener());        
        gbc.gridx = 4;
        gbc.insets = new Insets(0,0,0,0);
        pnlYard.add(cmbYard5, gbc);  
        cmbWinYard1 = new JComboBox<>(lst); 
        cmbWinYard1.setName("cmbWinYard1");
        cmbWinYard1.setActionCommand("cmbWinYard1");               
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(0,0,0,8);
        pnlYard.add(cmbWinYard1, gbc);    
        cmbWinYard2 = new JComboBox<>(lst);     
        cmbWinYard2.setName("cmbWinYard2");
        cmbWinYard2.setActionCommand("cmbWinYard2");        
        gbc.gridx = 1;
        pnlYard.add(cmbWinYard2, gbc);
        cmbWinYard3 = new JComboBox<>(lst);
        cmbWinYard3.setName("cmbWinYard3");
        cmbWinYard3.setActionCommand("cmbWinYard3");       
        gbc.gridx = 2;
        pnlYard.add(cmbWinYard3, gbc);
        cmbWinYard4 = new JComboBox<>(lst);
        cmbWinYard4.setName("cmbWinYard4");
        cmbWinYard4.setActionCommand("cmbWinYard4");        
        gbc.gridx = 3;
        pnlYard.add(cmbWinYard4, gbc);
        cmbWinYard5 = new JComboBox<>(lst);
        cmbWinYard5.setName("cmbWinYard5");
        cmbWinYard5.setActionCommand("cmbWinYard5");       
        gbc.gridx = 4;
        gbc.insets = new Insets(0,0,0,0);
        pnlYard.add(cmbWinYard5, gbc);    
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = w;
        gbc.weighty = h;    
        gbc.insets = new Insets(0,5,0,0);   //top, left, bottom, right 
        gbc.fill = GridBagConstraints.NONE;         
        return pnlYard;
    }
    //</editor-fold>
    private JPanel FinalPnl(int x, int y, int w, int h){  
    //<editor-fold defaultstate="collapsed">        
        JPanel pnlFinal = new JPanel(); 
      //  pnlFinal.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(0, 0, 0)));
      //  pnlFinal.setPreferredSize(new Dimension((selWide-13), 190));       
        pnlFinal.setLayout(new GridBagLayout());  
        dndList = new JList<String>(categories);
        dndList.setDragEnabled(true);
        dndList.setDropMode(DropMode.INSERT);
        dndList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dndList.setTransferHandler(new TransferHandler() {
            private int index;
            private boolean beforeIndex = false; //Start with `false` therefore if it is removed from or added to the list it still works

            @Override
            public int getSourceActions(JComponent comp) {
                return MOVE;
            }

            @Override
            public Transferable createTransferable(JComponent comp) {
                index = dndList.getSelectedIndex(); 
                return new StringSelection(dndList.getSelectedValue());
            }

            @Override
            public void exportDone(JComponent comp, Transferable trans, int action) {
                if (action == MOVE) {
                    if(beforeIndex)
                        categories.remove(index + 1);
                    else
                        categories.remove(index);
                    //Get the current order of how the categories are to be printed
                    Shoot_Winners.strCatPrint.clear();
                    for (int i = 0; i < Shoot_Winners.categories.size(); i++){
                        Shoot_Winners.strCatPrint.add(Shoot_Winners.categories.get(i));
                    } 
                }
            }

            @Override
            public boolean canImport(TransferHandler.TransferSupport support) {
                return support.isDataFlavorSupported(DataFlavor.stringFlavor);
            }

            @Override
            public boolean importData(TransferHandler.TransferSupport support) {
                try {
                    String s = (String) support.getTransferable().getTransferData(DataFlavor.stringFlavor);
                    JList.DropLocation dl = (JList.DropLocation) support.getDropLocation();
                    categories.add(dl.getIndex(), s);
                    beforeIndex = dl.getIndex() < index ? true : false;
                    return true;
                } catch (UnsupportedFlavorException | IOException e) {
                    e.printStackTrace();
                }
                return false;
            }
        });   
        pnlPrintCatOrder = new JScrollPane(dndList);
        pnlPrintCatOrder.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                "Category Print Order", TitledBorder.LEFT, TitledBorder.ABOVE_TOP,null, Color.BLACK));
        pnlPrintCatOrder.setPreferredSize(new Dimension(135, 120));
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 4;
        gbc.gridwidth = 2;
        gbc.weightx = 0;
        gbc.weighty = 1;    
        gbc.insets = new Insets(0,0,0,0);    //top, left, bottom, right    
        gbc.fill = GridBagConstraints.VERTICAL;              
        pnlFinal.add(pnlPrintCatOrder, gbc); 
        JLabel lblSpace = new JLabel("      ");
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.weighty = 0;  
        gbc.fill = GridBagConstraints.NONE;
        pnlFinal.add(lblSpace, gbc);         
        cmbShootOff = new JComboBox<>();
        cmbShootOff.addItem("Shoot Off");
        cmbShootOff.addItem("Carry Over");
        gbc.anchor = GridBagConstraints.PAGE_END;
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.insets = new Insets(0,0,0,0);   //top, left, bottom, right
        pnlFinal.add(cmbShootOff, gbc);
        btnWinners = new JButton();
        btnWinners.setEnabled(false);
        btnWinners.setText("Get Trophy List");
        btnWinners.setPreferredSize(new Dimension(150,40));
        btnWinners.addActionListener(new btnWinnersActionListener());
        gbc.gridx = 2;
        gbc.gridwidth = 2;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.PAGE_START;
        pnlFinal.add(btnWinners, gbc);
        JLabel lblSpace2 = new JLabel("      ");
        gbc.gridy = 3;
        gbc.weighty = 1;
        pnlFinal.add(lblSpace2, gbc); 
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.weightx = w;
        gbc.weighty = h;    
        gbc.fill = GridBagConstraints.VERTICAL;
        return pnlFinal;
    }
    //</editor-fold>
    public void setCatWin(int numWin){
    //<editor-fold defaultstate="collapsed">        
        //Set the number of category winners
        for (Component c: pnlCatD.getComponents()){
            if (c instanceof JSpinner){
                ((JSpinner)c).setValue(numWin);
            }
        }
    } 
    //</editor-fold>
    public void setYardWin(){ 
    //<editor-fold defaultstate="collapsed">        
        //find the Yardage winners and update  
        if (cmbYard1.getSelectedItem() != "") 
            {cmbWinYard1.setSelectedItem(cmbYard.getSelectedItem());}else { cmbWinYard1.setSelectedItem("0");};
        if (cmbYard2.getSelectedItem() != "") 
            {cmbWinYard2.setSelectedItem(cmbYard.getSelectedItem());}else { cmbWinYard2.setSelectedItem("0");};
        if (cmbYard3.getSelectedItem() != "") 
            {cmbWinYard3.setSelectedItem(cmbYard.getSelectedItem());}else { cmbWinYard3.setSelectedItem("0");};
        if (cmbYard4.getSelectedItem() != "") 
            {cmbWinYard4.setSelectedItem(cmbYard.getSelectedItem());}else { cmbWinYard4.setSelectedItem("0");};
        if (cmbYard5.getSelectedItem() != "") 
            {cmbWinYard5.setSelectedItem(cmbYard.getSelectedItem());}else { cmbWinYard5.setSelectedItem("0");};
    }    
    //</editor-fold>
    public static void setState(){
    //<editor-fold defaultstate="collapsed">        
        int itemCount = cmbState.getItemCount();
        String curState = cmbState.getSelectedItem().toString();
        for (int i = 0; i < (itemCount - 1); i++){
            cmbState.removeItemAt(1);
        }
        String SQL = "Select distinct(state) from raw order by state";
        try {
            ResultSet rsState = stmt.executeQuery(SQL);
            while (rsState.next()) {
                cmbState.addItem(rsState.getString("state"));
            }    
            //If the same state is in the combobox then reselect it
            cmbState.setSelectedItem(curState);
        }catch (Exception ex) {
            errorPrint(ex);
        }
    }    
    //</editor-fold>
    public static void DataImport(){
    //<editor-fold defaultstate="collapsed">   
        //Import Data
         if (Import.ImportData()){
             //Only do if we are importing the second event
             if (Constants.secondEvnt){                       
                 try {
                    stmt.executeUpdate("update RAW set five = one, six = two, seven = three, eight = four, one = 0, two = 0, three = 0, four = 0");
                    stmt.executeUpdate("update RAW r set one = (select one from RAW2 where membernum = r.membernum), two = (select two from RAW2 where membernum = r.membernum), three = (select three from RAW2 where membernum = r.membernum), four = (select four from RAW2 where membernum = r.membernum)");
                    stmt.executeUpdate("update raw set one = 0, two = 0, three = 0 , four = 0 where one is null");
                    stmt.executeUpdate("update RAW set Score = (one + two + three + four + five + six + seven + eight)");
                    stmt.executeUpdate("delete from RAW where score is null");
                    stmt.executeUpdate("Delete from RAW2");
                 } catch (Throwable ex) {
                     errorPrint(ex); 
                 }
             }
             new CheckCatYard();
             //Set Event name
             lblEvent.setText("<html>" + strEvent + "</html>");
             //Fill out the states
             setState();
             //Show the form to combine any categories
             new CatChange();
             //Show the form to remove categories if enabled
             if (Constants.imTO){new CatRemove();}
             //Show the Class Change form if enabled and possible
             if (checkIfClass() && Constants.inClass){new ClassChange();}
             //Clear categories panel
             pnlCatD.removeAll();  
             pnlCatD.revalidate();
             pnlCatD.repaint();                    
             //Load the categories 
             CatDisplay.LoadCat(Integer.parseInt(cmbCat.getSelectedItem().toString()));
             //Clear classes panel
             pnlClassD.removeAll();  
             pnlClassD.revalidate();
             pnlClassD.repaint();                    
             //Load the classes if necessary
             String SQL = "Select classoryardage from raw";
             ResultSet rsClass;                 
             String s;
             try {
                 rsClass = stmt.executeQuery(SQL);  
                 rsClass.next();
                 SQL = rsClass.getString("ClassorYardage");
                 CH_LOOP:                        
                 for (int i = 0; i < SQL.length(); i++){
                     s = SQL.substring(i, i + 1);
                     if (!s.equals(" ")){  //we don't want a blank character                                
                         if (!s.matches("[-+]?\\d*\\.?\\d+")) {  //This means that the first character is not numeric so assume class 
                             //Setup the Class's used
                             ClassDisplay.LoadClass(Integer.parseInt(cmbClass.getSelectedItem().toString()));
                         }
                         break CH_LOOP;  //stop the for loop
                     }
                 }
             }catch (Exception ex) {
                 errorPrint(ex);
             }
             //Show raw data
             LoadRaw();   
             //Enable 2nd Event Import
             mnAddEvent.setEnabled(true);
         }             
    }
      //</editor-fold>
    private class cmbCatActionListener implements ActionListener{
    //<editor-fold defaultstate="collapsed">        
        public void actionPerformed(ActionEvent e){
            //Set the number of category winners
            int numWin = Integer.parseInt(cmbCat.getSelectedItem().toString());
            for (Component c: pnlCatD.getComponents()){
                if (c instanceof JSpinner){
                    ((JSpinner)c).setValue(numWin);
                }
            }            
        }
    }    
    //</editor-fold>
    private class cmbClassActionListener implements ActionListener{
    //<editor-fold defaultstate="collapsed">
        public void actionPerformed(ActionEvent e){
            //Set the number of category winners
            int numWin = Integer.parseInt(cmbClass.getSelectedItem().toString());
            for (Component c: pnlClassD.getComponents()){
                if (c instanceof JSpinner){
                    ((JSpinner)c).setValue(numWin);
                }
            }
        }
    }
    //</editor-fold>   
    private class btnWinnersActionListener implements ActionListener{
    //<editor-fold defaultstate="collapsed">
        public void actionPerformed(ActionEvent e){
            //Clean out RAW2
            String States = "";
            try {
                stmt.executeUpdate("Delete from RAW2");
                stmt.executeUpdate("Delete from Results");
            } catch (Throwable evt) {
                logger.error("Clearing RAW2 or Results table");
                errorPrint(evt);
                return; //If we cannot clean this out return
            }
            try {
                //Check to see if this is a State shoot
                if (cmbState.getSelectedItem() != "") {     //State Shoot
                    //Get State results
                    stmt.executeUpdate("insert into RAW2 (State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, One, Two, Three, "
                            + "Four, Five, Six, Seven, Eight, Score) select State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, "
                            + "One, Two, Three, Four, Five, Six, Seven, Eight, Score from RAW where state = '"
                            + cmbState.getSelectedItem().toString() + "' order by score desc");
                    Results.getResults("In State");
                    logger.debug("In State results done");
                    //Get Out of State results
                    stmt.executeUpdate("Delete from RAW2");
                    stmt.executeUpdate("insert into RAW2 (State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, One, Two, Three, "
                            + "Four, Five, Six, Seven, Eight, Score) select State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, "
                            + "One, Two, Three, Four, Five, Six, Seven, Eight, Score from RAW where state <> '"
                            + cmbState.getSelectedItem().toString() + "' order by score desc");
                    Results.getResults("Out of State");
                    logger.debug("Out of State results done");
                } else if (cmbZone.getSelectedItem() != "") {   //Zone Shoot
                    //In Zone results
                    String SQL;
                    ResultSet rs;
                    States = "";
                    SQL = "Select State from State where ZoneCode = '" + cmbZone.getSelectedItem().toString() + "'";
                    rs = stmt.executeQuery(SQL);
                    while (rs.next()){
                        States = States + ",'" + rs.getString("State") + "'";                       
                    }                    
                    if (States.length() > 1){
                        States = States.substring(1);
                    }
//                    if (cmbZone.getSelectedItem().toString().equals("Western")){                        
//                        States = "'WA','OR','CA','NV','ID','MT','WY','UT','AZ'";
//                    } else if (cmbZone.getSelectedItem().toString().equals("SouthWestern")){
//                        States = "'CO','NM','KS','OK','TX','MO','AR','LA'";
//                    } else if (cmbZone.getSelectedItem().toString().equals("Central")){
//                        States = "'ND','SD','NE','MN','IA','WI','IL','MI','IN','OH'";
//                    } else if (cmbZone.getSelectedItem().toString().equals("Southern")){
//                        States = "'KY','TN','MS','AL','GA','FL','SC','NC','VA','WV'";
//                    } else if (cmbZone.getSelectedItem().toString().equals("Eastern")){
//                        States = "'ME','NH','VT','NY','PA','MA','RI','CT','NJ','DE','MD'";
//                    }
                    stmt.executeUpdate("insert into RAW2 (State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, One, Two, Three, "
                            + "Four, Five, Six, Seven, Eight, Score) select State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, "
                            + "One, Two, Three, Four, Five, Six, Seven, Eight, Score from RAW where state in "
                            + "(" + States + ") order by score desc");
                    Results.getResults("In Zone");
                    logger.debug("In Zone results done");
                    //Out of Zone results
                    stmt.executeUpdate("Delete from RAW2");
                    stmt.executeUpdate("insert into RAW2 (State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, One, Two, Three, "
                            + "Four, Five, Six, Seven, Eight, Score) select State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, "
                            + "One, Two, Three, Four, Five, Six, Seven, Eight, Score from RAW where state not in "
                            + "(" + States + ") order by score desc");
                    Results.getResults("Out of Zone");
                    logger.debug("Out of Zone results done");
                } else {    //normal shoot go through this once
                    stmt.executeUpdate("insert into RAW2 (State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, One, Two, Three, "
                            + "Four, Five, Six, Seven, Eight, Score) select State, Squad, Membernum, Name, Category, ClassorYardage, Shotat, "
                            + "One, Two, Three, Four, Five, Six, Seven, Eight, Score from RAW");
                    Results.getResults("");
                }
                //Pick a report
                //               new PickReport();
                //Save the report
                //               ShowReport.CreateReport();
            } catch (Throwable evt) {
                logger.error("Error duplicating RAW to RAW2");
                errorPrint(evt);
            }
        }
    }
    //</editor-fold>  
    private class cmbHdcpActionListener implements ActionListener{
    //<editor-fold defaultstate="collapsed">
        public void actionPerformed(ActionEvent e){
            //Load all the controls
            Component[] components = pnlYard.getComponents();
            Object source = e.getSource();
            String cmbSource = ((JComboBox)source).getName();
            String cName = "";
            int intSource = Integer.parseInt(cmbSource.substring(cmbSource.length() - 1));
            int intSourceVal = 0;
            int intC = 0;
            int intCVal = 0;
            if (source instanceof JComboBox){
                //This makes sure we have at least one handicap group
                try {
                    if (cmbSource.equals("cmbYard1") && ((JComboBox)source).getSelectedItem().equals("")){
                        JOptionPane.showMessageDialog(null, "Must have at least one Handicap Group!",
                                "Error!",JOptionPane.ERROR_MESSAGE);
                        ((JComboBox)source).setSelectedItem("19");
                    }
                    //Selection must be larger than 17 + control name and not ""
                    if (cmbSource.substring(0, 7).equals("cmbYard") && !((JComboBox)source).getSelectedItem().equals("")) {
                        //Check to make sure the value of this is possible
                        if (Integer.parseInt((String)((JComboBox)source).getSelectedItem()) <
                                (17 + Integer.parseInt(cmbSource.substring(cmbSource.length() - 1)))){
                            JOptionPane.showMessageDialog(null, "Value not possible!",
                                    "Error!",JOptionPane.ERROR_MESSAGE);
                            ((JComboBox)source).setSelectedItem(Integer.toString((17 + Integer.parseInt(cmbSource.substring(cmbSource.length() - 1)))));
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Error setting Handicap Groups");
                    errorPrint(ex);
                }
                try {
                    //This takes care of selecting a 'blank'
                    if (((JComboBox)source).getSelectedItem().equals("")){
                        for (Component c : components) {
                            cName = ((JComboBox)c).getName();
                            //If the component name is greater than or equal to c then make it blank
                            if (cmbSource.substring(0, 7).equals("cmbYard") &&
                                    Integer.parseInt(cName.substring(cName.length() - 1)) >
                                    Integer.parseInt(cmbSource.substring(cmbSource.length() - 1))){
                                //set the groups to blank
                                ((JComboBox)c).setSelectedItem("");
                                //set winners to 0
                                setYardWin();
                            }
                        }
                        //This is for all other selection of the yardage group
                        //Make sure that the previous value is lower and the next is higher
                    }else if (cmbSource.substring(0, 7).equals("cmbYard")){
                        intSourceVal = Integer.parseInt(((JComboBox)source).getSelectedItem().toString());
                        for (Component c : components) {
                            cName = ((JComboBox)c).getName();
                            intC = Integer.parseInt(cName.substring(cName.length() - 1));
                            if (((JComboBox)c).getSelectedItem().toString().equals("")){
                                intCVal = 30;
                            } else {
                                intCVal = Integer.parseInt(((JComboBox)c).getSelectedItem().toString());
                            }
                            //check if we are on the previous combobox and check its value
                            if ((intC + 1) == intSource && intCVal >= intSourceVal){
                                ((JComboBox)c).setSelectedItem(Integer.toString(intSourceVal - 1));
                                //Set any previous to the current yardage above
                                setYardWin();
                                //check if we are on the next combobox and check its value
                            } else if (intC == (intSource + 1) && intCVal <= intSourceVal){
                                ((JComboBox)c).setSelectedItem(Integer.toString(intSourceVal + 1));
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Error setting Handicap panel, setting blank section");
                    errorPrint(ex);
                }
                try {
                    //This takes care of selecting 27
                    if (((JComboBox)source).getSelectedItem().equals("27")){
                        for (Component c : components) {
                            cName = ((JComboBox)c).getName();
                            //If the component name is greater than or equal to c then make it blank
                            if (cmbSource.substring(0, 7).equals("cmbYard") &&
                                    Integer.parseInt(cName.substring(cName.length() - 1)) >
                                    Integer.parseInt(cmbSource.substring(cmbSource.length() - 1))){
                                //set the groups to blank
                                ((JComboBox)c).setSelectedItem("");
                                //Set winners to 0
                                setYardWin();
                            } else if (cmbSource.substring(0, 7).equals("cmbWin") &&
                                    Integer.parseInt(cName.substring(cName.length() - 1)) >
                                    Integer.parseInt(cmbSource.substring(cmbSource.length() - 1))){
                                //Set the yardage to 0
                                ((JComboBox)c).setSelectedItem("0");
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Error setting Handicap panel, selecting 27");
                    errorPrint(ex);
                }
            }
            //Figure out which group sent us here
            String cnt = e.getActionCommand();
            if (cnt.substring(0, 6).equals("cmbWin")){
                //Need to check the matching group, if blank then make this 0
                if (cnt.substring(10).equals("1")){
                    System.out.println("1");
                } else if (cnt.substring(10).equals("2")){
                    System.out.println("2");
                }
            }
        }
    }
    //</editor-fold>
    public static void LoadRaw(){
    //<editor-fold defaultstate="collapsed">
        //Make sure the table on the form is blank
        DefaultTableModel model = (DefaultTableModel) tblRaw.getModel();
        model.setRowCount(0);
        model.setColumnCount(0);
        ResultSet rsRaw;
        //Display the data on the form
        try {
            rsRaw = stmt.executeQuery("Select * from Raw order by score desc, squad");
            rsRaw.next();
            //Get the number of columns to display
            int columns = rsRaw.getMetaData().getColumnCount();
            //add the columns to the model
            for (int j = 1; j <= columns; j++) {
                model.addColumn(rsRaw.getMetaData().getColumnName(j));
            }
            //populate the table
            rsRaw.beforeFirst();
            while (rsRaw.next()){
                Object[] row = new Object[columns];
                //load the row array up with the values
                for (int i = 1; i <= columns; i++){
                    row[i-1] = rsRaw.getObject(i);
                }
                //insert the row into the jTable
                model.insertRow(rsRaw.getRow() - 1, row);
            }
            //testing how to set column widths
            TableColumn column;
            for (int i = 1; i < columns; i++){
                column = tblRaw.getTableHeader().getColumnModel().getColumn(i-1);
                if (column.getHeaderValue().equals("NAME")) column.setMinWidth(175);
                if (column.getHeaderValue().equals("ID")) {
                    column.setMaxWidth(0);
                    column.setMinWidth(0);
                    column.setWidth(0);
                }
            }
        } catch (Throwable e) {
            errorPrint(e);
        }
    }
    //</editor-fold>
  
    public static void main(String[] args) throws UnknownHostException{  
        logger.info("----------------------Program Started-----------------------!");
        File logLevel = new File("debug");
        //Enable or Disable loggin
        if (logLevel.exists()){
            setLoggingLevel(ch.qos.logback.classic.Level.DEBUG);
        } else {
            setLoggingLevel(ch.qos.logback.classic.Level.ERROR);
        }        
        Constants.vers = "";
        String SQL;
        ResultSet rsA;
        //System.setProperty("apple.laf.useScreenMenuBar", "true");
        //System.setProperty("com.apple.mrj.application.apple.menu.about.name", "ATA Winners");
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    //The following allows for a button to be pressed with the Enter key when it has focus, possibly just a Windows/Nimbus issue.
                    InputMap im = (InputMap)UIManager.get("Button.focusInputMap");
                    im.put( KeyStroke.getKeyStroke( "ENTER" ), "pressed" );
                    im.put( KeyStroke.getKeyStroke( "released ENTER" ), "released" );                    
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            logger.error("Shoot_Winners Class not Found ", ex); 
        } catch (InstantiationException ex) {
            logger.error("Shoot_Winners Instantiation error ", ex); 
        } catch (IllegalAccessException ex) {
            logger.error("Shoot_Winners Illegal Access ", ex); 
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            logger.error("Shoot_Winners Unsupported Look and Feel ", ex); 
        }
        //</editor-fold>
        //</editor-fold>        

        //Set the form icon for mac
        try {
            Constants.vers = System.getProperty("os.name").toLowerCase();            
            if (Constants.vers.indexOf("mac") != -1 && !Constants.vers.equals("")){
                logger.debug("Setting Mac Settings");
                macSettings.setMacDock();   
                macSettings.setMacSettings(); 
            }
        } catch (Throwable ex){
            logger.error("Error setting Main form icon, Mac");
            errorPrint(ex);
        }
        //Make sure the database is setup and ready
        Database workingstuff = new Database();  
        java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
        //System.out.println("Hostname of local machine: " + localMachine.getHostName());
        logger.info("Hostname of local machine: " + localMachine.getHostName()); 
        
        Constants.forceActivation = "No";
        frame = new JFrame();
        frame.setTitle(Constants.programTitle + " - Verifying Installation!");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new java.awt.Dimension(800,800));
        //Set the form icon for windows, mac must be set earlier
        try {
            //String vers = System.getProperty("os.name").toLowerCase();
            if (Constants.vers.indexOf("windows") != -1 && !Constants.vers.equals("")){
                logger.debug("Setting icon");
         //       InputStream imgInputStream = frame.getClass().getResourceAsStream("resources/images/shootwinners.png");
         //       BufferedImage bufferedImage = ImageIO.read(getClass().getResource("/resources/images/shootwinners.png"));
                setIcon();
                BufferedImage bufferedImage = frmIcon;
                frame.setIconImage(bufferedImage);                
            } 
        } catch (Throwable ex){
            logger.error("Error setting Main form icon");
            errorPrint(ex);
        }
        
//<editor-fold defaultstate="collapsed" desc="Menus">
        //Add menu to frame
        JMenuBar mb = new JMenuBar();
        JMenu file = new JMenu("File");
        //add the reports Menu Item
        JMenuItem reports = new JMenuItem("Reports");
        reports.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new ReportManager();
            }
        });
        file.add(reports);
        file.add(new JSeparator());
        //Add Import State menu
        JMenuItem state = new JMenuItem("Import State/Zone File");
        state.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //Select the file you want to import        
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setMultiSelectionEnabled(false);
                fileChooser.setCurrentDirectory(new File(defaultDirectory));
                fileChooser.setDialogTitle("Select State/Zone Import File");
                fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Files: (.txt)", "txt"));
                fileChooser.setAcceptAllFileFilterUsed(false);    
                int result = fileChooser.showOpenDialog(Shoot_Winners.frame);
                if (result == JFileChooser.APPROVE_OPTION){     //Only do the import if they select a file
                    //defaultDirectory = fileChooser.getSe
                    File selectedFile = fileChooser.getSelectedFile();
                    //Set the default path for FileChooser to current location
                    defaultDirectory = selectedFile.getParent();
                    File fileName = fileChooser.getSelectedFile(); 
                    ImportState.importData(fileName);
                } 
            }            
        });
        file.add(state);
        file.add(new JSeparator());
        //Add Acivate Menu Item
        JMenuItem activate = new JMenuItem("Activate");
        //create an actionlistener to manually activate the software
        activate.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Constants.forceActivation = "Yes";
                Activation.GetMachineInfo();
            }
        });
        file.add(activate);
        mb.add(file);
        file.add(new JSeparator());
        deactivate = new JMenu("Deactivate");
        JMenuItem deActivate = new JMenuItem("Yes, deactivate");
        deActivate.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Activation.DeActivate();
            }
        });
        file.add(deactivate);
        deactivate.add(deActivate);
        JMenuItem exit = new JMenuItem("Exit");
        exit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Database.closeDatabase();
                System.exit(0);
            }
        });
        mnEdit = new JMenu("Edit");
        mb.add(mnEdit);
        JMenu importMenu = new JMenu("Import Routines");     
        JCheckBoxMenuItem imTO = new JCheckBoxMenuItem("Ask Targets Only", true);
        imTO.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
                String SQL;
                try {                   
                    if (Constants.imTO){
                        //Currently true so make it false
                        SQL = "Update Params set Value = '0' where Name = 'imTO'";
                        stmt.executeUpdate(SQL);
                        imTO.setState(false);
                        Constants.imTO = false;
                    } else {
                        //Currently false so make it true
                        SQL = "Update Params set Value = '1' where Name = 'imTO'";
                        stmt.executeUpdate(SQL);
                        imTO.setState(true);
                        Constants.imTO = true;
                    }
                } catch (Throwable err){
                    logger.error("Error setting importTO local dB");
                    errorPrint(err);
                }
           } 
        });        
        JCheckBoxMenuItem inClass = new JCheckBoxMenuItem("Edit Class");   
        inClass.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
                String SQL;
                try {                   
                    if (Constants.inClass){
                        //Currently true so make it false
                        SQL = "Update Params set Value = '0' where Name = 'inClass'";
                        stmt.executeUpdate(SQL);
                        inClass.setState(false);
                        Constants.inClass = false;
                    } else {
                        //Currently false so make it true
                        SQL = "Update Params set Value = '1' where Name = 'inClass'";
                        stmt.executeUpdate(SQL);
                        inClass.setState(true);
                        Constants.inClass = true;
                    }
                } catch (Throwable err){
                    logger.error("Error setting inClass local dB");
                    errorPrint(err);
                }
           } 
        });            
        importMenu.add(imTO);
        importMenu.add(inClass);
        mnEdit.add(importMenu);        
        mnLink = new JMenu("Automation Setup");
        mb.add(mnLink);
        JMenu ThreeS = new JMenu("3S Link");
        JCheckBoxMenuItem ThreeSImport = new JCheckBoxMenuItem("Import");
        JCheckBoxMenuItem ThreeSExport = new JCheckBoxMenuItem("Report Export");        
        ThreeSImport.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
                String SQL;
                try {
                    //If we deactivate Import then Export is deactivated as well
                    if (Constants.threeSImport){
                        //Currently true so make it false
                        SQL = "Update Params set Value = '0' where Name = 'threeSImport'";
                        stmt.executeUpdate(SQL);
                        ThreeSImport.setState(false);
                        Constants.threeSImport = false;
                        SQL = "Update Params set Value = '0' where Name = 'threeSExport'";
                        stmt.executeUpdate(SQL);
                        ThreeSExport.setState(false);
                        Constants.threeSExport = false;
                    } else {
                        //Currently false so make it true
                        SQL = "Update Params set Value = '1' where Name = 'threeSImport'";
                        stmt.executeUpdate(SQL);
                        ThreeSImport.setState(true);
                        Constants.threeSImport = true;
                    }
                } catch (Throwable err){
                    logger.error("Error setting 3S params to local dB");
                    errorPrint(err);
                }
           } 
        });
        ThreeSExport.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
                String SQL;
                try {
                    if (Constants.threeSExport){
                        //Currently true so make it false
                        SQL = "Update Params set Value = '0' where Name = 'threeSExport'";
                        stmt.executeUpdate(SQL);
                        ThreeSExport.setState(false);
                        Constants.threeSExport = false;                       
                    } else {
                        //Currently false so make it true
                        //Must also make sure that Import is true
                        SQL = "Update Params set Value = '1' where Name = 'threeSExport'";
                        stmt.executeUpdate(SQL);
                        ThreeSExport.setState(true);
                        Constants.threeSExport = true;
                        SQL = "Update Params set Value = '1' where Name = 'threeSImport'";
                        stmt.executeUpdate(SQL);
                        ThreeSImport.setState(true);
                        Constants.threeSImport = true;                           
                    }
                } catch (Throwable err){
                    logger.error("Error setting 3S params to local dB");
                    errorPrint(err);
                }
           } 
        });        
        ThreeS.add(ThreeSImport);
        ThreeS.add(ThreeSExport);
        mnLink.add(ThreeS);
        mnSMS = new JMenu("Text Alerts");
        mnSMS.setBackground(Color.RED);
        mb.add(mnSMS);
        JMenu SMSSignup = new JMenu("SMS Signup");
        JMenuItem SMSAllow = new JMenuItem("Allow");
        JMenuItem SMSDeny = new JMenuItem("Deny");
        SMSAllow.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                TimerTask repeatedTask = new TimerTask() {
                    public void run() {
                        SMSRead.ReadSMS();
                    }
                };
                smsTimer = new Timer("Timer");
                long delay  = 6000L;
                long period = 15000L;
                //smsTimer.schedule(repeatedTask, delay, period);
                SMSAllow.setEnabled(false);
                SMSDeny.setEnabled(true);
                smsTimer.scheduleAtFixedRate(repeatedTask, delay, period); 
                mnSMS.setOpaque(true);
                mnSMS.setBackground(Color.GREEN);                
            }
        });
        SMSDeny.setEnabled(false);
        SMSDeny.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                smsTimer.cancel();
                SMSAllow.setEnabled(true);
                SMSDeny.setEnabled(false);
                mnSMS.setOpaque(false);
                mnSMS.setBackground(Color.RED); 
            }
        });
        SMSSignup.add(SMSAllow);
        SMSSignup.add(SMSDeny);
        mnSMS.add(SMSSignup);   
        JMenu delSMSmn = new JMenu("Delete Contacts");
        JMenuItem delSMS = new JMenuItem("Yes, Delete all Contacts!");
        delSMS.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if (!mnSMS.getBackground().equals(Color.RED)){
                    SMSAllow.setEnabled(true);
                    SMSDeny.setEnabled(false);
                    mnSMS.setOpaque(false);
                    smsTimer.cancel();
                }
                //It is possible that if someone is texting in at the moment you click on this 
                //and Allow signup is on that they could get into the contact table after the delete
                SMSRead.ClearSMSTable();                
            }
        });
        delSMSmn.add(delSMS);
        mnSMS.add(delSMSmn);
        JMenuItem editContacts = new JMenuItem("Edit Contacts");
        editContacts.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //show the form to edit contacts
                try{
                    new SMS();  //show the contacts form
                } catch (ParseException ex){
                    logger.error("Error calling Contacts form");
                    errorPrint(ex);
                }
            }
        });
        mnSMS.add(editContacts);
        JMenuItem testText = new JMenuItem("Test Text");
        testText.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                //Send a test text message
                try{
                    MaskFormatter ph = new MaskFormatter("(###) ###-####");
                    ph.setPlaceholderCharacter('_');
                    JFormattedTextField txtPhone = new JFormattedTextField(ph);
                    txtPhone.setFont(fnt);
                    JTextField txtMessage = new JTextField(15);
                    txtMessage.setFont(fnt);
                    JPanel testPnl = new JPanel();
                    testPnl.setFont(fnt);
                    testPnl.add(new JLabel("Phone Number:"));
                    testPnl.add(txtPhone);
                    testPnl.add(Box.createHorizontalStrut(15));
                    testPnl.add(new JLabel("Message:"));
                    testPnl.add(txtMessage);
                    int result = JOptionPane.showConfirmDialog(Shoot_Winners.frame, testPnl, "Enter test text info"
                            , JOptionPane.OK_CANCEL_OPTION);
                    if (result == JOptionPane.OK_OPTION && !txtPhone.getText().contains("_")){
                        SMS.TwilioSMS(txtPhone.getText(), txtMessage.getText());
                    }
                } catch (ParseException ex){
                    errorPrint(ex);
                }
            }
        });
        mnSMS.add(testText);
        JMenuItem blank = new JMenuItem(" ");
        mnSMS.add(blank);
        mnSMS.add(new JSeparator());
        JMenuItem sendText = new JMenuItem("Send Text Alerts");
        sendText.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //Pop up a form displaying the texts that can be sent
                SMS.selectSMSEvent();
            }
        });
        mnSMS.add(sendText);
        mnSMS.setEnabled(false);
        file.add(exit);
        //Add the Second Event Import
        mnAddEvent = new JMenuItem("Import Second Event");        
        mnAddEvent.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Constants.secondEvnt = true;
                DataImport();
                Constants.secondEvnt = false;
                mnAddEvent.setEnabled(false);
            }
        });
        mb.add(mnAddEvent);
        mnAddEvent.setEnabled(Constants.AddEvnt);
        
//</editor-fold>
        
        frame.setJMenuBar(mb);        
        frame.setContentPane(new Shoot_Winners());
        frame.pack();
        frame.setLocationRelativeTo(null);            
        frame.setVisible(true);
        //If Mac do not allow automation
        if (Constants.vers.indexOf("mac") != -1 && !Constants.vers.equals("")){
                mnLink.setEnabled(false); 
                mnLink.setVisible(false);
            }
        //Load 3S options
        try{
            SQL = "Select Value from Params where Name = 'threeSImport'";
            rsA = stmt.executeQuery(SQL);
            rsA.next();
            if (rsA.getBoolean("Value")){
                Constants.threeSImport = true;
                ThreeSImport.setState(true);
                //Since this is true try to get the number of events
                
            }   
            SQL = "Select Value from Params where Name = 'threeSExport'";
            rsA = stmt.executeQuery(SQL);
            rsA.next();
            if (rsA.getBoolean("Value")){
                Constants.threeSExport = true;
                ThreeSExport.setState(true);
            }
        } catch (SQLException e){
            if (e.getSQLState().equals("24000")){
                try {
                    logger.debug("Add 3S params");
                    SQL = "Insert into Params (Name, Value) Values ('threeSImport' , '0')";
                    stmt.executeUpdate(SQL);    
                    SQL = "Insert into Params (Name, Value) Values ('threeSExport' , '0')";
                    stmt.executeUpdate(SQL);                        
                } catch (SQLException ex) {
                    logger.error("Could not add the 3S params");
                    errorPrint(ex);
                    ThreeSImport.setState(false);
                    ThreeSExport.setState(false);                    
                }
            } 
        }      
        //Load imTO options
        try{
            SQL = "Select Value from Params where Name = 'imTO'";
            rsA = stmt.executeQuery(SQL);
            rsA.next();
            if (rsA.getBoolean("Value")){
                Constants.imTO = true;
                imTO.setState(true);           
            }   
        } catch (SQLException e){
            if (e.getSQLState().equals("24000")){
                try {
                    logger.debug("Add imTO params");
                    SQL = "Insert into Params (Name, Value) Values ('imTO' , '1')";
                    imTO.setState(true);
                    stmt.executeUpdate(SQL);                            
                } catch (SQLException ex) {
                    logger.error("Could not params");
                    errorPrint(ex);
                }
            } 
        }     
        //Load inClass options
        try{
            SQL = "Select Value from Params where Name = 'inClass'";
            rsA = stmt.executeQuery(SQL);
            rsA.next();
            if (rsA.getBoolean("Value")){
                Constants.inClass = true;
                inClass.setState(true);           
            }   
        } catch (SQLException e){
            if (e.getSQLState().equals("24000")){
                try {
                    logger.debug("Add inClass params");
                    SQL = "Insert into Params (Name, Value) Values ('inClass' , '0')";
                    stmt.executeUpdate(SQL);                            
                } catch (SQLException ex) {
                    logger.error("Could not params");
                    errorPrint(ex);
                    imTO.setState(false);
                }
            }             
        }          
        //Check Activation - does a check to make sure that the processor info matches what is in the database
        Activation.GetMachineInfo();
        //Enable the get winners button after activation has completed
        btnWinners.setEnabled(true);
        frame.setTitle(Constants.programTitle);
    }
    
    private static BufferedImage frmIcon;
    private static void setIcon(){
        try {
            //String vers = System.getProperty("os.name").toLowerCase();
            frmIcon = ImageIO.read(Shoot_Winners.class.getResource("/images/shootwinners.png"));
        //    return frmIcon;                         
        } catch (Throwable ex){
            logger.error("Error setting Main form icon");
            errorPrint(ex);
        }  
   //     return null;
    }
    static void errorPrint(Throwable e) {
        if (e instanceof SQLException)
            SQLExceptionPrint((SQLException)e);
        else {
            logger.error((e).getMessage());
            e.printStackTrace();
        }
    }
    
    //  Iterates through a stack of SQLExceptions
    static void SQLExceptionPrint(SQLException sqle) {
        while (sqle != null) {
            logger.debug("\n---SQLException Caught---\n");
            logger.debug("SQLState:   " + (sqle).getSQLState());
            logger.debug("Severity: " + (sqle).getErrorCode());
            logger.debug("Message:  " + (sqle).getMessage());
            sqle.printStackTrace();
            sqle = sqle.getNextException();
        }
    }
    
    public static void setLoggingLevel(ch.qos.logback.classic.Level level) {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        root.setLevel(level);
    }    
    
    public static boolean checkIfClass(){
        String SQL = "Select classoryardage from raw";
        ResultSet rsClass;                 
        String s;
        try {
            rsClass = stmt.executeQuery(SQL);  
            rsClass.next();
            SQL = rsClass.getString("ClassorYardage");                    
            for (int i = 0; i < SQL.length(); i++){
                s = SQL.substring(i, i + 1);
                if (!s.equals(" ")){  //we don't want a blank character                                
                    if (!s.matches("[-+]?\\d*\\.?\\d+")) {  //This means that the first character is not numeric so assume class 
                        //Setup the Class's used
                        return true;
                    }
                    return false;   //first character is numeric
                }
            }
            return false;  //If we made it this far nothing to do
        }catch (Exception ex) {
            errorPrint(ex);
            return false;
        }
    }
}