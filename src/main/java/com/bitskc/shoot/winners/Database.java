/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import entitymanager.Entity;
import java.io.File;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author talbers
 */
public class Database {
    
    static Logger logger = LoggerFactory.getLogger("Database");
    public static String connectionURL;
    public static Statement stmt;
    public static Entity entity = new Entity();
    
    public Database() {
        Constants.newDB = "No";
        String SQL;
        logger.debug("Loading connection properties");
        try{
          //  Progress.showProgress("Preparing","Getting program ready!", 0);
            Constants.pgmInfo1 = entity.getEntity("pgmInfo1");
            Constants.pgmInfo2 = entity.getEntity("pgminfo2");
            connectionURL = "jdbc:derby:" + Constants.dbName + ";create=true;user=" + Constants.pgmInfo1 + 
                    ";password=" + Constants.pgmInfo2;
        } catch (Throwable e){
            logger.error("Cannot load connection properties");
            System.exit(0);
        }         
        Constants.conn = null;    
        // Load the driver
        try {
            Class.forName(Constants.driver);
            logger.debug(Constants.driver + " loaded.");
        } catch (java.lang.ClassNotFoundException e) {
            logger.debug("ClassNotFoundException: ");
            logger.debug(e.getMessage());
            logger.debug("\n Make sure your CLASSPATH variable " +
                "contains %DERBY_HOME%\\lib\\derby.jar (${DERBY_HOME}/lib/derby.jar). \n");
            logger.debug("ClassNotFoundException: ");
        }                   
        // Log into database and create if it doesn't exist
        try {
            logger.debug("Trying to connect to " + Constants.dbName);
            Constants.conn = DriverManager.getConnection(connectionURL);
            stmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);            
            //Check to see if all the needed tables exist
            //Create the machineID table if it doesn't exist
            SQL = "Create Table machineID(id integer not null generated always as identity (start with 1, increment by 1),"
                    + " serialnum varchar(36), whenAdded timestamp, timesRun integer, isActivated boolean,"
                    + " SMS boolean default false not null, Account_SID varchar(40) default '0' not null,"
                    + " AUTH_Token varchar(40) default '0' not null, SMSDate date, PhoneNum varchar(11), uniqueID varchar(50))";
            if (!checkTable(SQL, "machineID", "No")) {
                JOptionPane.showMessageDialog(null, "Error creating machineID table");
                logger.error("Cannot create machineID table");
                System.exit(1);
            } 
            //Drop and Create RAW Table 
            SQL = "create table RAW(id integer not null generated always as identity (start with 1, increment by 1),"
                    + "state varchar(10), squad varchar(10), membernum varchar(20), name varchar(100), category varchar(20), "
                    + "classoryardage varchar(20), shotat integer, one integer, two integer, three integer, four integer, "
                    + "five integer, six integer, seven integer, eight integer, score integer)";
            if (!checkTable(SQL, "RAW", "Yes")) {
                JOptionPane.showMessageDialog(null, "Error creating RAW table");
                logger.error("Error creating RAW table");
                System.exit(1);
            }
            //Drop and Create RAW2 
            SQL = "create table RAW2(id integer not null generated always as identity (start with 1, increment by 1),"
                    + "state varchar(10), squad varchar(10), membernum varchar(20), name varchar(100), category varchar(20), "
                    + "classoryardage varchar(20), shotat integer, one integer, two integer, three integer, four integer, "
                    + "five integer, six integer, seven integer, eight integer, score integer)";
            if (!checkTable(SQL, "RAW2", "Yes")) {
                JOptionPane.showMessageDialog(null, "Error creating RAW table");
                logger.error("Error creating RAW2 table");
                System.exit(1);
            }
            //Drop and Create Results table 
            SQL = "Create table Results(id integer not null generated always as identity (start with 1, increment by 1),"
                    + "EventNum integer, EventName varchar(150), EventType varchar(30), Place varchar(30), ResultType varchar(50), "
                    + "state varchar(10), squad varchar(10), membernum varchar(20), name varchar(100), category varchar(20), "
                    + "classoryardage varchar(20), shotat integer, one integer, two integer, three integer, four integer, "
                    + "five integer, six integer, seven integer, eight integer, score integer, Extra varchar(30), PrintOrder integer)";
            if (!checkTable(SQL, "Results", "Yes")) {
                JOptionPane.showMessageDialog(null, "Error creating Results table");
                logger.error("Error creating Results table");
                System.exit(1);
            }
            //Create the Contact table if it doesn't exist
            SQL = "Create Table Contact(id integer not null generated always as identity (start with 1, increment by 1),"
                    + "ATANum varchar(7), name varchar(100) not null, whenAdded timestamp default current_timestamp,"
                    + " PhoneNum varchar(10), concent boolean default false)";
            if (!checkTable(SQL, "contact", "No")) {
                JOptionPane.showMessageDialog(null, "Error creating contact table");
                logger.error("Cannot create contact table");
                System.exit(1);
            }  
            //Create the SMS table if it doesn't exist.  This is used to keep SMS messages
            SQL = "Create Table SMS(id integer not null generated always as identity (start with 1, increment by 1),"
                    + " ATANum varchar(7), PhoneNum varchar(10) not null, resultSID varchar(75), message varchar(250),"
                    + " whenAdded timestamp default current_timestamp, uploaded boolean default false not null, eventname varchar(150))";
            if (!checkTable(SQL, "sms", "No")) {
                JOptionPane.showMessageDialog(null, "Error creating SMS table");
                logger.error("Cannot create SMS table");
                System.exit(1);
            }   
            //Create the test table if needed
            SQL = "Create Table test(num varchar(7), name varchar(30), phonenum varchar(10))";
            if (!checkTable(SQL, "test", "No")) {
                JOptionPane.showMessageDialog(null, "Error creating test table");
                logger.error("Cannot create test table");
                System.exit(1);
            }
            //Create the State table
            SQL = "Create Table State(State varchar(2), Name varchar(100), ZoneCode varchar(20))";
            if (!checkTable(SQL, "State", "No")){
                JOptionPane.showMessageDialog(null, "Error creating state table");
                logger.error("Cannot create state table");
                System.exit(1);
            }
            //Make sure we have information in the State Table
            ResultSet rs;
            SQL = "Select count(*) as NumState from State";
            rs = stmt.executeQuery(SQL);
            rs.next();
            if (rs.getInt("NumState") == 0){    //no information so import
                File fileName = new File("statezone.txt");
                ImportState.importData(fileName);
            }
            //Create the params table
            SQL = "Create Table Params(id integer not null generated always as identity (start with 1, increment by 1),"
                    + " Name varchar(50) not null, Value varchar(50) not null)";
            if (!checkTable(SQL, "Params", "No")){
                JOptionPane.showMessageDialog(null, "Error creating Params table");
                logger.error("Cannot create Params table");
                System.exit(1);
            }
            /*  No longer needed as we are dropping and recreating the table every time the program opens
            //Reset the ID on both tables
            stmt.executeUpdate("alter table RAW alter column ID restart with 1");
            stmt.executeUpdate("alter table RAW2 alter column ID restart with 1");
            */           
        } catch (Throwable e) {
            errorPrint(e);  
            JOptionPane.showMessageDialog(null, "Error initializing database!\nIs another instance running?", "Startup error!",JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }   
        //update the machineId table 
        try{
            SQL = "Select SMS from machineID";                     
            stmt.executeUpdate(SQL);             
        } catch (SQLException e){
            if (e.getSQLState().equals("42X04")){   //column does not exist so we need to add it.
                try {
                    SQL = "Alter Table machineID add Column SMS boolean default false not null";
                    stmt.addBatch(SQL);
                    SQL = "Alter Table machineID add Column Account_SID varchar(40) default '0' not null";
                    stmt.addBatch(SQL);
                    SQL = "Alter Table machineID add Column AUTH_Token varchar(40) default '0' not null";
                    stmt.addBatch(SQL);
                    SQL = "Alter Table machineID add Column SMSDate date";
                    stmt.addBatch(SQL);  
                    SQL = "Alter Table machineID add Column PhoneNum varchar(11)";
                    stmt.addBatch(SQL);
                    stmt.executeBatch();
                    stmt.clearBatch();                  
                } catch (SQLException ex){
                    if (ex.getSQLState().equals("X0Y32")){
                        logger.debug("machineID table already updated!");
                    }                    
                }
            }            
        }
    }
    
    public static void closeDatabase(){                    
        try {        
            //Close the database (move elsewhere later)
            Constants.conn.close();
            Constants.conn = null;    
            /* In embedded mode, an application should shut down Derby.
            Shutdown throws the XJ015 exception to confirm success. */
            boolean gotSQLExc = false;
            try {
                DriverManager.getConnection("jdbc:derby:;shutdown=true");
            } catch (SQLException se) {
                if ( se.getSQLState().equals("XJ015") ) {
                    gotSQLExc = true;
                }
            }
            if (!gotSQLExc) {
                 //System.out.println("Database did not shut down normally");
                 logger.debug("Database did not shut down normally");
            } else {
                 //System.out.println("Database shut down normally");
                 logger.debug("Database shut down normally");
            }
        } catch (Throwable e) {
            errorPrint(e);      
        }         
    }
    static void errorPrint(Throwable e) {
        Progress.closeProgress();   
        if (e instanceof SQLException)
            SQLExceptionPrint((SQLException)e);
        else {
            //System.out.println("A non-SQL error occurred.");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }
    //  Iterates through a stack of SQLExceptions
    static void SQLExceptionPrint(SQLException sqle) {
        while (sqle != null) {
            //System.out.println("\n---SQLException Caught---\n");
            //System.out.println("SQLState:   " + (sqle).getSQLState());
            //System.out.println("Severity: " + (sqle).getErrorCode());
            //System.out.println("Message:  " + (sqle).getMessage());
            logger.error("\n---SQLException Caught---\n");
            logger.error("SQLState:   " + (sqle).getSQLState());
            logger.error("Severity: " + (sqle).getErrorCode());
            logger.error("Message:  " + (sqle).getMessage());
            sqle.printStackTrace();
            sqle = sqle.getNextException();
        }
    }
/*
    public static void updateMachineID(String SQL){
        //quick routine to update the machineID table
        try {
            stmt.executeUpdate(SQL);
        }  catch (Throwable e) {
                errorPrint(e);      
        }        
    }
*/
    public static boolean checkTable(String SQL, String tableName, String dropTBL){
        //Check to see if all the needed tables have been created
        try {
            if (dropTBL.equals("Yes")) stmt.executeUpdate("Drop table " + tableName);
        } catch (SQLException ex) {
            if (ex.getSQLState().equals("42Y55")){
                logger.debug("Cannot drop " + tableName + " it doesn't exist!");
            }
        }
        try {           
            //This creates the table if it doesn't exist
            stmt.executeUpdate(SQL);
            logger.debug(tableName + " table created!");
            return true;  //table created
        } catch (SQLException e) {
            if (e.getSQLState().equals("X0Y32")){
                logger.debug(tableName + " table already exists!");
                return true;  //Table exists
            }
        }
         logger.error("Error creating " + tableName);
         return false;  //something went wrong
    }
}
