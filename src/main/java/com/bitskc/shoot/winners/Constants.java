/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import java.sql.Connection;

/**
 *
 * @author talbers
 */
public class Constants {
    final public static String programTitle = "Shoot Winners v12.2.8";
    final public static String email = "twalbers@gmail.com";   
    final public static String driver = "org.apache.derby.jdbc.EmbeddedDriver";
    final public static String dbName = "dbATA";
    //The following is simply a check to see if ativation is possible.
    final public static String host = "https://www.shootwinners.com";  //http://ata.albersmo.com"; 
    
    public static String pgmInfo1 = "";
    public static String pgmInfo2 = "";
    public static String fileName = "";
    public static String forceActivation;
    public static Connection conn;
    public static String newDB;
    public static String strSerial;
    public static String ACCOUNT_SID = "";
    public static String AUTH_TOKEN = "";  
    public static String PhoneNum = "";
    public static String vers = "";
    public static int shootCount = 0;
    public static boolean enableZone = true;
    public static boolean threeSImport = false;
    public static boolean threeSExport = false;
    public static boolean threeSFileCheck = false;
    public static boolean imTO = true;
    public static boolean inClass = false;
    public static Integer NumEvents = 0;
    public static String shootName = "";
    public static String eventName = "";
    public static String eventNum = "";
    public static boolean AddEvnt = false;
    public static boolean secondEvnt = false;
    
}
