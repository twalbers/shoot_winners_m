/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Database.stmt;
import static com.bitskc.shoot.winners.Shoot_Winners.frame;
import static com.bitskc.shoot.winners.importPCMaster.SQLExceptionPrint;
import static com.bitskc.shoot.winners.importPCMaster.logger;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.slf4j.LoggerFactory;

/**
 *
 * @author talbers
 */
public class ImportState {
    
    static String defaultDirectory = System.getProperty("user.home");  
    //static String fileName;
    static org.slf4j.Logger logger = LoggerFactory.getLogger("State Import");
    static JDialog dialog;
    
    public static boolean importData(File fileName){
        //Now check the file to make sure it is formatted correctly
        if (!verifyFile(fileName)){
            logger.error("Could not import the State/Zone file");
            JOptionPane.showMessageDialog(null, "Could not import the State/Zone file. Wrong Format!",
                        "Error!",JOptionPane.ERROR_MESSAGE);  
            Constants.enableZone = false;
            return false;            
        }
        //Everything looks good so clear the table and import the file
        try {
            //Disable Zone just in case
            Constants.enableZone = false;
            //Clear out the test table
            String SQL = "Delete from State";
            stmt.executeUpdate(SQL);
            //Import everything to a temp table called TEST
            SQL = "CALL SYSCS_UTIL.SYSCS_IMPORT_TABLE(null,'STATE','" + fileName.getAbsolutePath() + "',',',null,null,0)";
            stmt.executeUpdate(SQL);  
            //Enable Zone if everything worked correctly
            Constants.enableZone = true;
        } catch(Throwable e){
                errorPrint(e);
                logger.error(e.getMessage());
                return false;
        }
        return true;
    }
    static void errorPrint(Throwable e) {
        if (e instanceof SQLException)
            SQLExceptionPrint((SQLException)e);
        else {
            logger.error("Error in the SMS module");
            logger.error((e).getMessage());
            e.printStackTrace();
        }
    }
    public static boolean verifyFile(File fileName){
            //This will get one line at a time
            String line = null; 
            try { 
                //FileReader reads text files in the default encoding
                FileReader fileReader = new FileReader(fileName);            
                //Always wrap FileReader in Bufferedreader
                BufferedReader bufferedReader = new BufferedReader(fileReader);                 
                int curLine = 1; 
                //Check to make sure that each segment is the correct length
                while ((line = bufferedReader.readLine()) != null){
                    String[] lineData = line.split(",");
                    if (lineData[0].length() != 2 || lineData[1].length() > 100 || lineData[2].length() > 20){
                        return false;                    
                    }
                    curLine ++;
                }
                //Make sure to close the file
                bufferedReader.close();
                return true;
            } catch(FileNotFoundException ex) {
                logger.error("Unable to open file '" + Constants.fileName + "'");
                return false;
            } catch(IOException ex){
                logger.error("Error reading file '" + Constants.fileName + "'");
                return false;
            } catch(Throwable e){
                errorPrint(e);
                logger.error(e.getMessage());
                return false;
            } 
    }   
}
