/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;
/**
 *
 * @author talbers
 */
import com.twilio.Twilio;
import com.twilio.base.ResourceSet;
import com.twilio.rest.api.v2010.account.Message;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SMSRead {
    
    static Logger logger = LoggerFactory.getLogger("SMSRead"); 
    public static Statement stmtSMS;       

    public static void ReadSMS(){
        String SQL;
        ResultSet rs;
        try {
            Twilio.init(Constants.ACCOUNT_SID, Constants.AUTH_TOKEN);
            ResourceSet<Message> messages = Message.reader().read();
            for(Message record : messages){
                System.out.println("Phone number: " + record.getFrom());
                System.out.println("Message: "+ record.getBody());
                System.out.println("Message SID: " + record.getSid());
                //Remove any message sent by the program or service
                if (record.getFrom().toString().equals("+" + Constants.PhoneNum)){
                    DeleteSMS(record.getSid()); 
                } else {
                    //Accept only responses that are 7 digits in length
                    if (record.getBody().trim().length() == 7){
                        stmtSMS = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                        SQL = "Select count(*) from Contact where ATANum = '" + record.getBody() + "'";
                        rs = stmtSMS.executeQuery(SQL);
                        rs.next();
                        if (rs.getInt(1) != 0){
                            //ATANum exists so update phone number
                            if (UpdateSMS(record.getFrom().toString().substring(2),record.getBody().trim())){DeleteSMS(record.getSid());}                            
                        } else {
                            //ATANum does not exist so add entry
                            if (AddSMS(record.getFrom().toString().substring(2),record.getBody().trim())){DeleteSMS(record.getSid());} 
                        }
                        SMS.TwilioSMS(record.getFrom().toString(), "Accepted.  You are signed up for Shootoff Notifications. (Reply Stop to unsubscribe)");
                    } else {
                        //Not a valid number send a reply that it was not accepted
                        SMS.TwilioSMS(record.getFrom().toString(), "Number not accepted.  Please resend ATA Number with no spaces.");
                        DeleteSMS(record.getSid());
                    }
                }
            }            
        } catch (Throwable e) {
            logger.error("Error in SMSRead.ReadSMS");
            errorPrint(e);
        }
    }
    public static boolean AddSMS(String phoneNum, String ataNum){
        String SQL;
        SQL = "Insert into Contact (PhoneNum, ATANum, Name, Concent) values ('" + phoneNum + "', '" + ataNum + "', '" + ataNum + "', true)";        
        try {
            stmtSMS = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmtSMS.executeUpdate(SQL);
        } catch (Throwable e){
            errorPrint(e);
            logger.error("Error in SMSRead.AddSMS");            
            return false;
        }                 
        return true;
    }
    public static boolean UpdateSMS(String phoneNum, String ataNum){
        String SQL;
        SQL = "Update Contact set PhoneNum = '" + phoneNum + "', Concent = true where atanum = '" + ataNum + "'";
        try {
            stmtSMS = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmtSMS.executeUpdate(SQL);
        } catch (Throwable e){
            errorPrint(e);
            logger.error("Error in SMSRead.UpdateSMS");
            return false;
        }                 
        return true;
    }
    public static void ClearSMSTable(){
        String SQL;
        SQL = "Delete from Contact";
        try {
            stmtSMS = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmtSMS.executeUpdate(SQL);
        } catch (Throwable e){
            errorPrint(e);
            logger.error("Error in SMSRead.ClearSMSTable");
        }         
    }
    public static void DeleteSMS(String messageSID){
        Twilio.init(Constants.ACCOUNT_SID, Constants.AUTH_TOKEN);
        try {
            Message.deleter(messageSID).delete();
        } catch(Throwable e) {
            logger.error("Error in SMSRead.DeleteSMS");
            errorPrint(e);            
        }
    }
    static void errorPrint(Throwable e) {
        //JOptionPane.showMessageDialog(null, "Error in the SMSRead Module, check log file!", "Error!", JOptionPane.ERROR_MESSAGE);
        Shoot_Winners.mnSMS.getBackground().equals(Color.YELLOW);
        if (e instanceof SQLException)
            SQLExceptionPrint((SQLException)e);
        else {
            logger.error((e).getMessage());
            e.printStackTrace();
        }
    }    
    //  Iterates through a stack of SQLExceptions
    static void SQLExceptionPrint(SQLException sqle) {
        while (sqle != null) {
            logger.debug("\n---SQLException Caught---\n");
            logger.debug("SQLState:   " + (sqle).getSQLState());
            logger.debug("Severity: " + (sqle).getErrorCode());
            logger.debug("Message:  " + (sqle).getMessage());
            sqle.printStackTrace();
            sqle = sqle.getNextException();
        }
    }       
}
