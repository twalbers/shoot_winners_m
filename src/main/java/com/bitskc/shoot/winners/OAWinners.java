/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Database.stmt;
import static com.bitskc.shoot.winners.Shoot_Winners.cmbCat;
import java.sql.ResultSet;
import java.sql.Statement;
import org.slf4j.LoggerFactory;
/**
 *
 * @author talbers
 */
public class OAWinners {
    
    static org.slf4j.Logger logger = LoggerFactory.getLogger("OAWinners"); 

    public static void getOAWinners(String strResult, int numWin, String Place){        
        Statement stWin;
        ResultSet rsWin;
        ResultSet rsCount;
        //Figure out the last score eligible
        String SQL = "Select count(score) as totalScores, score from RAW2 group by Score order by Score desc";
        String Extra;
        int PrintOrder = 9995;
        int lastScore, curScore;
        try {
            stWin = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            rsWin = stWin.executeQuery(SQL);
            int curWin = 0;
            curScore = 0;
            lastScore = 1000;
            //the following should kick out once we have enough winners, to this point there has been no standby
            while (rsWin.next() && curWin < numWin) {
                curWin = curWin + rsWin.getInt("totalScores");
                lastScore = rsWin.getInt("Score");     
            }
            if (curWin < numWin){   //This should only happen if we have more places than people, if that happnes remove all scores as all won
                lastScore = 0;
            }
            //the current record is the last one eligible for this, any scores less would be gauranteed a trophy
            //and should be removed from RAW2
   //         lastScore = rsWin.getInt("Score");
   //         SQL = "Select * from RAW2 where Score >= " + lastScore + " order by Score desc, squad";
            SQL = "Select id, state, squad, membernum, name, category, classoryardage, shotat, one, two, three, four, five, six, seven, eight, score, "
                + "(Select count(score) from raw2 as r where Score >= " + lastScore + " and r.score = raw2.score) as totalscores "
                + "from RAW2 where Score >= " + lastScore + " order by Score desc, squad";    
            rsWin = stWin.executeQuery(SQL);
            //Go through the database and put winners in the table Results
            //if we have more records than places then mark the last records as shootoff for this loop as there is no standby
            while (rsWin.next()) {
                if (rsWin.getInt("Score") != curScore){
                    curScore = rsWin.getInt("Score");
                    PrintOrder = PrintOrder + 5;
                }
/*                
                rsCount = stmt.executeQuery("Select Count(Score) as totalScores from RAW2 where Score = " + rsWin.getInt("Score"));
                rsCount.next();
                if (rsCount.getInt("totalScores") > 1) {
                    Extra = Shoot_Winners.cmbShootOff.getSelectedItem().toString();
                } else {
                    Extra = "";
                }
*/
                if (rsWin.getInt("totalScores") > 1) {
                    Extra = Shoot_Winners.cmbShootOff.getSelectedItem().toString();
                } else {
                    Extra = "";
                }
                stmt.executeUpdate("Insert into Results (EventName, Place, ResultType, State, Squad, MemberNum, Name, Category, ClassorYardage, ShotAt, one, two, three, four, "
                        + "five, six, seven, eight, score, Extra, PrintOrder) values ('" + Shoot_Winners.strEvent + "', '" + Place + "', '" + strResult + "', '" 
                        + rsWin.getString("State") + "', '" + rsWin.getString("Squad") 
                        + "', '" + rsWin.getString("MemberNum") + "', '" + rsWin.getString("Name").replaceAll("'","\\\'\\\'") + "', '" + rsWin.getString("Category") + "', '" 
                        + rsWin.getString("ClassorYardage") + "', " + rsWin.getInt("Shotat") + ", " + rsWin.getInt("One") + ", " + rsWin.getInt("two") 
                        + ", " + rsWin.getInt("Three") + ", " + rsWin.getInt("four") + ", " + rsWin.getInt("five") + ", " + rsWin.getInt("six") 
                        + ", " + rsWin.getInt("seven") + ", " + rsWin.getInt("eight") + ", " + rsWin.getInt("score") + ", '" + Extra + "', " + PrintOrder + ")");                
            }
            if (curWin != numWin){
                //now remove any scores greater than the last score from RAW2 as they are gauranteed a trophy
                SQL = "Delete from RAW2 where score > " + lastScore;                
            } else {
                SQL = "Delete from RAW2 where score >= " + lastScore;
            }
            stmt.executeUpdate(SQL);
            logger.debug("Over All Winners done!");
        } catch (Throwable e) {
            logger.error("Error getting Over All Winners " + e.getMessage());
            errorPrint(e);
        }
    }
    public static void getOAOther(String strResult, int numWin){        
        Statement stRU;
        ResultSet rsRU;
        ResultSet rsCount;
        int PrintOrder;
        String Extra, SQL;
        int lastScore, curWin, numCount, placesAdded, totalPlacesAdded, standbyScore, removalScore, curScore;
        try {                        
            stRU = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            SQL = "Select count(score) as totalScores, score from RAW2 group by Score order by Score desc"; 
            rsRU = stmt.executeQuery(SQL);  //this is a list of eligible scores
            curWin = 0;
            standbyScore = 0;
            removalScore = 100000;
            curScore = 0;
            PrintOrder = 19995;
            lastScore = 1000;
            totalPlacesAdded = 0;
            while (rsRU.next() && curWin < numWin){
                placesAdded = 0;
                curWin = curWin + rsRU.getInt("totalScores");
                lastScore = rsRU.getInt("Score");
                if (curWin <= numWin){
                    removalScore = rsRU.getInt("Score");    //the only reason to put a score in here is to use it to remove scores later                
                }
                if (standbyScore == 1){
                    standbyScore = rsRU.getInt("Score");
                }
                SQL = "Select RAW2.score, count(RAW2.score) as totalScores from RAW2 join results on RAW2.membernum = results.membernum where RAW2.score ="
                        + rsRU.getInt("Score") + " group by RAW2.score";
                rsCount = stRU.executeQuery(SQL);
                while (rsCount.next()){ //this should only have max one record but it might also be empty
                    placesAdded = rsCount.getInt("totalScores");
                    totalPlacesAdded = totalPlacesAdded + placesAdded;
                }  
                //Since this will always happen after OA and category, get a count of how many are in the results table that are still in the raw table
                //and are not a category shooter.  Subtract numElOA from that total and if > 0 subtract the answer from placesAdded
                //TODO:  if there are no category winners than this needs to be adjusted.  Currently I am not worried about this as 
                //there is always category winners but it would be nice to fix
                //The following is an attempt to fix the below statement when there are no category winners. 
                if (Integer.parseInt(cmbCat.getSelectedItem().toString()) != 0){
                    //We have category Winners
                    SQL = "Select RAW2.score, count(RAW2.score) as totalScores from RAW2 join results on RAW2.membernum = results.membernum where RAW2.score ="
                        + rsRU.getInt("Score") + " and RAW2.category = '' group by RAW2.score";  
                } else {
                    //No Category Winners
                    SQL = "Select RAW2.score, count(RAW2.score) as totalScores from RAW2 join results on RAW2.membernum = results.membernum where RAW2.score ="
                        + rsRU.getInt("Score") + " group by RAW2.score";  
                }
              
                rsCount = stRU.executeQuery(SQL);
                if (rsCount.next()){
                    if (rsCount.getInt("totalScores") > Results.numElOA){
                        placesAdded = placesAdded - (rsCount.getInt("totalScores") - Results.numElOA);
                    }
                }
                if (placesAdded != 0){
                    curWin = curWin - placesAdded;  //Get us the extra places needed for people in shootoffs
                }
                if ((curWin + totalPlacesAdded) >= numWin && totalPlacesAdded != 0 && curWin < numWin){ //This needs to happen before we adjust the places, as any scores after this are in standby
                    standbyScore = 1;   //If we go through the loop again then we are getting standby scores, if not there are no standby scores
                }                  
            }
//            SQL = "Select * from RAW2 where Score >= " + lastScore + " order by Score desc, squad";
            SQL = "Select id, state, squad, membernum, name, category, classoryardage, shotat, one, two, three, four, five, six, seven, eight, score, "
                + "(Select count(score) from raw2 as r where Score >= " + lastScore + " and r.score = raw2.score) as totalscores "
                + "from RAW2 where Score >= " + lastScore + " order by Score desc, squad";               
            rsRU = stRU.executeQuery(SQL);
            while (rsRU.next()){
                if (rsRU.getInt("Score") != curScore){
                    curScore = rsRU.getInt("Score");
                    PrintOrder = PrintOrder + 5;
                }
                Extra = "";
/*                rsCount = stmt.executeQuery("Select Count(Score) as totalScores from RAW2 where Score = " + rsRU.getInt("Score"));
                rsCount.next();
                numCount = rsCount.getInt("totalScores");   //keeping track to see if we have a shootoff for this score
*/
                numCount = rsRU.getInt("totalScores");
                //check to see if the current person is already in the results.  They could be here for both OA shootoffs or Category Standby
                rsCount = stmt.executeQuery("Select * from Results where membernum = '" + rsRU.getString("membernum").toString() + "' order by ID desc"); 
                while (rsCount.next()){    //check to make sure we have records if so then 
                    if (rsCount.getString("Extra").equals("Category Standby")){ 
                        Extra = "Pending Category Standby";
                    } else {
                        Extra = "Pending OA Shootoff";
                    }
                    break;
                } 
                if (Extra.equals("")){
                    if (rsRU.getInt("Score") == standbyScore){
                        Extra = "Standby";
                    } else if (numCount > 1){
                        Extra = Shoot_Winners.cmbShootOff.getSelectedItem().toString();
                    } else {
                        Extra = "";
                    }                    
                }
                stmt.executeUpdate("Insert into Results (EventName, Place, ResultType, State, Squad, MemberNum, Name, Category, ClassorYardage, ShotAt, one, two, three, four, "
                        + "five, six, seven, eight, score, Extra, PrintOrder) values (' " + Shoot_Winners.strEvent + "', 'Runnerup+', '" + strResult + "', '" 
                        + rsRU.getString("State") + "', '" + rsRU.getString("Squad") 
                        + "', '" + rsRU.getString("MemberNum") + "', '" + rsRU.getString("Name").replaceAll("'","\\\'\\\'") + "', '" + rsRU.getString("Category") + "', '" 
                        + rsRU.getString("ClassorYardage") + "', " + rsRU.getInt("Shotat") + ", " + rsRU.getInt("One") + ", " + rsRU.getInt("two") 
                        + ", " + rsRU.getInt("Three") + ", " + rsRU.getInt("four") + ", " + rsRU.getInt("five") + ", " + rsRU.getInt("six") 
                        + ", " + rsRU.getInt("seven") + ", " + rsRU.getInt("eight") + ", " + rsRU.getInt("score") + ", '" + Extra + "', " + PrintOrder + ")");  
            }
            //Remove from RAW2 if guaranteed a trophy
            if (standbyScore > 1){ //This means we have standby shooters so remove all scores above this
                SQL = "Delete from RAW2 where score > " + standbyScore;
            } else {    //We don't have any standby scores, the last score has either won something or in a shootoff so they are no longer eligible
                SQL = "Delete from RAW2 where score >= " + removalScore;
            }
            stmt.executeUpdate(SQL);    //Remove ineligible people
            logger.debug("getOAOther done!");                      
        } catch (Throwable e) {
            logger.error("Error in getOAOther " + e.getMessage());
            errorPrint(e);
        }
    }    
}
