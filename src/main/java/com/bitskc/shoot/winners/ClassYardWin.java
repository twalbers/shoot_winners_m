/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Database.stmt;
import static com.bitskc.shoot.winners.Shoot_Winners.pnlClassD;

import java.awt.Component;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JSpinner;
import org.slf4j.LoggerFactory;
/**
 *
 * @author talbers
 */
public class ClassYardWin {
    
    static org.slf4j.Logger logger = LoggerFactory.getLogger("ClassYardWin"); 
    
    public static void getClassWinners(String strResult){
        Statement stClass;
        ResultSet rsClass;
        ResultSet rsCount;
        int PrintOrder;
        String Extra, SQL;
        int lastScore, curWin, numWin, numCount, placesAdded, totalPlacesAdded, standbyScore, removalScore, curScore;    
        try {
            stClass = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            for (Component c: pnlClassD.getComponents()){
                if (c instanceof JSpinner && (Integer)((JSpinner)c).getValue() > 0){    //only do this if we need to
                    numWin = (Integer)((JSpinner)c).getValue();
                    curScore = 0;
                    SQL = "Select count(score) as totalScores, score from RAW2 where CLASSORYARDAGE = '"
                            + ((JSpinner)c).getName()  + "' group by Score order by Score desc"; 
                    rsClass = stmt.executeQuery(SQL);  //this is a list of eligible scores
                    curWin = 0;
                    standbyScore = 0;
                    removalScore = 100000;
                    curScore = 0;
                    lastScore = 1000;
                    totalPlacesAdded = 0;
                    //Set print order based on class
                    PrintOrder = 29995 + ((3 - ((JSpinner)c).getName().length()) * 150);  //this takes care of AA and AAA
                    if ((((JSpinner)c).getName().trim()).length() == 1){ //this takes care of all the other classes
                        if ((((JSpinner)c).getName().trim()).toCharArray()[0] > 65){
                            PrintOrder = PrintOrder + (((((JSpinner)c).getName().trim()).toCharArray()[0] - 65) * 150);
                        }
                    }
                    //figure out the last score needed 
                    while (rsClass.next() && curWin < numWin){
                        placesAdded = 0;
                        curWin = curWin + rsClass.getInt("totalScores");
                        lastScore = rsClass.getInt("Score");
                        if ((curWin + totalPlacesAdded) <= numWin){
                            removalScore = rsClass.getInt("Score");    //the only reason to put a score in here is to use it to remove scores later                
                        }
                        if (standbyScore == 1){
                            standbyScore = rsClass.getInt("Score");
                        }
//                        Replaced to get a more accurate number of individuals
//                        SQL = "Select RAW2.score, count(RAW2.score) as totalScores from RAW2 join results on RAW2.membernum = results.membernum where RAW2.score ="
//                                + rsClass.getInt("Score") + " and raw2.CLASSORYARDAGE = '" + ((JSpinner)c).getName() + "' group by RAW2.score";
                        SQL = "Select RAW2.score, count(distinct(raw2.membernum)) as totalScores from RAW2 join results on RAW2.membernum = results.membernum where RAW2.score ="
                                + rsClass.getInt("Score") + " and raw2.CLASSORYARDAGE = '" + ((JSpinner)c).getName() + "' group by RAW2.score";
                        rsCount = stClass.executeQuery(SQL);
                        while (rsCount.next()){ //this should only have max one record but it might also be empty
                            placesAdded = rsCount.getInt("totalScores");
                            totalPlacesAdded = totalPlacesAdded + placesAdded;
                        }              
                        if (placesAdded != 0){
                            curWin = curWin - placesAdded;  //Get us the extra places needed for people in shootoffs
                        }
                        if ((curWin + totalPlacesAdded) >= numWin && totalPlacesAdded != 0 && curWin < numWin){ //This needs to happen before we adjust the places, as any scores after this are in standby
                            standbyScore = 1;   //If we go through the loop again then we are getting standby scores, if not there are no standby scores
                        }                  
                    }
    //                SQL = "Select * from RAW2 where Score >= " + lastScore + " and CLASSORYARDAGE = '" + ((JSpinner)c).getName() + "' order by Score desc, squad";
                    //Get the scores along with a count of scores to see if there is a shootoff
                    SQL = "Select id, state, squad, membernum, name, category, classoryardage, shotat, one, two, three, four, five, six, seven, eight, score, "
                        + "(Select count(score) from raw2 as r where Score >= " + lastScore + " and CLASSORYARDAGE = '" + ((JSpinner)c).getName() + "' and r.score = raw2.score) as totalscores "
                        + "from RAW2 where Score >= " + lastScore + " and CLASSORYARDAGE = '" + ((JSpinner)c).getName() + "' order by Score desc, squad";    
                    rsClass = stClass.executeQuery(SQL);  
                    //Get the winners 
                    while (rsClass.next()){
                        if (rsClass.getInt("Score") != curScore){
                            curScore = rsClass.getInt("Score");
                            PrintOrder = PrintOrder + 5;
                        }
                        Extra = "";
                        /* //Not needed, combined with the master query
                        rsCount = stmt.executeQuery("Select Count(Score) as totalScores from RAW2 where Score = " + rsClass.getInt("Score") + " and CLASSORYARDAGE = '" + ((JSpinner)c).getName() + "'");
                        rsCount.next();
                        numCount = rsCount.getInt("totalScores");   //keeping track to see if we have a shootoff for this score
                        */
                        numCount = rsClass.getInt("totalScores");   //keeping track to see if we have a shootoff for this score
                        //check to see if the current person is already in the results.  
                        rsCount = stmt.executeQuery("Select * from Results where membernum = '" + rsClass.getString("membernum") + "' order by ID desc"); 
                        while (rsCount.next()){    //check to make sure we have records if so then 
                            if (rsCount.getString("Extra").equals("Category Standby")){ 
                                Extra = "Pending Category Standby";
                            } else if (rsCount.getString("Extra").equals("Standby")){
                                Extra = "Pending Standby";
                            } else {
                                Extra = "Pending " + Shoot_Winners.cmbShootOff.getSelectedItem().toString();
                            }
                            break;
                        } 
                        if (Extra.equals("")){
                            if (rsClass.getInt("Score") >= standbyScore && rsClass.getInt("Score") < removalScore && standbyScore > 1){ //We can possibly have multiple standby scores less than the removal
                                Extra = "Standby";
                            } else if (numCount > 1){
                                Extra = Shoot_Winners.cmbShootOff.getSelectedItem().toString();
                            } else {
                                Extra = "";
                            }                    
                        }
                        stmt.executeUpdate("Insert into Results (EventName, Place, ResultType, State, Squad, MemberNum, Name, Category, ClassorYardage, ShotAt, one, two, three, four, "
                                + "five, six, seven, eight, score, Extra, PrintOrder) values ('" + Shoot_Winners.strEvent + "', '" + ((JSpinner)c).getName() + "', '" + strResult + "', '" 
                                + rsClass.getString("State") + "', '" + rsClass.getString("Squad") 
                                + "', '" + rsClass.getString("MemberNum") + "', '" + rsClass.getString("Name").replaceAll("'","\\\'\\\'") + "', '" + rsClass.getString("Category") + "', '" 
                                + rsClass.getString("ClassorYardage") + "', " + rsClass.getInt("Shotat") + ", " + rsClass.getInt("One") + ", " + rsClass.getInt("two") 
                                + ", " + rsClass.getInt("Three") + ", " + rsClass.getInt("four") + ", " + rsClass.getInt("five") + ", " + rsClass.getInt("six") 
                                + ", " + rsClass.getInt("seven") + ", " + rsClass.getInt("eight") + ", " + rsClass.getInt("score") + ", '" + Extra + "', " + PrintOrder + ")");  
                    }   
                    //Remove from RAW2 if guaranteed a trophy
                    if (standbyScore > 1){ //This means we have standby shooters so remove all scores above this
                        SQL = "Delete from RAW2 where score > " + standbyScore + " and CLASSORYARDAGE = '" + ((JSpinner)c).getName() + "'";
                    } else {    //We don't have any standby scores, the last score has either won something or in a shootoff so they are no longer eligible
                        SQL = "Delete from RAW2 where score >= " + removalScore + " and CLASSORYARDAGE = '" + ((JSpinner)c).getName() + "'";
                    }
                    stmt.executeUpdate(SQL);    //Remove ineligible people
                    logger.debug("Class " + ((JSpinner)c).getName() + " done!");                      
                }
            }
        } catch (Throwable e) {
            logger.error("Error Class Winners " + e.getMessage());
            errorPrint(e);
        }            
    }
    public static void getYardWinners(String strResult, ArrayList<String> yardGps, ArrayList<Integer> yardGpsWin){
        Statement stClass;
        ResultSet rsClass;
        ResultSet rsCount;
        int PrintOrder;
        String Extra, SQL, strBegin, strEnd;
        int lastScore, curWin, numWin, numCount, placesAdded, totalPlacesAdded, standbyScore, removalScore, curScore;
        int intGp = 0;  //times through the for loop below
        try {
            stClass = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            for (String s : yardGps){            
                if (yardGpsWin.get(intGp) > 0 && !s.equals("")){    //only do this if we need to
                    numWin = yardGpsWin.get(intGp);
                    curScore = 0;
                    strBegin = s;
                    if (intGp < 4 && !yardGps.get(intGp + 1).equals("")){
                        strEnd = Double.toString(Integer.parseInt(yardGps.get(intGp + 1)) - .5);    //need to remove .5 from this string
                    } else strEnd = "27.0";
                    SQL = "Select count(score) as totalScores, score from RAW2 where CLASSORYARDAGE between '"
                            + strBegin  + "' and '" + strEnd + "' group by Score order by Score desc"; 
                    rsClass = stmt.executeQuery(SQL);  //this is a list of eligible scores
                    curWin = 0;
                    standbyScore = 0;
                    removalScore = 1000;
                    curScore = 0;
                    lastScore = 1000;
                    totalPlacesAdded = 0;
                    PrintOrder = 30000 + intGp * 150;
                    while (rsClass.next() && curWin < numWin){
                        placesAdded = 0;
                        curWin = curWin + rsClass.getInt("totalScores");
                        lastScore = rsClass.getInt("Score");
                        if ((curWin + totalPlacesAdded) <= numWin){
                            removalScore = rsClass.getInt("Score");    //the only reason to put a score in here is to use it to remove scores later                
                        }
                        if (standbyScore == 1){
                            standbyScore = rsClass.getInt("Score");
                        }
                        SQL = "Select RAW2.score, count(RAW2.score) as totalScores from RAW2 join results on RAW2.membernum = results.membernum where RAW2.score ="
                                + rsClass.getInt("Score") + " and raw2.CLASSORYARDAGE between '" + strBegin  + "' and '" + strEnd + "' group by RAW2.score";
                        rsCount = stClass.executeQuery(SQL);
                        while (rsCount.next()){ //this should only have max one record but it might also be empty
                            placesAdded = rsCount.getInt("totalScores");
                            totalPlacesAdded = totalPlacesAdded + placesAdded;
                        }              
                        if (placesAdded != 0){
                            curWin = curWin - placesAdded;  //Get us the extra places needed for people in shootoffs
                        }
                        if ((curWin + totalPlacesAdded) >= numWin && totalPlacesAdded != 0 && curWin < numWin){ //This needs to happen before we adjust the places, as any scores after this are in standby
                            standbyScore = 1;   //If we go through the loop again then we are getting standby scores, if not there are no standby scores
                        }                  
                    }
                    SQL = "Select * from RAW2 where Score >= " + lastScore + " and CLASSORYARDAGE between '" + strBegin  + "' and '" + strEnd + "' order by Score desc, squad";
                    rsClass = stClass.executeQuery(SQL);  
                    while (rsClass.next()){
                        if (rsClass.getInt("Score") != curScore){
                            curScore = rsClass.getInt("Score");
                            PrintOrder = PrintOrder + 5;
                        }
                        Extra = "";
                        rsCount = stmt.executeQuery("Select Count(Score) as totalScores from RAW2 where Score = " + rsClass.getInt("Score") 
                                + " and CLASSORYARDAGE between '" + strBegin  + "' and '" + strEnd + "'");
                        rsCount.next();
                        numCount = rsCount.getInt("totalScores");   //keeping track to see if we have a shootoff for this score
                        //check to see if the current person is already in the results.  
                        rsCount = stmt.executeQuery("Select * from Results where membernum = '" + rsClass.getString("membernum") + "' order by ID desc"); 
                        while (rsCount.next()){    //check to make sure we have records if so then 
                            if (rsCount.getString("Extra").equals("Category Standby")){ 
                                Extra = "Pending Category Standby";
                            } else if (rsCount.getString("Extra").equals("Standby")){
                                Extra = "Pending Standby";
                            } else {
                                Extra = "Pending " + Shoot_Winners.cmbShootOff.getSelectedItem().toString();
                            }
                            break;
                        } 
                        if (Extra.equals("")){
                            if (rsClass.getInt("Score") >= standbyScore && rsClass.getInt("Score") < removalScore && standbyScore > 1){
                                Extra = "Standby";
                            } else if (numCount > 1){
                                Extra = Shoot_Winners.cmbShootOff.getSelectedItem().toString();
                            } else {
                                Extra = "";
                            }                    
                        }
                        stmt.executeUpdate("Insert into Results (EventName, Place, ResultType, State, Squad, MemberNum, Name, Category, ClassorYardage, ShotAt, one, two, three, four, "
                                + "five, six, seven, eight, score, Extra, PrintOrder) values ('" + Shoot_Winners.strEvent + "', '(" + strBegin  + " - " + strEnd + ")', '" 
                                + strResult + "', '" + rsClass.getString("State") + "', '" + rsClass.getString("Squad") 
                                + "', '" + rsClass.getString("MemberNum") + "', '" + rsClass.getString("Name").replaceAll("'","\\\'\\\'") + "', '" + rsClass.getString("Category") + "', '" 
                                + rsClass.getString("ClassorYardage") + "', " + rsClass.getInt("Shotat") + ", " + rsClass.getInt("One") + ", " + rsClass.getInt("two") 
                                + ", " + rsClass.getInt("Three") + ", " + rsClass.getInt("four") + ", " + rsClass.getInt("five") + ", " + rsClass.getInt("six") 
                                + ", " + rsClass.getInt("seven") + ", " + rsClass.getInt("eight") + ", " + rsClass.getInt("score") + ", '" + Extra + "', " + PrintOrder + ")");  
                    }   
                    //Remove from RAW2 if guaranteed a trophy
                    if (standbyScore > 1){ //This means we have standby shooters so remove all scores above this
                        SQL = "Delete from RAW2 where score > " + standbyScore + " and CLASSORYARDAGE between '" + strBegin  + "' and '" + strEnd + "' ";
                    } else {    //We don't have any standby scores, the last score has either won something or in a shootoff so they are no longer eligible
                        SQL = "Delete from RAW2 where score >= " + removalScore + " and CLASSORYARDAGE between '" + strBegin  + "' and '" + strEnd + "' ";
                    }
                    stmt.executeUpdate(SQL);    //Remove ineligible people
                    logger.debug("Yardage Group " + strBegin  + " and " + strEnd + " done!");                      
                }
                intGp++;
            }
        } catch (Throwable e) {
            logger.error("Error getting Yardage Winners " + e.getMessage());
            errorPrint(e);
        }         
    }
}
