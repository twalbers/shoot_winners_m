/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Shoot_Winners.frame;
import static com.bitskc.shoot.winners.Shoot_Winners.strReport;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.slf4j.LoggerFactory;
/**
 *
 * @author talbers
 */
public class PickReport {
    static org.slf4j.Logger logger = LoggerFactory.getLogger("PickReport");
    public JComboBox<String> cmbReport;
    public PickReport(){
        try {
            File dir = new File("Reports/");
            File[] strReports = dir.listFiles((d,name) -> name.endsWith(".jrxml"));

            //setup the dialog form
            GridBagConstraints gbcC = new GridBagConstraints();
            JDialog dlgDisplay = new JDialog(Shoot_Winners.frame, "Select a Report to Display!", true);
            dlgDisplay.setLayout(new GridBagLayout());
            gbcC.gridx = 0;
            gbcC.gridy = 0;
            gbcC.weightx = 0;
            gbcC.weighty = 0;
            gbcC.fill = GridBagConstraints.HORIZONTAL;
            //Setup top Panel
            JLabel lbl = new JLabel("Pick a report to display:");
            lbl.setFont(new Font("Serif", Font.BOLD, 16));
            JPanel topPnl = new JPanel();
        //    topPnl.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
            topPnl.add(lbl, gbcC);
            gbcC.insets = new Insets(0,0,0,0);   //top, left, bottom, right  
            gbcC.gridx = 1;
            gbcC.anchor = GridBagConstraints.LINE_START;
            cmbReport = new JComboBox<>();  
            for (int i = 0; i < strReports.length; i++){
                cmbReport.addItem(strReports[i].getPath().substring(8, (strReports[i].getPath().length() - 6)));
            } 
            if (strReport != ""){
                cmbReport.setSelectedItem(strReport);
            }
            cmbReport.setName("Report");
            topPnl.add(cmbReport, gbcC);
            gbcC.gridx = 0;
            gbcC.gridy = 0;
            dlgDisplay.getContentPane().add(topPnl, gbcC);
            JPanel btmPanel = new JPanel();
            JButton btnOK = new JButton();
            btnOK.setText("Continue");
            btnOK.setPreferredSize(new Dimension(150,40));
            btnOK.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent arg0) 
                {  
                    if (cmbReport.getSelectedItem() != null){
                        dlgDisplay.dispose();
                        //Create and display the report
                        strReport = cmbReport.getSelectedItem().toString();
                        ShowReport.CreateReport(strReport + ".jrxml");                    
                    } 
                }
            }) ;            
            dlgDisplay.getContentPane().add(topPnl, gbcC); 
            gbcC.gridx = 0;
            gbcC.gridy = 1;
            btmPanel.add(btnOK);
            dlgDisplay.getContentPane().add(btmPanel, gbcC);
            dlgDisplay.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            dlgDisplay.setSize(400, 50);
            dlgDisplay.setLocationRelativeTo(frame); 
            dlgDisplay.pack();
            dlgDisplay.setResizable(false);
            dlgDisplay.setVisible(true);             
        } catch (Exception e){
            logger.error("Error " + e.getMessage());
            e.printStackTrace();
        }
    }
}
