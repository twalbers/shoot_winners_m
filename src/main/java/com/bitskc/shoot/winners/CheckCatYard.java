/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Database.stmt;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.slf4j.LoggerFactory;
import static com.bitskc.shoot.winners.Shoot_Winners.frame;
/**
 *
 * @author talbers
 */
public class CheckCatYard {
    static org.slf4j.Logger logger = LoggerFactory.getLogger("CheckCatYard");    
    public CheckCatYard(){
        //This here to check classoryardage for blank or null, that is not acceptable so popluate it
        String SQL;
        ResultSet rsCk;
        ResultSet rsUnique;
        ArrayList<String> CatOrYard = new ArrayList<>();
        int character;
        String strType = "";
        try {
            //Find out if this is a Class or Yardage shoot
            SQL = "Select * from RAW order by classoryardage desc";
            rsCk = stmt.executeQuery(SQL);  
            if (rsCk.next()){
                character = (int)rsCk.getString("classoryardage").charAt(0);
                if (character > 47 && character < 58){  //this is a number
                    strType = "Handicap";
                    SQL = "select distinct(classoryardage) from raw where classoryardage is not null and classoryardage <> '' order by classoryardage";
                } else {
                    strType = "Class";
                    SQL = "select distinct(classoryardage) from raw where classoryardage is not null and classoryardage <> '' order by length(classoryardage) desc, classoryardage";
                }
            } else {    //no records
                return;
            }            
            //load up the classes or yardage            
            rsUnique = stmt.executeQuery(SQL);
            while (rsUnique.next()){
                CatOrYard.add(rsUnique.getString("classoryardage"));
            }               
            //See if we have any blank classoryardage
            SQL = "Select * from RAW where classoryardage = '' or classoryardage is null";
            rsCk = stmt.executeQuery(SQL);         
            if (rsCk.next()){   //only do this if we have records
                rsCk.beforeFirst();
                //setup the dialog form
                GridBagConstraints gbcC = new GridBagConstraints();
                JDialog dlgCheck = new JDialog(Shoot_Winners.frame, "Fix " + strType, true);
                dlgCheck.setLayout(new GridBagLayout());
                gbcC.gridx = 0;
                gbcC.gridy = 0;
                gbcC.weightx = 1;
                gbcC.weighty = 1;
                gbcC.fill = GridBagConstraints.HORIZONTAL;
                //Setup top Panel
                JLabel lbl = new JLabel("<html>We have a empty " + strType + "<br><center>Please set a value!</center></html>");
                lbl.setFont(new Font("Serif", Font.BOLD, 16));
                JPanel topPnl = new JPanel();
                topPnl.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
                //topPnl.setLayout(new GridBagLayout());
                //topPnl.setPreferredSize(new java.awt.Dimension(120,30));
                topPnl.add(lbl);
                dlgCheck.getContentPane().add(topPnl, gbcC);     
                //Now setup center panel with checkboxes
                JPanel cntrPnl = new JPanel();
                cntrPnl.setLayout(new GridBagLayout());
                int x = 0;
                int y = 0;
                gbcC.gridx = 0;
                gbcC.gridy = 0;
                gbcC.insets = new Insets(0,0,0,0);   //top, left, bottom, right  
                gbcC.ipadx = 0;
                gbcC.ipady = 0;
                gbcC.gridwidth = 1;
                gbcC.anchor = GridBagConstraints.LINE_START;
                JLabel lblName;
                JLabel lblNumber;
                JComboBox cmbClassYard;                
                while (rsCk.next()){   //add the items needed to the dialog form 
                    lblName = new JLabel();
                    gbcC.gridx = x;
                    gbcC.gridy = y;
                    lblName.setText(rsCk.getString("Name"));
                    cntrPnl.add(lblName, gbcC);
                    x++;
                    lblNumber = new JLabel();
                    gbcC.gridx = x;
                    lblNumber.setText(rsCk.getString("membernum"));
                    cntrPnl.add(lblNumber, gbcC);
                    x++;
                    cmbClassYard = new JComboBox<>(CatOrYard.toArray());  //load up the class or yardage
                    gbcC.gridx = x;
                    cmbClassYard.setName(rsCk.getString("membernum"));  //need a unique number to update the recordset when done
                    //For class do not allow edit, for yardage allow edit but limit to 3 characters, numbers and decimal only
                    if (!strType.equals("Class")){
                        cmbClassYard.setEditable(true);
                    } else {    //Handicap event
                        cmbClassYard.getEditor().getEditorComponent().addKeyListener(new KeyAdapter(){  //limit entry to numbers and period
                            @Override
                            public void keyTyped(KeyEvent e){
                                char c = e.getKeyChar();
                                if (!(Character.isDigit(c)) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE) || (c == KeyEvent.VK_PERIOD)){
                                    e.consume();
                                }
                            }
                        });
                    }
                    cmbClassYard.setPreferredSize(new Dimension(60,25));                    
                    cntrPnl.add(cmbClassYard, gbcC);
                    x = 0;
                    y++;
                }    
                //Now set the cntrPnl height
                cntrPnl.setPreferredSize(new Dimension(300, y * 25));
                gbcC.gridx = 0;
                gbcC.gridy = 1;
                gbcC.insets = new Insets(0,5,0,0);   //top, left, bottom, right  
                dlgCheck.getContentPane().add(cntrPnl, gbcC);                    
                JPanel btmPanel = new JPanel();
                JButton btnOK = new JButton();
                btnOK.setText("Continue");
                btnOK.setPreferredSize(new Dimension(150,40));
                btnOK.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent arg0) 
                    {  
                        Component[] components = cntrPnl.getComponents();  //Get a list of components in panel
                        for (Component c : components) {
                            if (c instanceof JComboBox){
                                String strSource = ((JComboBox)c).getSelectedItem().toString();
                                String strName = ((JComboBox)c).getName();
                                String SQL = "Update RAW set classoryardage = '" + strSource + "' where membernum = '" + strName + "'";
                                try{
                                    stmt.executeUpdate(SQL);
                                } catch (Throwable e){
                                    errorPrint(e);
                                    logger.error(e.getMessage()); 
                                }                                
                            }                   
                        }
                        dlgCheck.dispose();
                    }
                }) ;                     
                btmPanel.add(btnOK);
                gbcC.gridy = 2;
                dlgCheck.getContentPane().add(btmPanel, gbcC);
                dlgCheck.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                dlgCheck.setSize(300, y * 25);
                dlgCheck.setLocationRelativeTo(frame); 
                dlgCheck.pack();
                dlgCheck.setResizable(false);
                dlgCheck.setVisible(true);                                       
            }
        } catch (Throwable e){
            errorPrint(e);
            logger.error(e.getMessage()); 
            return;
        }
    }    
}
