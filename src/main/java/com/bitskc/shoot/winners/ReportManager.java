/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Import.defaultDirectory;
import static com.bitskc.shoot.winners.Shoot_Winners.frame;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.slf4j.LoggerFactory;

/**
 *
 * @author talbers
 */
public class ReportManager {
    
    static org.slf4j.Logger logger = LoggerFactory.getLogger("PickReport");
    public JList<String> lstReports;
    static File fileNames;
    static public DefaultListModel rptNames = new DefaultListModel();
        
    public ReportManager(){
        try {
            File dir = new File("Reports/");
            //setup the dialog form
            GridBagConstraints gbcC = new GridBagConstraints();
            JDialog dlgDisplay = new JDialog(Shoot_Winners.frame, "Report Manager!", true);
            dlgDisplay.setLayout(new GridBagLayout());
            gbcC.gridx = 0;
            gbcC.gridy = 0;
            gbcC.weightx = 0;
            gbcC.weighty = 0;
            gbcC.fill = GridBagConstraints.HORIZONTAL;
            //Setup top Panel
            JPanel topPnl = new JPanel();
            //Add reports to the ArrayList            
            JList<String> lstReports = new JList<String>(rptNames);
            GetReports(dir);
            JScrollPane jsReports = new JScrollPane(lstReports);
            jsReports.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                    "Current Reports", TitledBorder.LEFT, TitledBorder.ABOVE_TOP,null, Color.BLACK));
            jsReports.setPreferredSize(new Dimension(170, 170));           
            topPnl.add(jsReports, gbcC);           
            JButton btnAdd = new JButton();
            btnAdd.setText("Add Report");
            btnAdd.setPreferredSize(new Dimension(150,40));
            btnAdd.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent evt0)
                {
                    try {
                        //Have them pick the new jrxml report
                        if (SelectNewReport(dir)){
                            GetReports(dir);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(ReportManager.class.getName()).log(Level.SEVERE, null, ex);
                    }      
                }
            });
            JPanel btmPanel = new JPanel();
            btmPanel.add(btnAdd);            
            JButton btnDone = new JButton();
            btnDone.setText("Done");
            btnDone.setPreferredSize(new Dimension(150,40));
            btnDone.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent arg0) 
                {                  
                    dlgDisplay.dispose();
                }
            }) ;            
            dlgDisplay.getContentPane().add(topPnl, gbcC); 
            gbcC.gridx = 0;
            gbcC.gridy = 1;
            btmPanel.add(btnDone);
            dlgDisplay.getContentPane().add(btmPanel, gbcC);
            dlgDisplay.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            dlgDisplay.setSize(400, 50);
            dlgDisplay.setLocationRelativeTo(frame); 
            dlgDisplay.pack();
            dlgDisplay.setResizable(false);
            dlgDisplay.setVisible(true);             
        } catch (Exception e){
            logger.error("Error " + e.getMessage());
            e.printStackTrace();
        }
    }
         
    public static void GetReports(File dir){
        //Fill out the Model
        rptNames.clear();
        File[] strReports = dir.listFiles((d,name) -> name.endsWith(".jrxml"));
        for (int i = 0; i < strReports.length; i++){
            rptNames.add(i,strReports[i].getPath().substring(8, (strReports[i].getPath().length() - 6)));
        } 
    }
    public static boolean SelectNewReport(File dir) throws IOException{
        //Select the file you want to import        
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setCurrentDirectory(new File(defaultDirectory));
        fileChooser.setDialogTitle("Select Report");
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Files: (.jrxml)", "jrxml"));
        fileChooser.setAcceptAllFileFilterUsed(false);    
        int result = fileChooser.showOpenDialog(frame);
        if (result == JFileChooser.APPROVE_OPTION){
            //defaultDirectory = fileChooser.getSe
            File selectedFile = fileChooser.getSelectedFile();
            //Set the default path for FileChooser to current location
            defaultDirectory = selectedFile.getParent();
            fileNames = fileChooser.getSelectedFile();           
            //Move the jrxml file into the reports directory
            Path tmp = Files.move(selectedFile.toPath(), Paths.get(dir + "/" + selectedFile.getName()));
            //Move the jasper file to the reports directory      
            if (Files.exists(Paths.get(selectedFile.getPath().substring(0,(selectedFile.getPath().length()-5))+ "jasper"))){
                tmp = Files.move(Paths.get(selectedFile.getPath().substring(0,(selectedFile.getPath().length()-5))+ "jasper"), Paths.get(dir + "/" + selectedFile.getName().substring(0,(selectedFile.getName().length()-5))+ "jasper"));
            }
            return true;
        } 
        //something is wrong
        return false;
    }    
}
