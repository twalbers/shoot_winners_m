/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.errorPrint;
import static com.bitskc.shoot.winners.Database.stmt;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.slf4j.LoggerFactory;
import static com.bitskc.shoot.winners.Shoot_Winners.frame;
/**
 *
 * @author talbers
 */
public class CatChange {
    ArrayList<String> origCat = new ArrayList<>();
    ArrayList<String> newCat = new ArrayList<>();
    static org.slf4j.Logger logger = LoggerFactory.getLogger("CatChange");
    JDialog dialog;
    JPanel cntrPnl;
    public CatChange(){
        ResultSet rsCat;  //Unique categories
        String SQL;
        int numDSCat;   //how many distinct categories do we have
        GridBagConstraints gbcI = new GridBagConstraints();
        SQL = "Select count(distinct(category)) as totalRows from raw where category is not null and category <> ''";
        //Get the number of unique categories in this shoot
        try {
            rsCat = stmt.executeQuery(SQL);
            rsCat.next();
            numDSCat = rsCat.getInt("totalRows");   //Figure out how many categories we have
            SQL = "Select distinct(category) from raw where category is not null and category <> '' order by category";
            rsCat = stmt.executeQuery(SQL); //fill out recordset with distinct categories
            rsCat.next();
        } catch (Throwable e){
            errorPrint(e);
            logger.error(e.getMessage()); 
            return;
        }
        //Set a string with used later
        String[] lstCat = new String[numDSCat + 1];
        lstCat[0] = "";
        try {  
            for (int j = 1; j <= numDSCat; j++){
                lstCat[j] = (rsCat.getString("Category"));
                if (j != numDSCat){
                    rsCat.next();
                } else rsCat.first();
            }                
        } catch (Throwable e){
            errorPrint(e);
            logger.error(e.getMessage()); 
            return;
        } 
        //Now we have the categories involved display them on a form
        dialog = new JDialog(Shoot_Winners.frame, "Category Edits", true);
        dialog.setLayout(new GridBagLayout());
        gbcI.gridx = 0;
        gbcI.gridy = 0;
        gbcI.weightx = 1;
        gbcI.weighty = 1;
        gbcI.fill = GridBagConstraints.HORIZONTAL;
        //Setup top Panel
        JLabel lbl = new JLabel("Make any needed changes to categories");
        lbl.setFont(new Font("Serif", Font.BOLD, 16));
        JPanel topPnl = new JPanel();
        topPnl.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        //topPnl.setLayout(new GridBagLayout());
        //topPnl.setPreferredSize(new java.awt.Dimension(120,30));
        topPnl.add(lbl);
        dialog.getContentPane().add(topPnl, gbcI);  
        //Now setup center panel with checkboxes
        cntrPnl = new JPanel();
        cntrPnl.setLayout(new GridBagLayout());
        int x = 0;
        gbcI.gridx = 0;
        gbcI.gridy = 0;
        gbcI.insets = new Insets(0,0,0,0);   //top, left, bottom, right  
        gbcI.ipadx = 0;
        gbcI.ipady = 0;
        gbcI.gridwidth = 1;
        JLabel lblCat;
        JComboBox cmbCat;
        int numCat;
        try {
            for (numCat = 1; numCat <= numDSCat; numCat++){
                lblCat = new JLabel();
                gbcI.gridx = x;
                gbcI.gridy = ((numCat-1) / 4);
                gbcI.insets = new Insets(0,5,0,0);   //top, left, bottom, right 
                gbcI.anchor = GridBagConstraints.LINE_END;               
                lblCat.setName(rsCat.getString("Category"));
                lblCat.setText(rsCat.getString("Category") + "->");     
                lblCat.setHorizontalTextPosition(JLabel.RIGHT);
                lblCat.setHorizontalAlignment(SwingConstants.RIGHT);              
                cntrPnl.add(lblCat,gbcI);
                //Now load the ComboBox
                x++;
                cmbCat = new JComboBox<>(lstCat);  
                gbcI.anchor = GridBagConstraints.LINE_START;
                gbcI.gridx = x;
                gbcI.insets = new Insets(0,0,0,5);   //top, left, bottom, right 
                cmbCat.setName(rsCat.getString("Category"));
                cmbCat.setSelectedItem(rsCat.getString("Category"));
                cmbCat.setEditable(true);
                cmbCat.setPreferredSize(new Dimension(60,25));
        //        cmbCat.addActionListener(new Import.cmbCatActionListener());
                cntrPnl.add(cmbCat, gbcI);
                rsCat.next();
                x++;
                if (x == 8)x = 0;                
            }
        } catch (Throwable e){
            errorPrint(e);
            logger.error(e.getMessage()); 
            return;
        }
        if ((numDSCat % 4) != 0){
            numCat = (numDSCat / 4 + 1) * 25;  //Set the height of the form
        } else {
            numCat = (numDSCat / 4) * 25;  //Set the height of the form
        }
        cntrPnl.setPreferredSize(new java.awt.Dimension(500,numCat));
        gbcI.gridx = 0;
        gbcI.gridy = 1;
        gbcI.insets = new Insets(0,0,0,0);   //top, left, bottom, right  
        dialog.getContentPane().add(cntrPnl, gbcI); 
        JPanel btmPanel = new JPanel();
        JButton btnOK = new JButton();
        btnOK.setText("Continue");
        btnOK.setPreferredSize(new Dimension(150,40));
        btnOK.addActionListener(new btnActionListener());                 
        btmPanel.add(btnOK);
        gbcI.gridy = 2;
        dialog.getContentPane().add(btmPanel, gbcI);
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dialog.setSize(500, numCat);
        dialog.setLocationRelativeTo(frame); 
        dialog.pack();
        dialog.setResizable(false);
        dialog.setVisible(true);
    }
    private class btnActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            origCat.clear();
            newCat.clear();
            //Figure out what changes, to category, need to be made (if any)          
            Component[] components = cntrPnl.getComponents();
            for (Component c : components){
                if (c instanceof JComboBox){
                    if (!((JComboBox)c).getName().equals(((JComboBox)c).getSelectedItem().toString())){  //this means something has been altered
                        origCat.add(((JComboBox)c).getName());
                        newCat.add(((JComboBox)c).getSelectedItem().toString());
                    }                 
                }               
            }                         
            //Make needed changes to categories
            if (!origCat.equals("")){  //we have something to change
                Iterator<String> itrOrig = origCat.iterator();
                Iterator<String> itrNew = newCat.iterator();
                String SQL;
                while (itrOrig.hasNext()){
                    SQL = "update raw set category = '" + itrNew.next() + "' where category = '" + itrOrig.next() + "'";
                    try {
                        stmt.executeLargeUpdate(SQL);
                    } catch (SQLException ex) {;
                        logger.error("Error on the following statement: " + SQL);
                        logger.error(ex.getMessage());
                        origCat.clear();    //holds the original category to change
                        newCat.clear();     //holds the new category 
                        closeDialog();
                    } 
                }
                origCat.clear();
                newCat.clear();
                closeDialog();
            }
        }    
    }
    public void closeDialog() {        
        dialog.dispose();        
    }    
}
