/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import com.apple.eawt.Application;
import java.awt.Image;
import java.awt.event.KeyEvent;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.text.DefaultEditorKit;
import org.slf4j.LoggerFactory;
import javax.swing.UIManager;
/**
 *
 * @author talbers
 */
public class macSettings extends Shoot_Winners{
    static org.slf4j.Logger logger = LoggerFactory.getLogger("Mac Settings");  
    public static InputMap im;
    public static void setMacDock(){
        try { 
            Application.getApplication().setDockIconImage(ImageIO.read(Shoot_Winners.class.getResource("/images/shootwinners.png")));  //set Dock icon            
        } catch (Exception e){
            logger.error("Error setting Mac Dock Icon");
            logger.error(e.getMessage());
        }
    }
    public static void setMacSettings(){
        try {           
            im = (InputMap) UIManager.get("TextField.focusInputMap");
            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.META_DOWN_MASK), DefaultEditorKit.copyAction);
            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.META_DOWN_MASK), DefaultEditorKit.pasteAction);
            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.META_DOWN_MASK), DefaultEditorKit.cutAction);            
        } catch (Exception e){
            logger.error("Error setting Mac settings");
            logger.error(e.getMessage());
        }
    }
}
