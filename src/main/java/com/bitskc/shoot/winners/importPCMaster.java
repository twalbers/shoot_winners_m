/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.slf4j.LoggerFactory;

/**
 *
 * @author talbers
 */
public class importPCMaster {
    static String defaultDirectory = System.getProperty("user.home");  
    //static String fileName;
    static File fileName;
    static org.slf4j.Logger logger = LoggerFactory.getLogger("Import");
    public static int curLine = 0;
    private static Statement stmt;
    public static boolean ImportData(){
        ResultSet rs;
        //Select the file you want to import        
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setCurrentDirectory(new File(defaultDirectory));
        fileChooser.setDialogTitle("Select Shoot Winners Import File");
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Files: (.swc)", "swc"));
        fileChooser.setAcceptAllFileFilterUsed(false);    
        int result = fileChooser.showOpenDialog(Shoot_Winners.frame);
        if (result == JFileChooser.APPROVE_OPTION){
            //defaultDirectory = fileChooser.getSe
            File selectedFile = fileChooser.getSelectedFile();
            //Set the default path for FileChooser to current location
            defaultDirectory = selectedFile.getParent();
            fileName = fileChooser.getSelectedFile();           
            //Import the selected file
            if (!Open_Import_File(fileName)){
                logger.error("Could not import file");
                JOptionPane.showMessageDialog(null, "Could not import file, is it in the correct format?",
                            "Error!",JOptionPane.ERROR_MESSAGE);  
                closeStmt();
                return false;
            } 
            //File has been imported to table TEST, now append the data in the correct table, don't import ones that exists
            try {              
                String SQL; 
                Progress.showProgress("Importing File", "Please wait till import is complete!", 0);                
                SQL = "insert into contact (atanum, name, phonenum) select test.num as atanum, test.name, test.phonenum from test left join contact c on test.num = c.atanum"
                        + " where c.atanum is null order by test.num";
                stmt.executeUpdate(SQL);    
                SQL = "Delete from test";
                stmt.executeUpdate(SQL);
            } catch (SQLException e){
                logger.error(e.getMessage()); 
                errorPrint(e);
            } finally {
                Progress.closeProgress();
            }  
            closeStmt();
            return true;
            
        } 
        //something is wrong
        closeStmt();
        return false;
    } 
    public static void closeStmt(){
        if (stmt != null){
            try{
                stmt.close();
            } catch (SQLException e) {}
        }
    }
    public static boolean Open_Import_File(File fileName){  
            String ATANum;
            String Name;
            String SQL;
            String Phone;
            //This will get one line at a time
            String line = null; 
            try {
                stmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);    
                //FileReader reads text files in the default encoding
                FileReader fileReader = new FileReader(fileName);            
                //Always wrap FileReader in Bufferedreader
                BufferedReader bufferedReader = new BufferedReader(fileReader);  
                //If the file exists delete it
                File file = new File(fileName + ".del");
                if (file.delete());
                BufferedWriter out = new BufferedWriter(new FileWriter(fileName + ".del"));
                curLine = 1; 
                //We need to change the file format to be ATANumber%Name%PhoneNumber
                while ((line = bufferedReader.readLine()) != null){
                    String[] lineData = line.split(",");
                    if (lineData.length == 5){  //A lame attempt to make sure we are importing the correct file
                        //Put data into table                                         
                        ATANum = lineData[0];
                        Name = lineData[1].trim();                        
                        Name = Name.replaceAll("'","\\\'");     //helps keep single quotes
                        Name = Name.replace("\"","");   //replaces all double quotes
                        if (lineData[3].length() < 10) 
                            Phone = ""; 
                        else {
                            Phone = lineData[3];                               
                        }
                        out.write(ATANum + "%" + Name + "%" + Phone + "\n");                            
                    }                
                    curLine ++;
                }     
                //Make sure to close the file
                bufferedReader.close();
                out.close();
                //Clear out the test table
                SQL = "Delete from Test";
                stmt.executeUpdate(SQL);
                //Import everything to a temp table called TEST
                SQL = "CALL SYSCS_UTIL.SYSCS_IMPORT_TABLE(null,'TEST','" + fileName + ".del','%',null,null,0)";
                stmt.executeUpdate(SQL);
                file = new File(fileName + ".del");
                if (file.delete());                
            } catch(FileNotFoundException ex) {
                logger.error("Unable to open file '" + Constants.fileName + "'");
                return false;
            } catch(IOException ex){
                logger.error("Error reading file '" + Constants.fileName + "'");
                return false;
            } catch(SQLException ex){
                if (!ex.getSQLState().equals("X0Y78")){
                    return false;
                } 
                return true;
            } catch(Throwable e){
                errorPrint(e);
                logger.error(e.getMessage());
                return false;
            }       
        return true;
    }
    static void errorPrint(Throwable e) {
        if (e instanceof SQLException)
            SQLExceptionPrint((SQLException)e);
        else {
            logger.error("Error in the SMS module");
            logger.error((e).getMessage());
            e.printStackTrace();
        }
    }
    
    //  Iterates through a stack of SQLExceptions
    static void SQLExceptionPrint(SQLException sqle) {
        while (sqle != null) {
            logger.debug("\n---SQLException Caught---\n");
            logger.debug("SQLState:   " + (sqle).getSQLState());
            logger.debug("Severity: " + (sqle).getErrorCode());
            logger.debug("Message:  " + (sqle).getMessage());
            sqle.printStackTrace();
            sqle = sqle.getNextException();
        }
    }     
}
