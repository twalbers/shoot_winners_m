/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.MaskFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author talbers
 */
public class SMS {    
    //public JDialog dlgSMS;
    public JFrame dlgSMS;
    public JTextField txtName;
    public JFormattedTextField txtATA;
    public JFormattedTextField txtPhone;
    public JCheckBox ckConcent;
    public JScrollPane sbContact;    
    static Logger logger = LoggerFactory.getLogger("SMS"); 
    static public JList<String> contactList, lstEvents;
    static public DefaultListModel<String> contacts = new DefaultListModel<String>();    
    private static ArrayList<String> defaultValues = new ArrayList<>();
    public JRadioButton rbATA;
    public JRadioButton rbName;
    public JRadioButton rbPhone;
    public JRadioButton rbConcent;
    public ButtonGroup btnFilter = new ButtonGroup();
    public static Statement stmt;
    private static String vers;
    private static Font fnt = new Font("Arial", Font.PLAIN, 15); 
    
    public SMS() throws ParseException{             
     //   new dbConnection();   //used for testing
        try{
            stmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
        } catch (Throwable e){
            logger.error("Error setting the statement in SMS");
            errorPrint(e);
        }      
        dlgSMS = new JFrame("Contacts");  
        dlgSMS.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);  
        dlgSMS.addWindowListener(new WindowAdapter(){   //Make sure to close the database on exit
            public void windowClosing(WindowEvent e){
                if (stmt != null){
                    try {
                        stmt.close();
                    } catch (SQLException ex){                        
                    }
                }
            }
        });
        JPanel pnl = new JPanel();
        pnl.setLayout(new GridBagLayout());
        GridBagConstraints gbd = new GridBagConstraints();
        //Add ATAnum controls        
        gbd.gridx = 0;
        gbd.gridy = 0;
        gbd.fill = GridBagConstraints.HORIZONTAL;
        gbd.insets = new Insets(0,10,0,0);    //top, left, bottom, right    
        rbATA = new JRadioButton("ATA #");
        rbATA.setActionCommand("ATA");
        rbATA.setSelected(true);
        btnFilter.add(rbATA);
        rbATA.setFont(fnt);
        pnl.add(rbATA, gbd);
        gbd.gridy = 1;
        gbd.insets = new Insets(0,5,0,0);    //top, left, bottom, right  
        MaskFormatter ata = new MaskFormatter("## #####");
        ata.setPlaceholderCharacter('_');  
        txtATA = new JFormattedTextField(ata);
        txtATA.setFont(fnt);
        txtATA.setPreferredSize(new Dimension(75, 30));
        txtATA.addFocusListener(new FocusListener(){
            public void focusLost(FocusEvent e){loadInfo();}    //This just loads all the info in the txtFields for easier update
            public void focusGained(FocusEvent e){}
        });
        //Setup a filter for the list
        txtATA.getDocument().addDocumentListener(new DocumentListener(){
            @Override public void insertUpdate(DocumentEvent e) { filter(); }
            @Override public void removeUpdate(DocumentEvent e) { filter(); }
            @Override public void changedUpdate(DocumentEvent e) {}
            public void filter(){   
                filterSet();
            }
        });   
        pnl.add(txtATA, gbd);
        //Add Name controls
        gbd.gridx = 1;
        gbd.gridy = 0;
        gbd.insets = new Insets(0,10,0,0);    //top, left, bottom, right   
        rbName = new JRadioButton("Name");
        rbName.setActionCommand("Name");
        btnFilter.add(rbName);
        rbName.setFont(fnt);
        pnl.add(rbName, gbd);
        gbd.gridy = 1;
        gbd.insets = new Insets(0,5,0,0);    //top, left, bottom, right         
        txtName = new JTextField();
        txtName.setFont(fnt);
        txtName.setPreferredSize(new Dimension(150, 30));     
        //Setup a filter for Name
        txtName.getDocument().addDocumentListener(new DocumentListener(){
            @Override public void insertUpdate(DocumentEvent e) { filter(); }
            @Override public void removeUpdate(DocumentEvent e) { filter(); }
            @Override public void changedUpdate(DocumentEvent e) {}
            public void filter(){            
                filterSet(); 
            }
        });

        pnl.add(txtName, gbd);
        //Add phone controls        
        gbd.gridx = 2;
        gbd.gridy = 0;
        gbd.insets = new Insets(0,10,0,0);    //top, left, bottom, right  
        rbPhone = new JRadioButton("Phone #");
        rbPhone.setActionCommand("Phone");
        btnFilter.add(rbPhone);        
        rbPhone.setFont(fnt);
        pnl.add(rbPhone, gbd);
        gbd.gridy = 1;
        gbd.insets = new Insets(0,5,0,0);    //top, left, bottom, right 
        //Only allow numbers and have a format
        MaskFormatter ph = new MaskFormatter("(###) ###-####");
        ph.setPlaceholderCharacter('_');
        txtPhone = new JFormattedTextField(ph);
        txtPhone.setFont(fnt);
        txtPhone.setPreferredSize(new Dimension(115, 30));
        //setup a filter for phone
        txtPhone.getDocument().addDocumentListener(new DocumentListener(){
            @Override public void insertUpdate(DocumentEvent e) { filter(); }
            @Override public void removeUpdate(DocumentEvent e) { filter(); }
            @Override public void changedUpdate(DocumentEvent e) {}
            public void filter(){    
                filterSet();
            }
        });

        pnl.add(txtPhone, gbd);
        //Add Concent controls        
        gbd.gridx = 3;
        gbd.gridy = 0; 
        rbConcent = new JRadioButton("Send txt");
        rbConcent.setActionCommand("Concent");                    
        btnFilter.add(rbConcent);      
        rbConcent.setFont(fnt);
        pnl.add(rbConcent, gbd);
        gbd.gridy = 1;
        gbd.insets = new Insets(0,25,0,0);    //top, left, bottom, right          
        ckConcent = new JCheckBox();
        ckConcent.setFont(fnt);
        //setup a filter for concent
        ckConcent.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                filterSet();
            }
        });
        pnl.add(ckConcent, gbd);
        //Add Button
        gbd.gridx = 4;
        gbd.insets = new Insets(0,0,0,0);    //top, left, bottom, right          
        JButton btnOK = new JButton();
        btnOK.setFont(fnt);
        btnOK.setText("OK");     
        btnOK.addActionListener(new ActionListener(){
            @Override
            @SuppressWarnings("ResultOfObjectAllocationIgnored")
            public void actionPerformed(ActionEvent arg0) 
            {    
                if (!txtATA.getText().contains("_") && !txtPhone.getText().contains("_") && !txtName.equals("")){   //make sure the ATA number is complete
                    String SQL;
                    ResultSet rsC;
                    String strPhone = txtPhone.getText().replace(" ", "").replace("(", "").replace(")", "").replace("-","");    //remove all the unwanted characters                      
                    Boolean c = ckConcent.isSelected();
                    try {
                        SQL = "Select * from contact where atanum = '" + txtATA.getText().replaceAll(" ", "") + "'";                         
                        rsC = stmt.executeQuery(SQL);
                        if (rsC.next()){    //this means we have a match so update
                            SQL = "Update Contact set name = '" + txtName.getText().trim().toUpperCase().replaceAll("'","\\\'\\\'") + 
                                    "', Phonenum = '" + strPhone + "', concent = " + c + " where atanum = '" 
                                    + txtATA.getText().replaceAll(" ", "") + "'";
                            stmt.executeUpdate(SQL);                            
                        } else {    //no records so insert record
                            SQL = "Insert into Contact (ATANum, Name, Phonenum, Concent) values ('" + 
                                    txtATA.getText().replaceAll(" ", "") + "', '" + txtName.getText().toUpperCase().replaceAll("'","\\\'\\\'") 
                                    + "', '" + strPhone + "', " + c + ")";
                            stmt.executeUpdate(SQL);
                        }
                        DefaultListModel model = (DefaultListModel) contactList.getModel();     //Get the ListModel in use
                        model.setSize(0);   //clear the list model
                        getContacts();  //reload the list model   
                        txtATA.requestFocus();  //Set Focus to the ATA number and clear it out
                        txtATA.setText("");
                    } catch (Throwable ex){
                        errorPrint(ex);
                    }                    
                }                         
            }            
        });
        pnl.add(btnOK, gbd);
        //Add Contact List
        contactList = new JList<String>(getContacts());
        contactList.setVisibleRowCount(30);
        contactList.setFont(fnt);
        contactList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);       
        //Add action listener to load the values to the edit boxes
        contactList.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e) {
                //make sure we have something in the list 
                txtATA.setText("");   
                if (contactList.getSelectedIndex() != -1){
                    String s = contactList.getSelectedValue();                 
                    txtATA.setText(s.substring(0, 8));
                    txtName.setText(s.substring(12, 32).trim());
                    if (s.length() > 45){
                        txtPhone.setText(s.substring(36, 46)); 
                    } else {
                        txtPhone.setText("");
                    }
                    if (s.length() > 50 && s.substring(51).equals("true")){
                        ckConcent.setSelected(true);
                    } else {
                        ckConcent.setSelected(false);
                    }                    
                    txtPhone.requestFocus();
                }
            }
        });         
        gbd.gridx = 0;
        gbd.gridy = 2;
        gbd.gridwidth = 5;
        gbd.insets = new Insets(10,0,0,0);    //top, left, bottom, right          
        gbd.fill = GridBagConstraints.BOTH;
        sbContact = new JScrollPane(contactList);
        sbContact.setFont(fnt);
        sbContact.setPreferredSize(new Dimension(510, 600));
        pnl.add(sbContact, gbd);
        dlgSMS.add(pnl);     
        //Add menu to frame
        JMenuBar mb = new JMenuBar();
        JMenu file = new JMenu("Import");
        JMenuItem pcTrap = new JMenuItem("SWC File");
        pcTrap.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                importPCMaster.ImportData();
                getContacts();  //Add new to list
            }
        });
        file.add(pcTrap);        
        mb.add(file);      
        dlgSMS.setJMenuBar(mb);  
        dlgSMS.pack();
        //dlg.setSize(200, 60);
        dlgSMS.setLocationRelativeTo(Shoot_Winners.frame); 
        dlgSMS.setResizable(false);
        dlgSMS.setVisible(true);
        //Set focus to the txtATA JTextField
        dlgSMS.addWindowListener(new WindowAdapter(){
            public void windowOpened(WindowEvent e){
                txtATA.requestFocus();
            }
        });
        try {
            vers = System.getProperty("os.name").toLowerCase();
            if (vers.indexOf("windows") != -1 && !vers.equals("")){
                logger.debug("Setting icon");
                InputStream imgInputStream = dlgSMS.getClass().getResourceAsStream("/images/shootwinners.png");
                BufferedImage bufferedImage = ImageIO.read(imgInputStream);
                dlgSMS.setIconImage(bufferedImage);                
            } 
        } catch (Throwable ex){
            logger.error("Error setting Main form icon");
            errorPrint(ex);
        }        
    }
    public void loadInfo(){     //This is used by the lost focus of the ATAnum to make updating easier      
        if (contactList.getModel().getSize() == 1){ //Only do this if there is one value, that should be most of the time
            contactList.setSelectedIndex(0);
            String s = contactList.getSelectedValue();
            txtATA.setText(s.substring(0, 8));
            txtName.setText(s.substring(12, 32).trim());
            if (s.length() > 45){
                txtPhone.setText(s.substring(36, 46)); 
            } else {
                txtPhone.setText("");
            }
            if (s.length() > 50 && s.substring(51).equals("true")){
                ckConcent.setSelected(true);
            } else {
                ckConcent.setSelected(false);
            }   
        } else {
            txtName.setText("");
            txtPhone.setText("");
            ckConcent.setSelected(false);
        }       
    }
    public void filterModel(DefaultListModel<String> model, String filter, String SentBy) {     //Add and removes elements from for filtering  
        for (String s : defaultValues) {
            if (SentBy.equals("ATA") && !s.startsWith(filter)){
                model.removeElement(s);
            }else if (!s.contains(filter)) {
                model.removeElement(s);
            } else {
                if (!model.contains(s) && model.getSize() < 100) {
                    model.addElement(s);
                }
            }
        }
    }
    public static DefaultListModel<String> getContacts(){   //Get our current contacts from the db and load them
        // load contacts
        ResultSet rsContacts;
        defaultValues.clear(); 
        DefaultListModel<String> dmContacts = new DefaultListModel<String>();  
        String list;
        String SQL = "Select * from contact order by atanum";
        try {
            rsContacts = stmt.executeQuery(SQL);            
            while (rsContacts.next()){
                list = rsContacts.getString("ATANum") + "     ";
                list = list + rsContacts.getString("Name");
                for (int i = rsContacts.getString("Name").length(); i < 19; i++){
                    list = list + " ";
                }
                list = list + "     ";
                if (rsContacts.getString("PhoneNum") != null){
                    list = list + rsContacts.getString("PhoneNum") + "     ";
                    list = list + rsContacts.getBoolean("concent");
                }                
                if (dmContacts.getSize() < 100) {dmContacts.addElement(list);}  //To speed up the program only show 100 names
                defaultValues.add(list);    //This is the entire list used as a master for filtering
            }
        } catch (Throwable e){
            errorPrint(e);
        }     
        return dmContacts;
    }
    public static void addSMStoDB(){    //Add records to the SMS table for texting at a later time
        ResultSet rs;
        String SQL;
        String strEventName;
        String strMessage;
        int result = 4;
        try {
            stmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            SQL = "Select count(atanum) as txtCount, eventname from results join contact on membernum = atanum where concent = true group by eventname";
            rs = stmt.executeQuery(SQL);
            if (rs.next()){ //If anyone has concented then continue
                int txtCount = rs.getInt("txtCount");
                strEventName = rs.getString("Eventname");          
                if (txtCount > 0){  //we only need to do this if we have people that concent to getting a text
                    //Check the table, if there are already results from this event and the texts have been sent.  Let them know that nothing will be done 
                    SQL = "Select count(*) as txtComplete from SMS where eventname = '" + strEventName + "' and ResultSID is not null";
                    rs = stmt.executeQuery(SQL);
                    rs.next();
                    if (rs.getInt("txtComplete") > 0){  //Text message have already been sent for this event so what to do
                        String message = "<html>Texts have already been sent for this event.";
                        message = message + "<br>Select No if you just want to run the report.";
                        message = message + "<br>Select Yes if you want to be able to reset texting for this event.</html>";
                        JLabel lbl = new JLabel(message);
                        lbl.setFont(fnt);
                        result = JOptionPane.showConfirmDialog(Shoot_Winners.frame, lbl, "Reset Texing?"
                            , JOptionPane.YES_NO_OPTION);
                    } 
                    if(result == JOptionPane.YES_OPTION || result == 4) {   //If they select yes then texting is reset, no does nothing.
                        SQL = "Delete from SMS where eventname = '" + strEventName +"'";
                        stmt.executeUpdate(SQL);
                        SQL = "select r.membernum,r.eventname, c.phonenum, ('' ||r.membernum || '  is in a shoot off for event ' || r.eventname || ', for ' || r.place ||"
                                + " '. Please report to shoot management.  To Opt out reply: Stop') as message from results r join contact c on membernum = atanum "
                                + "where c.concent = true and extra = 'Shoot Off'";                    
                        rs = stmt.executeQuery(SQL);
                        Statement txstmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);                    
                        while (rs.next()){      //add the results to the sms table 
                            if (rs.getString("message").length() > 249) {
                                strMessage = rs.getString("message").substring(0, 249);
                            } else {
                                strMessage = rs.getString("message");
                            }
                            SQL = "Insert into sms(atanum, phonenum, message, eventname) values "
                                    + "('" + rs.getString("membernum") + "', '" + rs.getString("phonenum") + "', '" 
                                    + strMessage + "', '" + rs.getString("eventname") + "')";
                            txstmt.executeUpdate(SQL);
                        }   
                        txstmt.close();                   
                    }  
                }                
            }
            stmt.close();
        } catch (Throwable e){
            errorPrint(e);
        }        
    }
    public static void selectSMSEvent(){        
        try{
            stmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
        } catch (Throwable e){
            logger.error("Error setting the statement in SMS");
            errorPrint(e);
        }
//        JList<String> lstEvents = new JList<String>(getEvents());  
        lstEvents = new JList<String>(getEvents());  
        if (lstEvents.getModel().getSize() == 0){
            JOptionPane.showMessageDialog(Shoot_Winners.frame, "Nothing to do!", "Error!", JOptionPane.OK_OPTION);
            try{
                stmt.close();
            } catch (Throwable ex){
                errorPrint(ex);
            }
            return;
        } 
        JDialog selEvent = new JDialog(Shoot_Winners.frame, "Select Events", true);
        selEvent.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        lstEvents.setFont(fnt);
        lstEvents.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); 
        lstEvents.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e) {
                //an event has been selected so ask for confirmation and send texts
                if (lstEvents.getSelectedIndex() != -1){
                    SMSPopUpMenu menu = new SMSPopUpMenu();
                    menu.show(e.getComponent(),e.getX(),e.getY());
                    /*
                    int dlgResult = JOptionPane.showConfirmDialog(Shoot_Winners.frame, "Confirm that you want to send text alerts for event - "
                            + lstEvents.getSelectedValue(), "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (dlgResult == 0){
                        sendSMS(lstEvents.getSelectedValue());  //send texts
                        JOptionPane.showMessageDialog(Shoot_Winners.frame, "Texts have been sent", "Complete!", JOptionPane.INFORMATION_MESSAGE);
                    }  */                   
                }             
            }
        }); 
        JScrollPane pnl = new JScrollPane(lstEvents);
        pnl.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                "Select Event to send Text alert to", TitledBorder.LEFT, TitledBorder.ABOVE_TOP,null, Color.BLACK));
        pnl.setFont(fnt);
        pnl.setPreferredSize(new Dimension(500, 200));                
        selEvent.add(pnl);
        selEvent.pack();
        selEvent.setLocationRelativeTo(Shoot_Winners.frame); 
        selEvent.setResizable(false);
        selEvent.setVisible(true);                 
    }
    private static DefaultListModel<String> getEvents(){
        ResultSet rsEvents;
        DefaultListModel<String> dmEvents = new DefaultListModel<String>();  
        String SQL = "Select Eventname from sms where resultsid is null group by eventname order by eventname";
        try {
            rsEvents = stmt.executeQuery(SQL);            
            while (rsEvents.next()){
                dmEvents.addElement(rsEvents.getString("EventName"));
            }
        } catch (Throwable e){
            logger.error("Error setting list for txt events");
            errorPrint(e);
        }     
        return dmEvents;
    }
    public static int sendSMS(String eventName){
        //Check to see if anyone has opted to get an SMS
        ResultSet rs;
        String SQL;  
        String strResult;
        Statement stResult;
        int count = 0;
        try {
            stmt = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stResult = Constants.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            SQL = "Select * from sms where resultSID is null and eventname = '" + eventName + "'";
            rs = stmt.executeQuery(SQL);
            while (rs.next()){
                strResult = TwilioSMS(rs.getString("phonenum"), rs.getString("Message"));
                //Now we need to add the ReturnSID to the Local DB
                SQL = "Update sms set ResultSID = '" + strResult + "' where id = " + rs.getInt("ID");
                stResult.executeUpdate(SQL);
                count += 1;
            }
            stmt.close();
            stResult.close();
        } catch (Throwable e){
            errorPrint(e);
        }     
        return count;
    }
    public static String TwilioSMS(String number, String SMStext){
        Twilio.init(Constants.ACCOUNT_SID, Constants.AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber(number), new PhoneNumber(Constants.PhoneNum),
            SMStext).create();  
        return((message.getSid()));
    }               
    public void filterSet(){ 
        String fltType = btnFilter.getSelection().getActionCommand();
        String filter = "";
        if (fltType.equals("ATA")){
            filter = txtATA.getText();
            filter = filter.replaceAll("\\s+", ""); //remove all white space
            filter = filter.replace("_", "");   //remove all underscores                                             
        } else if (fltType.equals("Name")){
            filter = txtName.getText().toUpperCase();
            filter = filter.trim();       
        } else if (fltType.equals("Phone")){
            filter = txtPhone.getText();
            filter = filter.replaceAll("\\s+", ""); //remove all white space
            filter = filter.replace("_", "");   //remove all underscores
            filter = filter.replace("(","");
            filter = filter.replace(")","");
            filter = filter.replace("-","");            
        } else if (fltType.equals("Concent")){
            boolean y = ckConcent.isSelected();
            filter = String.valueOf(y);
        }
        filterModel((DefaultListModel<String>)contactList.getModel(), filter, fltType); 
    }    
    static void errorPrint(Throwable e) {
        JOptionPane.showMessageDialog(null, "Error in the Texting Module, check log file!", "Error!", JOptionPane.ERROR_MESSAGE);
        if (e instanceof SQLException)
            SQLExceptionPrint((SQLException)e);
        else {
            logger.error("Error in the SMS module");
            logger.error((e).getMessage());
            e.printStackTrace();
        }
    }    
    //  Iterates through a stack of SQLExceptions
    static void SQLExceptionPrint(SQLException sqle) {
        while (sqle != null) {
            logger.debug("\n---SQLException Caught---\n");
            logger.debug("SQLState:   " + (sqle).getSQLState());
            logger.debug("Severity: " + (sqle).getErrorCode());
            logger.debug("Message:  " + (sqle).getMessage());
            sqle.printStackTrace();
            sqle = sqle.getNextException();
        }
    }     
}
