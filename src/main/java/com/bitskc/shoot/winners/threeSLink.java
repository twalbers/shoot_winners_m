/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Shoot_Winners.frame;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author talbers
 */
public class threeSLink {
    
    static Logger logger = LoggerFactory.getLogger("3SLink"); 
    public JComboBox<String> cmbEvent;
 
    public threeSLink(int numEvents){
        //<editor-fold defaultstate="collapsed">          
        try {     
            logger.debug("3S Import initiated.");            
            //setup the dialog form
            GridBagConstraints gbcC = new GridBagConstraints();
            JDialog dlgDisplay = new JDialog(Shoot_Winners.frame, "Select an Event to Import.", true);
            dlgDisplay.setLayout(new GridBagLayout());
            gbcC.gridx = 0;
            gbcC.gridy = 0;
            gbcC.weightx = 0;
            gbcC.weighty = 0;
            gbcC.fill = GridBagConstraints.HORIZONTAL;
            //Setup top Panel
            JLabel lbl = new JLabel("Pick an Event to Import:");
            lbl.setFont(new Font("Serif", Font.BOLD, 16));
            JPanel topPnl = new JPanel();
            topPnl.add(lbl, gbcC);
            gbcC.insets = new Insets(0,0,0,0);   //top, left, bottom, right  
            gbcC.gridx = 1;
            gbcC.anchor = GridBagConstraints.LINE_START;
            cmbEvent = new JComboBox<>();
            for (int i = 1; i <= numEvents; i++){
                cmbEvent.addItem(String.valueOf(i));
            } 
            cmbEvent.addItem("HAA");
            cmbEvent.addItem("HOA");
            cmbEvent.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                   if (cmbEvent.getSelectedItem().toString().equals("HAA") ||
                           cmbEvent.getSelectedItem().toString().equals("HOA")){
                        JOptionPane.showMessageDialog(null, "Make sure to run the HAA/HOA Report in 3S prior to selecting this option!",
                                    "Information!",JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            });
            cmbEvent.setName("Events");
            topPnl.add(cmbEvent, gbcC);
            gbcC.gridx = 0;
            gbcC.gridy = 0;
            dlgDisplay.getContentPane().add(topPnl, gbcC);
            JPanel btmPanel = new JPanel();
            JButton btnOK = new JButton();
            btnOK.setText("Continue");
            btnOK.setPreferredSize(new Dimension(150,40));
            btnOK.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent arg0) 
                {  
                    if (cmbEvent.getSelectedItem() != null){
                        dlgDisplay.dispose();
                        Constants.eventNum = cmbEvent.getSelectedItem().toString();
                        if (Constants.eventNum.equals("HAA") || Constants.eventNum.equals("HOA")){
                            Constants.eventName = Constants.eventNum;
                            //Get scores
                            if (Constants.eventNum.equals("HOA")){
                                ExecLink("GET" + Constants.eventNum,"true,false,false");
                            }else{
                                ExecLink("GET" + Constants.eventNum,"false,false,false");
                            }                            
                            if (Constants.eventNum.equals("HAA")){
                                Constants.eventName = "High All Around";                            
                            } else {
                                Constants.eventName = "High Over All";
                            }                            
                        } else {
                            //Get Event Name
                            if (threeSLink.ExecLink("GETEVENTDETAILS", Constants.eventNum)){
                                Constants.eventName = threeSLink.SngleResult();
                                 String[] lineData = Constants.eventName.split(",");
                                 Constants.eventName = lineData[2].trim();
                            }
                            //Get Event Scores
                            ExecLink("GETEVENTSCORES", Constants.eventNum + "," + "false,false");                                                           
                        }
                    } 
                }
            }) ;            
            dlgDisplay.getContentPane().add(topPnl, gbcC); 
            gbcC.gridx = 0;
            gbcC.gridy = 1;
            btmPanel.add(btnOK);
            dlgDisplay.getContentPane().add(btmPanel, gbcC);
            dlgDisplay.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            dlgDisplay.setSize(400, 50);
            dlgDisplay.setLocationRelativeTo(frame); 
            dlgDisplay.pack();
            dlgDisplay.setResizable(false);
            dlgDisplay.setVisible(true);             
        } catch (Exception e){
            logger.error("Error " + e.getMessage());
            e.printStackTrace();
        }           
    }
        //</editor-fold>
    
    static public boolean threeS(){
        //<editor-fold defaultstate="collapsed">          
        //This simply makes sure that the 3S files are in place and the link should work
        //The files we need to exist
        if (Constants.threeSImport && !Constants.threeSFileCheck) {   //We only need to do this if it has been selected
            String[] filesNeeded = new String[4];
            filesNeeded[0] = "c:/3S/ShootPro/Data/Server_HostName.txt";
            filesNeeded[1] = "3S/Elevate.ElevateDB.Data.dll";
            filesNeeded[2] = "3S/ShootingSportsSoftware.dll.dll";
            filesNeeded[3] = "3S/shootingsportssoftwareapi.exe";
            //Check to make sure each file exists
            for (String strFile : filesNeeded){
                File tmpFile = new File(strFile);
                if (!tmpFile.exists()){
                    //File does not exists so exit
                    JOptionPane.showMessageDialog(null, "Cannot create link to 3S, " + tmpFile + " missing!");
                    logger.error("Error locating 3S files!");
                    return false;
                }
            }  
        }

        //If we made it this far all the files exist
        Constants.threeSFileCheck = true;
        return true;
    }
        //</editor-fold>
    
    //Retrieve the number of Events
    static public int Events(){
        //Return Events
        if (ExecLink("GETMAXEVENT","")){
            return Integer.parseInt(SngleResult());
        }
        return 0;
    }
  
    //Read a single line from the restults file
    static public String SngleResult(){
        //<editor-fold defaultstate="collapsed">  
        try {
            logger.debug("3S Single Result fired.");
            //Command successfull get results
            File results = new File("results.csv");
            if (results.exists()){
                String line = null; 
                FileReader fileReader = new FileReader(results);            
                //Always wrap FileReader in Bufferedreader
                BufferedReader bufferedReader = new BufferedReader(fileReader);                                            
                line = bufferedReader.readLine();
                //Make sure to close the file
                bufferedReader.close();
                return line;
            }
        } catch (Throwable e){
            logger.error("Error " + e.getMessage());
        }
        return "";
    }
        //</editor-fold> 
    //Create command and params along with running Link program
    static public boolean ExecLink(String command, String params){
        //<editor-fold defaultstate="collapsed">          
        logger.debug("3S link being setup.");   
        //Create the command file
        try{
            if (!command.equals("")) {
                //params can be nothing
                if (!params.equals("")){
                    if (!Params(params)){
                        //there was an error creating the params file do not continue
                        return false;
                    }
                }
            } 
            //Create the command file
            if (Command(command)){
                //if file creattion sucessful, execute 
                if (Execute3S()){
                    logger.debug("3S link completed.");
                    return true;
                }
            }
            return false;          
        } catch (Throwable e){
            logger.error("Error in 3S Link ExecLink " + e.getMessage());
        }
        return false;        
    }
        //</editor-fold>    
    //Create the command file
    static public boolean Command(String cmd){
        //<editor-fold defaultstate="collapsed">          
        try {
            PrintWriter c = new PrintWriter("command", "UTF-8");
            c.print(cmd);
            c.close(); 
            return true;
        } catch (Throwable e){
            return false;
        }      
    }
        //</editor-fold>
    
    //Create the params file
    static public boolean Params(String cmd){
        //<editor-fold defaultstate="collapsed">          
        try {
            PrintWriter p = new PrintWriter("params", "UTF-8");
            p.print(cmd);
            p.close();   
            return true;
        } catch (Throwable e){
            return false;
        }   
    }
        //</editor-fold>    
    
    //Remove the command file
    static public void rmCommand(){
        File rmFile = new File("command");
        if (rmFile.exists()){rmFile.delete();}
    }
    
    //Execute the 3S link program
    static public boolean Execute3S(){ 
        //<editor-fold defaultstate="collapsed">          
        logger.debug("3S link executing.");
        //Make sure the done.alarm file does not exists
        logger.debug("Delete done.alarm");
        File rmFile = new File("done.alarm");
        if (rmFile.exists()){ rmFile.delete();}    
        //Make sure the API_ERROR_LOG file does not exists
        logger.debug("Delete API_ERROR_LOG");
        rmFile = new File("API_ERROR_LOG");
        if (rmFile.exists()){ rmFile.delete();}         
        try {        
            Process process = new ProcessBuilder("3S/shootingsportssoftwareapi.exe").start();           
            //Wait until process is done
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get("");
            path.register(
                watchService, 
                  StandardWatchEventKinds.ENTRY_CREATE, 
                    StandardWatchEventKinds.ENTRY_DELETE, 
                      StandardWatchEventKinds.ENTRY_MODIFY);
            WatchKey key;
            logger.debug("3S watch initiated.");
            try {
                while ((key = watchService.take()) != null) {
                    for (WatchEvent<?> event : key.pollEvents()) {
                        if (event.kind().toString().equals("ENTRY_CREATE") && event.context().toString().equals("done.alarm")){
                            //3S was successfull so return and do stuff
                            Thread.sleep(40);
                            rmFile = new File("done.alarm");
                            rmFile.delete();
                            //Delete the command file
                            rmCommand();
                            return true;
                        } else if (event.kind().toString().equals("ENTRY_CREATE") && event.context().toString().equals("API_ERROR_LOG")){
                            Thread.sleep(40);
                            rmFile = new File("API_ERROR_LOG");
                            //Open error log and place it in ours for reference
                            Scanner error = new Scanner(rmFile);                           
                            //Log the file contents
                            String line = error.useDelimiter("\\A").next();
                            error.close();                            
                            logger.error(line);
                            rmFile.delete();  
                            //Delete the command file
                            rmCommand();                            
                            JOptionPane.showMessageDialog(null, "Cannot execute link to 3S!\nMake sure 3S server is running!");
                            return false;                            
                        }
                    }
                    key.reset();
                }
            } catch (InterruptedException ex) {
                //java.util.logging.Logger.getLogger(threeSLink.class.getName()).log(Level.SEVERE, null, ex);
                logger.error(ex.getMessage());
            }           
        } catch (IOException ex) {
            logger.error("Cannot execute 3S link");
            JOptionPane.showMessageDialog(null, "Cannot execute link to 3S!");
            logger.error(ex.getMessage());
            //Delete the command file
            rmCommand();            
            return false;
        }
        //Delete the command file
        rmCommand();        
        return false;
    }
        //</editor-fold>
}
