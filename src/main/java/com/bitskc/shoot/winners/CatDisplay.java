/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitskc.shoot.winners;

import static com.bitskc.shoot.winners.Database.stmt;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import org.slf4j.LoggerFactory;
/**
 *
 * @author talbers
 */
public class CatDisplay {
   
    static org.slf4j.Logger logger = LoggerFactory.getLogger("CatDisplay");  
    
    public static void LoadCat(int numWin){
        ResultSet rsCat;
        String SQL;
        int numCat = 0;
        int numWide = 3;
        ArrayList<String>tmpCatPrint = new ArrayList<String>();
        //Clear out any old categories
        Shoot_Winners.categories.clear();
        //Get the number of unique categories in this shoot
        try {
            SQL = "Select count(distinct(category)) as totalRows from raw where category is not null and category <> ''";
            rsCat = stmt.executeQuery(SQL);
            rsCat.next();
            numCat = rsCat.getInt("totalRows");   //Figure out how many categories we have            
            SQL = "Select distinct(category) from raw where category is not null and category <> '' order by category";
            rsCat = stmt.executeQuery(SQL);
            rsCat.next();
        } catch (Throwable e){
            errorPrint(e);
            logger.error(e.getMessage()); 
            return;
        }   
        Shoot_Winners.pnlCatD.setLayout(new GridBagLayout());
        JLabel lblCat;
        SpinnerModel model;
        JSpinner spnrCat;
        GridBagConstraints gbcLC = new GridBagConstraints();
        gbcLC.ipadx = 0;
        gbcLC.ipady = 0; 
        gbcLC.gridy = 0;
        gbcLC.weightx = 0;
        gbcLC.weighty = 0;
        gbcLC.anchor = GridBagConstraints.LINE_START;                
        int x = 0;
        String catName;
        try {   //Add components to JPanel
            for (int i = 1; i <= numCat; i++){   
                model = new SpinnerNumberModel(0,0,20,1);
                spnrCat = new JSpinner(model);  
                spnrCat.setPreferredSize(new Dimension(45, 28));
                spnrCat.setValue(numWin);
                catName = rsCat.getString("Category");
 //               Shoot_Winners.categories.addElement(catName); 
                tmpCatPrint.add(catName);
                spnrCat.setName(catName);
                gbcLC.gridx = x;
                gbcLC.insets = new Insets(0,8,0,0);   //top, left, bottom, right                  
                gbcLC.gridwidth = 3; //JSpinners are 2, JLabels are 3
                Shoot_Winners.pnlCatD.add(spnrCat, gbcLC);
                lblCat = new JLabel();
                lblCat.setText(catName); 
                lblCat.setName(catName);
                x = x + 3;
                gbcLC.gridx = x;
                gbcLC.insets = new Insets(0,0,0,0);
                gbcLC.gridwidth = 4;
                Shoot_Winners.pnlCatD.add(lblCat, gbcLC);
                if ((i % numWide) == 0) {
                    x = 0;
                    gbcLC.gridy = (i / numWide);
                } else {
                    x = x + 4;
                }
                //reset x after 3 items have been                 
                rsCat.next();
            }
            //Add this to make everything move to the top           
            gbcLC.weightx = 1;
            gbcLC.weighty = 1;
            gbcLC.gridy = gbcLC.gridy + 1;
            gbcLC.gridx = 0;
            gbcLC.gridwidth = (numWide * 7);
            Shoot_Winners.pnlCatD.add(new JLabel(" "),gbcLC);                     
            for (int i = 0; i < Shoot_Winners.strCatPrint.size(); i++){   //Check strCatPrint against the new category list tmpCatPrint, remove any in strCatPrint that don't belong
                if (!tmpCatPrint.contains(Shoot_Winners.strCatPrint.get(i))){ //no match found remove the item from strCatPrint
                    Shoot_Winners.strCatPrint.remove(i);
                    i--;
                } 
            }
            for (int i = 0; i < tmpCatPrint.size(); i++){  //now add any categories not in strCatPrint that are in tmpCatPrint
                if (!Shoot_Winners.strCatPrint.contains(tmpCatPrint.get(i))){
                    Shoot_Winners.strCatPrint.add(tmpCatPrint.get(i));
                }
            }
            //add all the categories to the JList
            for (int i = 0; i < Shoot_Winners.strCatPrint.size(); i++){
                Shoot_Winners.categories.addElement(Shoot_Winners.strCatPrint.get(i));
            }
        } catch (Throwable e){
            errorPrint(e);
            logger.error(e.getMessage()); 
            return;
        } 
    }
    
    static void errorPrint(Throwable e) {
        Progress.closeProgress();   
        if (e instanceof SQLException)
            SQLExceptionPrint((SQLException)e);
        else {
            //System.out.println("A non-SQL error occurred.");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }
    //  Iterates through a stack of SQLExceptions
    static void SQLExceptionPrint(SQLException sqle) {
        while (sqle != null) {
            //System.out.println("\n---SQLException Caught---\n");
            //System.out.println("SQLState:   " + (sqle).getSQLState());
            //System.out.println("Severity: " + (sqle).getErrorCode());
            //System.out.println("Message:  " + (sqle).getMessage());
            logger.error("\n---SQLException Caught---\n");
            logger.error("SQLState:   " + (sqle).getSQLState());
            logger.error("Severity: " + (sqle).getErrorCode());
            logger.error("Message:  " + (sqle).getMessage());
            sqle.printStackTrace();
            sqle = sqle.getNextException();
        }
    }    
}